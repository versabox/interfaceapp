#ifndef STREAMINITIALIZER_H
#define STREAMINITIALIZER_H

#include <QUdpSocket>
#include <QTimer>

#include <iostream>

class StreamInitializer : public QObject
{
    Q_OBJECT
public:
    StreamInitializer(QString ip, int port);
    ~StreamInitializer();

    void start();

private:
    QUdpSocket *udpSocket;
    QTimer timerUdpStarter;
    QString ip;
    int port;

private slots:
    void readUdpDatagram();
    void writeUdpDatagram();

signals:
    void mappedAddressReceived(QString ip, int port);
};

#endif // STREAMINITIALIZER_H
