#ifndef HEADCAMERAIMAGEPROVIDER_H
#define HEADCAMERAIMAGEPROVIDER_H

#include <QObject>
#include "QQuickImageProvider"
#include <iostream>

class HeadCameraImageProvider : public QObject, public QQuickImageProvider
{
    Q_OBJECT
public:
    HeadCameraImageProvider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);
    QImage image;

public slots:
    void newImage(QImage image);
};

#endif // HEADCAMERAIMAGEPROVIDER_H
