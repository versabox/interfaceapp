#ifndef VIDEORECEIVER_H
#define VIDEORECEIVER_H

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/pixfmt.h>
#include <stdio.h>
#include <pthread.h>
}

#include <string>
#include <exception>

#include <QString>
#include <QTimer>
#include <QObject>
#include <QByteArray>
#include <QImage>

class VideoReceiver : public QObject
{
    Q_OBJECT

private:
    AVFormatContext *formatCtx = NULL;
    int videoStream;
    AVCodecContext *codecCtx = NULL;
    AVFrame *decodedFrame = NULL;
    AVFrame *frameRGBA = NULL;
    uint8_t *buffer;
    struct SwsContext *sws_ctx = NULL;

    int outputWidth;
    int outputHeight;

    QTimer frameReaderTimer;
    qint64 startTime;

    std::string fileName;
    bool uninitialized;
    void finish();


    QByteArray convertToPPMByteArray(AVFrame *pFrame, int width, int height);

public:
    VideoReceiver(std::string fileName, int outputWidth, int outputHeight);
    ~VideoReceiver();
    int getVideoWidth();
    int getVideoHeight();

signals:
    void frameReady(QImage image);
    void receiverException(QString message);
    void receiverInitialized();

public slots:
    void init();
    void startReceiver(int frequency);
    void stopReceiver();
    void setResolution(int width, int height);

private slots:
    void receiveFrame();
};

class VideoReceiverException : public std::exception
{
private:
    const std::string message;
public:
    VideoReceiverException(std::string message);
    const char *what() const noexcept(true);
};

#endif // VIDEORECEIVER_H
