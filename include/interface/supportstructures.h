#ifndef SUPPORTSTRUCTURES
#define SUPPORTSTRUCTURES

#include <QObject>
#include <QQmlListProperty>
#include <map>
#include <iostream>
#include <cmath>

class KeyPoseElement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString getName READ getName)
    Q_PROPERTY(int getQmlType READ getQmlType)
    Q_PROPERTY(QString getDescription READ getDescription)
    Q_PROPERTY(double getX READ getX)
    Q_PROPERTY(double getY READ getY)
    Q_PROPERTY(double getFi READ getFi)
    Q_PROPERTY(int getTime READ getTime)
    Q_PROPERTY(int getTimeSetType READ getTimeSetType)
    Q_PROPERTY(QList<int> getIntsArray READ getIntsArray)
    Q_PROPERTY(QList<float> getFloatsArray READ getFloatsArray)
    Q_PROPERTY(QList<QString> getStringsArray READ getStringsArray)
public:
    double getX(){
        return x;
    }
    double getY(){
        return y;
    }
    double getFi(){
        return fi;
    }
    QString getName(){
        return name;
    }
    int getQmlType(){
        return keyPoseTypeToQmlMap.find(type)->second;
    }
    QString getDescription(){
        return description;
    }
    QList<int> getIntsArray(){
        return intsArray;
    }

    QList<float> getFloatsArray(){
        return floatsArray;
    }

    QList<QString> getStringsArray(){
        return stringsArray;
    }
    QString getTypeSting(){
        if(keyPoseNameTypeMap.count(type)){
            return keyPoseNameTypeMap.at(type);
        }
        return "";
    }
    QString getTimeSting(){
        if(boolParamsArray.size() == static_cast<int>(BOOLPARAMSSTRUCTURE::NUMBER_OF_ITEMS)){

            if(boolParamsArray.at(static_cast<int>(BOOLPARAMSSTRUCTURE::SET_DEFAULT_TIME))){

                return keyPoseNameBoolParamsMap.at(static_cast<int>(BOOLPARAMSSTRUCTURE::SET_DEFAULT_TIME));
            }
            else if (boolParamsArray.at(static_cast<int>(BOOLPARAMSSTRUCTURE::SET_INFINITE_TIME))){

                return keyPoseNameBoolParamsMap.at(static_cast<int>(BOOLPARAMSSTRUCTURE::SET_INFINITE_TIME));
            }
            else{

                return QString::number(intsArray.at(static_cast<int>(INTSDEADLINESTRUCTURE::DEADLINE_TIME_SEC)));
            }
        }
        return QString("");
    }

    QString getIntsParamString(int index){
        if(index < intsArray.size()){
            return QString::number(intsArray.at(index));
        }
        else{
            return QString("");
        }
    }
    int getTime(){

        return intsArray.at(static_cast<int>(INTSDEADLINESTRUCTURE::DEADLINE_TIME_SEC));
    }

    int getTimeSetType(){

        if(boolParamsArray.size() == static_cast<int>(BOOLPARAMSSTRUCTURE::NUMBER_OF_ITEMS)){

            if(boolParamsArray.at(static_cast<int>(BOOLPARAMSSTRUCTURE::SET_DEFAULT_TIME))){

                return static_cast<int>(QMLBOOLPARAMSTRUCTURE::SET_DEFAULT_TIME);
            }
            else if (boolParamsArray.at(static_cast<int>(BOOLPARAMSSTRUCTURE::SET_INFINITE_TIME))){

                return static_cast<int>(QMLBOOLPARAMSTRUCTURE::SET_INFINITE_TIME);
            }
            else{

                return static_cast<int>(QMLBOOLPARAMSTRUCTURE::SET_DEADLINE_TIME);
            }
        }
    }

    QString name;
    QString description;
    double x;
    double y;
    double fi;
    int type;
    QList<int> intsArray;
    QList<float> floatsArray;
    QList<QString> stringsArray;
    QList<bool> boolParamsArray;

    enum KEYPOSESTRUCTURE{
        NAME = 0,
        TYPE = 1,
        DESCRIPTION = 2,
        POSEX = 3,
        POSEY = 4,
        POSEFI = 5,
        INTS = 6,
        FLOATS = 7,
        STRINGS = 8,
        BOOLS = 9
    };
    enum class QMLKEYPOSETYPE{
        BUTTON_WITH_DEADLINE = 0,
        SIMPLE = 1,
        TROLLEY = 2,
        DOOR = 3,
        GIVE_WAY = 4,
        CHARGING = 5,
        LIFT = 6

    };
    enum class ROSKEYPOSETYPE{
        BUTTON_WITH_DEADLINE = 10,
        SIMPLE = 20,
        TROLLEY = 30,
        DOOR = 40,
        GIVE_WAY = 50,
        CHARGING = 60,
        LIFT = 70
    };
    enum class INTSDEADLINESTRUCTURE{
        DEADLINE_TIME_SEC = 0,
        NUMBER_OF_ITEMS = 1
    };
    enum class BOOLPARAMSSTRUCTURE{
        SET_DEFAULT_TIME = 0,
        SET_INFINITE_TIME = 1,
        NUMBER_OF_ITEMS = 2
    };
    enum class QMLBOOLPARAMSTRUCTURE{
        SET_DEADLINE_TIME = 0,
        SET_DEFAULT_TIME = 1,
        SET_INFINITE_TIME = 2
    };

    static std::map<int,int> keyPoseTypeToRosMap;
    static std::map<int,int> keyPoseTypeToQmlMap;
    static std::map<int,int> keyPoseBoolParamMap;
    static std::map<int,QString> keyPoseNameTypeMap;
    static std::map<int,QString> keyPoseNameBoolParamsMap;

};

class MidKeyPoseGraphElement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double X READ getX WRITE setX)
    Q_PROPERTY(double Y READ getY WRITE setY)
    Q_PROPERTY(QQmlListProperty<MidKeyPoseGraphElement> edge READ getQmlEdges)
public:
    double getX(){
        return x;
    }
    double getY(){
        return y;
    }
    QList<MidKeyPoseGraphElement* > getEdges(){
        return edges;
    }
    QQmlListProperty<MidKeyPoseGraphElement> getQmlEdges(){

        return QQmlListProperty<MidKeyPoseGraphElement>(this,edges);
    }
    void setX(double _x){

        x = _x;
    }
    void setY(double _y){

        y = _y;
    }
    void setEdges(MidKeyPoseGraphElement* newEdge){

        if(!edges.count(newEdge)){

            edges.push_back(newEdge);
        }
    }
    void deleteEdge (MidKeyPoseGraphElement* point){

        edges.removeAll(point);
    }

private:
    double x;
    double y;
    QList<MidKeyPoseGraphElement* > edges;
};

class Point : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double x READ x)
    Q_PROPERTY(double y READ y)

public:

    Point() = default;
    Point(const Point& newPoint):
    x_(newPoint.x_), y_(newPoint.y_){}

    double x(){

        return x_;
    }

    double y(){

        return y_;
    }

    void setX(double x){

        x_ = x;
    }

    void setY(double y){

        y_ = y;
    }

private:

    double x_;
    double y_;
};

class WarningZoneElement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int Type READ Type)
    Q_PROPERTY(QQmlListProperty<Point> Shape READ Shape)
    Q_PROPERTY(QString Name READ Name)

public:

    WarningZoneElement() = default;
    WarningZoneElement(const WarningZoneElement& newWarningZone):
    name_(newWarningZone.name_), shape_(newWarningZone.shape_), type_(newWarningZone.type_){}

//    WarningZoneElement& operator = (const WarningZoneElement& rhs) {
//        this->name_ = rhs.name_;
//        this->shape_ = rhs.shape_;
//        this->type_ = rhs.type_;
//        return *this;
//    }

    int Type(){

        return type_;
    }

    QQmlListProperty<Point> Shape(){

        return QQmlListProperty<Point>(this, shape_);
    }

    QString Name(){

        return name_;
    }

    void setType(int type){

        type_ = type;
    }

    void setShape(QList<Point*> shape){

        shape_ = shape;
    }

    QList<Point*> getShape(){

        return shape_;
    }

    Q_INVOKABLE void addToShape(double x, double y){

        auto index = getIndexOfPoint(x,y);
        if(index != -1){

            shape_.push_back(shape_.at(index));
        }
        else {

            Point *newPoint = new Point;
            newPoint->setX(x);
            newPoint->setY(y);
            shape_.push_back(newPoint);
        }
    }

    Q_INVOKABLE Point* deleteFromShape(int index){

        shape_.removeAt(index);
        return new Point(*shape_.last());
    }

    Q_INVOKABLE Point* deleteLastPointFromShape(){

        if(!shape_.empty()){

            shape_.removeLast();
            if(!shape_.empty()){

                return new Point(*shape_.last());
            }
            else{

                return nullptr;
            }
        }
        return nullptr;
    }

    Q_INVOKABLE void clearShape(){

        shape_.clear();
    }

    Q_INVOKABLE int getIndexOfPoint(double x, double y){

        for(auto tempElement : shape_){

            if(std::sqrt(std::pow(tempElement->x() - x,2) + std::pow(tempElement->y() - y, 2)) < 6){

                return shape_.indexOf(tempElement);
            }
        }

        return -1; //not found
    }

    void setName(QString name){

        name_ = name;
    }

    enum class QMLWARNINGZONETYPE{
        CROSSING = 0,
        DOCKING = 1,
        FORBIDDEN = 2,
        INFO = 3,
        SILENT = 4,
        DOOR = 5,
        FOLLOW_PATH = 6,
        FAST_RIDE_ZONE = 7,
        SLOW_RIDE_ZONE = 8,
        STOP_ZONE = 9,
        NO_RECOVERY_BEHAVIOR = 10,
        FOLLOW_PATH_CONDITIONAL_ZONE = 11,
    };
    enum class JSONWARNINGZONETYPE{
        CROSSING = 10,
        DOCKING = 20,
        FORBIDDEN = 30,
        INFO = 40,
        SILENT = 50,
        DOOR = 60,
        FOLLOW_PATH =70,
        FAST_RIDE_ZONE = 80,
        SLOW_RIDE_ZONE = 90,
        STOP_ZONE = 100,
        NO_RECOVERY_BEHAVIOR = 110,
        FOLLOW_PATH_CONDITIONAL_ZONE = 120,
    };
    enum class JSONWARNINGZONESHAPETYPE{
        CIRCLE = 10,
        POLYGON = 20
    };
    enum class JSONWARNINGZONESTRUCTURE{
        NAME = 0,
        TYPE = 1,
        SHAPE = 2,
    };
    enum class JSONWARNINGZONECIRCLESTRUCTURE{
        X = 0,
        Y = 1,
        R = 2,
    };
    enum class JSONWARNINGZONESHAPESTRUCTURE{
        TYPE = 0,
        INTS = 1,
        FLOATS = 2,
        STRINGS = 3
    };

    static std::map<int,int> qmlToJsonWarningZoneTypeMap;

private:
    int type_;
    QList<Point*> shape_;
    QString name_;
};

enum class LIFTING{

    UP = 2,
    DOWN = 1,
    NEUTRAL = 0
};

class KeyPosePathViewElement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString imageSource READ imageSource CONSTANT)
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(QString type READ type CONSTANT)
    Q_PROPERTY(QString time READ time CONSTANT)

public:

    KeyPosePathViewElement(){}
    KeyPosePathViewElement(QString name, QString imageSource, QString description = ""):
    name_(name), imageSource_(imageSource), description_(description){}
    KeyPosePathViewElement(QString name, QString imageSource, QString description, QString type, QString time):
    name_(name), imageSource_(imageSource), description_(description), type_(type), time_(time){}

    QString name(){

        return name_;
    }

    QString imageSource(){

        return imageSource_;
    }

    QString description(){

        return description_;
    }
    QString type(){

        return type_;
    }

    QString time(){

        return time_;
    }

    void setName(QString name){

        name_ = name;
    }

    void setImageSource(QString imageSource){

        imageSource_ = imageSource;
    }

    void setDescription(QString description){

        description_ = description;
    }

    void setType(QString type){

        type_ = type;
    }

    void setTime(QString time){

        time_ = time;
    }

private:

    QString name_;
    QString imageSource_;
    QString description_;
    QString type_;
    QString time_;
};

class KeyPosePath : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(QList<QString> path READ path CONSTANT)

public:

    KeyPosePath(QString name, QList<QString> path, QString description = ""):
        name_(name),description_(description), path_(path){}

    QString name() const{

        return name_;
    }

    QString description() const{

        return description_;
    }

    QList<QString> path() const{

        return path_;
    }

    void setName(QString name){

        name_ = name;
    }

    void setDescription(QString description){

        description_ = description;
    }

    void setPath(QList<QString> path){

        path_ = path;
    }

private:

    QString name_;
    QString description_;
    QList<QString> path_;
};

class Locker
{

public:
    enum class QMLLOCKERACTION{
        lock = 0,
        unlock = 1
    };
    enum class JSONLOCKERACTION{
        lock = 40,
        unlock = 20
    };

    static std::map<int,int> qmlToJsonLockerActionMap;
};
#endif // SUPPORTSTRUCTURES
