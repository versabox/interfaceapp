#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QMutex>
#include <iostream>

#include "stubclient.h"
#include "supportstructures.h"
#include <jsonrpccpp/client/connectors/httpclient.h>
#include <QThread>
#include <QTimer>
#include <stdio.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <QString>
#include <math.h>
#include "videoreceiver.h"
#include "streaminitializer.h"
#include <QStyle>
#include <QVariant>
#include <QQmlListProperty>
#include <QNetworkInterface>
#include <QQmlContext>
#include <QVector2D>
#include <map>
#include <QVector>
#include <QString>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <jsoncpp/json/value.h>
#include <fstream>
#include <string.h>

class Client : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString mapAdress READ mapAdress)
    Q_PROPERTY(int mapWidth READ mapWidth)
    Q_PROPERTY(int batteryPower READ batteryPower)
    Q_PROPERTY(int mapHeight READ mapHeight)
    Q_PROPERTY(QList<double> midpoints READ midpoints NOTIFY midpointsChanged)
    Q_PROPERTY(QList<double> position READ position)
    Q_PROPERTY(QList<double> headPosition READ headPosition NOTIFY headPositionChanged)
    Q_PROPERTY(int titleBarHeight READ titleBarHeight)
    Q_PROPERTY(QList<double> path READ path)
    Q_PROPERTY(QList<QString> listOfMaps READ listOfMaps)
    Q_PROPERTY(QQmlListProperty<KeyPoseElement> keyPoseList READ keyPoseList)
    Q_PROPERTY(QList<double> laserScan READ laserScan)
    Q_PROPERTY(double angleOfLaserScan READ angleOfLaserScan)
    Q_PROPERTY(QList<bool> visibleOfKeyPoses READ visibleOfKeyPoses)
    Q_PROPERTY(QList<double> tempNoDrivingArea READ tempNoDrivingArea)
    Q_PROPERTY(QQmlListProperty<MidKeyPoseGraphElement> midKeyPoseGraphList READ midKeyPoseGraphList)
    Q_PROPERTY(QQmlListProperty<WarningZoneElement> warningZoneList READ warningZoneList)
    Q_PROPERTY(QList<QString> keyPosePathList READ keyPosePathList NOTIFY signalKeyPosePathList)
    Q_PROPERTY(QQmlListProperty<QList<Point>> noDrivingArea READ noDrivingArea)
    Q_PROPERTY(QQmlListProperty<Point> newNoDrivingArea READ newNoDrivingArea)
    Q_PROPERTY(WarningZoneElement* newWarningZone READ newWarningZone)

public:

    explicit Client(QQmlContext *&context, QObject *parent = 0);

    ~Client();
    //language
    Q_INVOKABLE QString translateIntoSelectedLanguage(QString id, QString language);

    //midPoint
    Q_INVOKABLE QList<double> getMidpointToRemove(double firstMeterX, double firstMeterY);

    //keyPoses
    Q_INVOKABLE void setVisibleOfKeyPoses(int Id, bool checked);
    Q_INVOKABLE KeyPoseElement* getKeyPoseToDelete(double firstMeterX, double firstMeterY);

    //midKeyPoseGraph
    Q_INVOKABLE void addMidKeyPoseGraph(double firstX, double firstY, double lastX, double lastY);
    Q_INVOKABLE void deleteMidKeyPoseGraphPoint(double firstX, double firstY, double lastX, double lastY);
    Q_INVOKABLE MidKeyPoseGraphElement* getMidKeyPose(double x, double y);
    Q_INVOKABLE void editMidKeyPoint(double x, double y, double oldX, double oldY);
    Q_INVOKABLE int getIndexOfMidKeyPose(double x, double y);

    //keyPosePath
    Q_INVOKABLE QVector2D getKeyPosePositionFromName(const QString name);

    //noDrivingArea
    Q_INVOKABLE void addNoDrivingPoint(double x, double y);
    Q_INVOKABLE int getIndexOfNoDrivingPoint(double x, double y);
    Q_INVOKABLE Point* deleteLastNoDrivingPoint();

    QString mapAdress() {
        mutex.lock();
        QString ret = myMapAdress;
        mutex.unlock();
        return ret;
    }
    int mapWidth() {
       mutex.lock();
       int ret = myMapWidth;
       mutex.unlock();
       return ret;
    }
    int mapHeight() {
       mutex.lock();
       int ret = myMapHeight;
       mutex.unlock();
       return ret;
    }
    int batteryPower(){
        mutex.lock();
        int ret = qmlBatteryPower;
        mutex.unlock();
        return ret;
    }

    QList<double> midpoints(){
        mutex.lock();
        QList<double> ret = qmlMidpointList;
        mutex.unlock();
        return ret;
    }
    QList<double> position() {
        mutex.lock();
        QList<double> ret = qmlRobotPosition;
        mutex.unlock();
        return ret;
    }
    QList<double> headPosition() {
        mutex.lock();
        QList<double> ret = qmlHeadPosition;
        mutex.unlock();
        return ret;
    }

    int titleBarHeight(){
           return QStyle::PM_TitleBarHeight;
    }
    QList<double> path() {
        mutex.lock();
        QList<double> ret = qmlPath;
        mutex.unlock();
        return ret;
    }

    QList<QString> listOfMaps(){
        return qmlListOfMaps;
    }
    QList<double> laserScan(){
        mutex.lock();
        QList<double> ret = qmlLaserScan;
        mutex.unlock();
        return ret;
    }
    double angleOfLaserScan(){
        return qmlAngleOfLaserScan;
    }
    QList<bool> visibleOfKeyPoses() {
           return qmlSettingsOfVisibleKeyPoses;
    }
    QList<double> tempNoDrivingArea(){

           return qmltempNoDrivingArea;
    }
    QQmlListProperty<KeyPoseElement> keyPoseList(){

        return QQmlListProperty<KeyPoseElement>(this, qmlKeyPoseList);
    }

    QQmlListProperty<MidKeyPoseGraphElement> midKeyPoseGraphList(){

        if(!drawingStateOfMidKeyPoseGraph){

            return QQmlListProperty<MidKeyPoseGraphElement>(this,qmlMidKeyPoseGraphList);
        }
        else{

            return QQmlListProperty<MidKeyPoseGraphElement>(this,tempMidKeyPoseGraphList);
        }
    }
    QQmlListProperty<WarningZoneElement> warningZoneList(){

        return QQmlListProperty<WarningZoneElement>(this, qmlWarningZone);
    }

    QList<QObject *> keyPoseGridViewList; //typ KeyPosePathViewElement
    QList<QObject *> keyPoseListViewList; //typ KeyPosePathViewElement
    QList<QObject *> keyPosePathGridViewList; //typ KeyPosePathViewElement

    QList<QString> keyPosePathList(){

        QList<QString> ret;

        for(auto tempElement : keyPosePathGridViewList){

            ret.append(qobject_cast<KeyPosePathViewElement *>(tempElement)->name());
        }

        return ret;
    }

    QQmlListProperty<QList<Point>> noDrivingArea(){

        return QQmlListProperty<QList<Point>>(this, qmlNoDrivingArea);
    }

    QQmlListProperty<Point> newNoDrivingArea(){

        return QQmlListProperty<Point>(this, qmlNewNoDrivingArea);
    }


    WarningZoneElement* newWarningZone(){

        return &qmlNewWarningZone;
    }

private:
    QString login_;
    QString password_;
    int port_;

    QQmlContext* context_;
    jsonrpc::HttpClient *httpclient;
    StubClient *client;

    QTimer *timerRobotInfo;
    QTimer *timerHeadPosition;

    QTimer *timerMoveHead;
    QTimer *timerMoveHeadCameraKeyboard;
    QTimer *timerCmdVel;
    QTimer *timerBatteryState;
    QTimer *timerLiftState;

    QTimer *starter;
    VideoReceiver *videoReceiver;
    QThread videoReceiverThread;
    QMutex mutex;
    QMutex muteDictionary;

    QString serverAdress;

    int sessionId;
    QString myMapAdress;
    int myMapWidth=-1;
    int myMapHeight=-1;
    int nominalBatteryValue = 12000;
    double myMapResolution;
    double horizontalMoveHead;
    double verticalMoveHead;
    double horizontalMoveHeadSpeed;
    double verticalMoveHeadSpeed;
    double robotLinearSpeed;
    double robotAngularSpeed;
    double maxLinearSpeed=0.5;
    double maxAngularSpeed=1;
    bool enableSteering=false;
    bool enableMoveHeadCameraByKeyboard=false;
    int qmlBatteryPower=0;
    bool visualTypeOfMap=false;
    bool dictionary_flag = false;


    QList<QList<double>> midpointList;
    QList<double> qmltempNoDrivingArea;
    QList<double> qmlMidpointList;
    unsigned myNumberOfMidpoints=0;

    QList<double> qmlRobotPosition;
    QList<double> qmlHeadPosition;
    QList<double> qmlPath;
    QList<QString> qmlListOfMaps;
    QList<KeyPoseElement*> qmlKeyPoseList;
    QList<MidKeyPoseGraphElement*> qmlMidKeyPoseGraphList;
    QList<MidKeyPoseGraphElement*> tempMidKeyPoseGraphList;
    bool drawingStateOfMidKeyPoseGraph = false;
    QList<double> qmlLaserScan;
    double qmlAngleOfLaserScan;
    QList<bool> qmlSettingsOfVisibleKeyPoses;
    bool mapIsBuilding=false;
    bool cameraWindowVisable=false;
    bool localConnection = true;
    StreamInitializer *streamInitializer;
    QList<WarningZoneElement *> qmlWarningZone;
    int lastLiftingAction = 0;
    QString dockImage = "qrc:/images/ikona.png";
    QString keyPoseImage = "qrc:/images/arrow.png";
    QString keyPosePathImage = "qrc:/images/arrowPath.png";
    QList<KeyPosePath *> listOfKeyPosePath;
    bool cyclicStateOfKeyPosePath;
    QList<QList<Point>*> qmlNoDrivingArea;
    QList<Point*> qmlNewNoDrivingArea;
    WarningZoneElement qmlNewWarningZone;
    std::map <QString, QString> dictionaries;

signals:
    //qml
//    void mapAdressChanged();
//    void mapWidthChanged();
//    void mapHeightChanged();
//    void midpointsChanged();
//    void numberOfMidpointsChanged();
//    void positionChanged();
    void updateMapNow();
    void updateImageMapOnly();
    void updateHeadCameraNow();
    void headPositionChanged();
    void editMidpoint();
    void newHeadImageReceived(QImage);
    void signalUpdateListOfMaps();
    void midpointsChanged();
    void refreshKeyPoseQmlList();
    void signalLogInRejected(int i);
    void signalLogInAccepted(QVariant i);
    //void sendVelocity(QString linear, QString angular);
    void sendVelocity(QVariant linearText, QVariant angularText, QVariant linear, QVariant angular);
    void signalRefreshBatteryState(QVariant low, QVariant critical);
    void signalLiftAction(QVariant action, QVariant status);

    //video
    void startReceiver(int frequency);
    void stopReceiver();
    void setResolution(int width, int height);

    void signalRefreshContextProperties(QVariant list, QString name);
    void signalSetCurrentIndexOfListView(QVariant index);
    void signalKeyPosePathList();


public slots:
    //qml
    void logIn(QString login, QString password, int port);
    void logIn();
    void logOut();
    void startApp();
    void startRobot();
    void stopRobot();
    void statusSlot();
    void showMap(int heightPixels, int widthPixels);
    void showMap();
    void moveHead(double horizontal, double vertical);
    void baseHead();
    void moveAbsHead(double horizontal, double vertical);
    void setRideMode(int mode);
    void getRideStatus();
    void getListOfMaps();
    void loadMap(int i, bool on);
    void joystickEnable();
    void joystickDisable();
    void joystickValueChanged(double linear,double angular);
    void goInDirectionWithSpeed();
    void changeRobot(QString login, QString password, int index, QString ip, int port);
    void changeStartingPoint(int which);
    void bulidNewMap();
    void mapWindowVisableChanged(bool visible);
    void cameraWindowVisableChanged(bool visible);
    void setEnableSteering(bool enable);
    void setHeadCameraSpeedKeyboard(double horizontal, double vertical);
    void setEnableMoveHeadKeyboard(bool enable);
    void changeMidpointsTolerance(double xy,double angle);
    void liftHandler(bool lift);

    //language
    void changeLanguage(QString language);



    //keyPosePath
    void keyPoseGridViewChooseHandler(QString name);
    void refreshMenuContent();
    void moveItemInListView(int oldIndex, int newIndex);
    void deleteItemFromListView(int index);
    void saveKeyPosePath(QString name, QString description);
    void loadKeyPosePath(QString name);
    void startKeyPosePath(QString name, bool stopRobot = false);

    //midPoints
    void removeAllMidpoint();
    void addMidpoint(double firstMeterX, double firstMeterY, double fiRadian, int index = -1);
    void removeMidpointIndex(int index);
    void removeMidpoint(double firstMeterX, double firstMeterY);

    //keyPoses
    void addKeyPose(QString name_, QString description_, double firstMeterX_, double firstMeterY_, double fiRadian_, int typeOfKeyPose_, int typeOfTimeSet_, int time_);
    void addKeyPoseAtRobotPosition(QString name_, QString description_, int typeOfKeyPose_, int typeOfTimeSet_, int time_);
    void sendToKeyPose(QString name);
    void deleteKeyPoseName(QString name);
    void deleteKeyPose(double firstMeterX, double firstMeterY);

    //warningArea
    void addWarningZone(double x, double y, double range, QString name, int type);
    void addWarningZone(QString name, int type);
    void deleteWarningZone(QString name);

    //midKeyPoseGraph
    void saveMidKeyPoseGraph(bool on, bool accepted = false);

    //keyPosePath
    void startKeyPosePath(bool cyclic);

    //robot
    void setRobotPosition(double X, double Y, double fi);
    void dockDeparture(int side);
    void dockApproach(int side);
    void sideButtonClicked(bool state);
    void stopCharingButtonClicked();
    void skipButtonClicked();

    //map
    void saveMap(QString name);

    //locker
    void lockerHandler(int on);

    //brake
    void brakeModeHandler(bool on);

    //reset diagnostics
    void resetDiagnosticsAction(bool on);

    //battery
    void getTheNominalBatteryValue();

    //task queue
    void suspendTaskQueue();
    void unsuspendTaskQueue();
    void abortTask();
    void clearTaskQueue();

private slots:
    //video
    void updateImage(QImage image);
    void printException(QString message);
    void receiverInitialized();
    void resizeDone();
    void timeOutMoveHead();
    void getTempMap();
    void getExtendedRobotInfo();
    void getStandardRobotInfo();
    void changeRobotInfoType(bool on);
    void moveHeadKeyboard();
    void initLocalStream();
    void initInternetStream(QString ip, int port);
    //refrash data
    void refreshMidpointList();
    void refreshMidpointList(Json::Value newData);
    void refreshRobotPosition();
    void refreshRobotPosition(Json::Value newData);
    void refreshKeyPose();
    void refreshKeyPose(Json::Value newData);
    void refreshHeadPosition();
    void refreshHeadPosition(Json::Value newData);
    void refreshPath();
    void refreshPath(Json::Value newData);
    void refreshLaserScan();
    void refreshLaserScan(Json::Value newData);
    void refreshBatteryState();
    void refreshWarningZone(Json::Value newData);
    void refreshWarningZone();
    void refreshLiftStatus();
    void refreshKeyPoseGridViewModel();
    void refreshKeyPosePathList();
    void refreshMidKeyPoseGraph(Json::Value newData);
};

#endif // CLIENT_H
