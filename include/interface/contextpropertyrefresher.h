#ifndef CONTEXTPROPERTYREFRESHER_H
#define CONTEXTPROPERTYREFRESHER_H

#include <QQmlContext>


class ContextPropertyRefresher : public QObject
{
    Q_OBJECT

public:

    explicit ContextPropertyRefresher(QQmlContext* &context, QObject *parent = 0): context_(context), QObject(parent)
    {

    }

public slots:
    void refreshProperty(QVariant list, QString name){
        context_->setContextProperty(name, list);
    }

private:

    QQmlContext* context_;

};

#endif // CONTEXTPROPERTYREFRESHER_H
