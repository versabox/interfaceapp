TEMPLATE = app
QT += core
QT += qml quick widgets
QT += network
CONFIG += c++11

LIBS += -lmicrohttpd -lcurl -ljsonrpccpp-client -ljsonrpccpp-common -ljsoncpp -lavformat -lavcodec -lavutil -lswscale


SOURCES += src/main.cpp src/client.cpp src/videoreceiver.cpp \
    src/headcameraimageprovider.cpp src/streaminitializer.cpp \
    src/supportstructures.cpp

RESOURCES += resources/qml.qrc resources/images.qrc \
             resources/dictionary.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += include/interface/client.h include/interface/videoreceiver.h include/interface/headcameraimageprovider.h \
    include/interface/findqmlobject.h include/interface/streaminitializer.h \
    include/interface/supportstructures.h \
    include/interface/contextpropertyrefresher.h

unix:!macx: LIBS += -L$$PWD/dependencies/lib/ -ljsonrpccpp-client

INCLUDEPATH += $$PWD/dependencies/include
DEPENDPATH += $$PWD/dependencies/include
