import QtQuick 2.5
import QtQml.Models 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import QtQuick.Window 2.2

Window{
    property var workspace
    property int firstPointX
    property int firstPointY
    property int lastPointX
    property int lastPointY
    property double fiRadian
    property double zoom: 1.0
    property var point
    property int lastWidth
    property int lastHeight
    property bool partMax: false
    property int flickX
    property int flickY
    property int counter: 2
    property int oldWidth
    property int oldHeight
    property double deltaW: 0
    property double deltaH: 0
    property double flickXInit
    property double flickYInit
    property double oldZoom
    property double oldFlickX
    property double oldFlickY
    property int state: 4

    id:drawMapWindow

    visible: true
    title: "Map"

    //color: "grey"

    signal dockMe(int w, int h)
    signal setScrollPosition(int x,int y,double zoom)
    signal closingWindow()

    function partlyMaximize()
    {
        lastWidth=drawMapWindow.width
        lastHeight=drawMapWindow.height
        drawMapWindow.height=Screen.height
        drawMapWindow.width=Screen.width/2
        partMax=true
    }

    function resizeCallback(w,h)
    {

        deltaW+=(w-oldWidth)/2
        deltaH+=(h-oldHeight)/2

        var x=flickX-Math.round(deltaW)
        var y=flickY-Math.round(deltaH)


        if(x>=(-zoom*mapCanvas.width/2+mapCanvas.width/2)&&x<=(zoom*mapCanvas.width/2+mapCanvas.width/2-scrollMap.flickableItem.width))
             scrollMap.flickableItem.contentX=x
        else{
            if(x<=(-zoom*mapCanvas.width/2+mapCanvas.width/2))
                scrollMap.flickableItem.contentX=-zoom*mapCanvas.width/2+mapCanvas.width/2
            else
                scrollMap.flickableItem.contentX=zoom*mapCanvas.width/2+mapCanvas.width/2-scrollMap.flickableItem.width
        }

        if(y>=(-zoom*mapCanvas.height/2+mapCanvas.height/2)&&y<=(zoom*mapCanvas.height/2+mapCanvas.height/2-scrollMap.flickableItem.height))
             scrollMap.flickableItem.contentY=y
        else{
            if(y<=(-zoom*mapCanvas.height/2+mapCanvas.height/2))
                 scrollMap.flickableItem.contentY=-zoom*mapCanvas.height/2+mapCanvas.height/2
            else
                scrollMap.flickableItem.contentY=zoom*mapCanvas.height/2+mapCanvas.height/2-scrollMap.flickableItem.height
        }

        deltaW-=Math.round(deltaW)
        deltaH-=Math.round(deltaH)
        flickX=scrollMap.flickableItem.contentX
        flickY=scrollMap.flickableItem.contentY
        oldWidth=w
        oldHeight=h

        if(counter<0){
            drawMapWindow.maximumWidth=zoom*mapCanvas.width
            drawMapWindow.maximumHeight=zoom*mapCanvas.height
        }

        if(counter==1||counter==2){
            if(visibility==Window.Maximized){
                zoom=Math.round(width/mapCanvas.width*10)/10
                flickXInit=-zoom*mapCanvas.width/2+mapCanvas.width/2
                flickYInit=-zoom*mapCanvas.height/2+mapCanvas.height/2
            }
        }
        counter--
    }

    ScrollView{

        id: scrollMap
        width: parent.width
        height: parent.height
        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
//        flickableItem.boundsBehavior: Flickable.StopAtBounds

        Canvas{
           property bool addMidpoint: false
           property bool removeMidpoint: false
           property bool addMidpointEnable: false
           property bool editMidpoint: false
           property bool editMidpointEnable: false
           property bool setRobotPosition: false
           property bool setRobotPositionEnable: false
           property double editMidpointRadian
           property int indexOfEditMidpoint

           signal signalAddMidpoint(double firstPointX,double firstPointY, double fiRadian)
           signal signalAddIndexMidpoint(double firstPointX,double firstPointY, double fiRadian, int index)
           signal signalRemoveMidpoint(double mouseX, double mouseY)
           signal signalSetRobotPosition(double firstPointX,double firstPointY, double fiRadian)


           id:mapCanvas
           scale: zoom
           width: 100
           height: 100
           onImageLoaded: {
               mapCanvas.requestPaint()
           }

           onPaint: {
               var ctx=getContext("2d")
               if(client.mapAdress!=""){
                  ctx.drawImage(client.mapAdress,0,0)
                  var angle
                   for(var i=0; i<client.path.length/2; i++){
                       ctx.save()
                       ctx.fillStyle=Qt.rgba(0, 0, 255, 0.5)
                       ctx.fillRect(client.path[2*i],client.path[2*i+1],3,3)
                       ctx.restore()
                   }
                   for(var i=0; i<3*(client.midpoints.length); i=i+3){
                       angle=client.midpoints[i+2]
                       ctx.save()
                       ctx.rotate(angle)
                       ctx.translate(0,-4)
                       ctx.drawImage("qrc:/images/arrow.png",(client.midpoints[i])*Math.cos(-angle)-(client.midpoints[i+1])*Math.sin(-angle),(client.midpoints[i])*Math.sin(-angle)+(client.midpoints[i+1])*Math.cos(-angle),25,8)
                       ctx.restore()
                       if(angle>=(Math.PI/2)  || angle<=(-Math.PI/2))
                       {
                           ctx.strokeText(i/3+1, client.midpoints[i]+2, client.midpoints[i+1])
                       }
                       else
                       {
                           if(i/3<10)
                               ctx.strokeText(i/3+1, client.midpoints[i]-7, client.midpoints[i+1])
                           else
                               ctx.strokeText(i/3+1, client.midpoints[i]-15, client.midpoints[i+1])
                       }

                   }
                   for(var i=0; i<client.keyPoseList.length; i++){
                       if(client.visibleOfKeyPoses[i] === true){
                           for(var j=0; j< client.keyPoseList[i].getX.length; j++)
                           {
                               angle=client.keyPoseList[i].getFi[j]
                               ctx.save()
                               ctx.rotate(angle)
                               ctx.translate(0,-4)
                               ctx.drawImage("qrc:/images/arrow.png",(client.keyPoseList[i].getX[j])*Math.cos(-angle)-(client.keyPoseList[i].getY[j])*Math.sin(-angle),(client.keyPoseList[i].getX[j])*Math.sin(-angle)+(client.keyPoseList[i].getY[j])*Math.cos(-angle),25,8)
                               ctx.restore()
                               ctx.strokeText(client.keyPoseList[i].getName + "_" + j, client.keyPoseList[i].getX[j]+5, client.keyPoseList[i].getY[j])
                           }
                       }
                   }
                   for(var i=0;i< client.laserScan.length; i++){
                       ctx.save()
                       var angle=client.position[2]+client.angleOfLaserScan/2-((i)/client.laserScan.length)*client.angleOfLaserScan
                       ctx.fillStyle=Qt.rgba(255, 0, 0, 1)
                       ctx.fillRect(client.position[0]+client.laserScan[i]*Math.cos(angle)+(2*Math.cos(-client.position[2]))-1,client.position[1]+client.laserScan[i]*Math.sin(angle)+(2*Math.sin(client.position[2]))-1,2,2)
                       ctx.restore()

                   }

                       ctx.save()
                       angle = client.position[2]-client.headPosition[0]-Math.PI/4
                       ctx.rotate(angle)
                       ctx.drawImage("qrc:/images/radar.png",(client.position[0])*Math.cos(-angle)-(client.position[1])*Math.sin(-angle),(client.position[0])*Math.sin(-angle)+(client.position[1])*Math.cos(-angle))
                       ctx.restore()

                       ctx.save()
                       ctx.rotate(client.position[2])
                       ctx.translate(-12.5,-12.5)
                       ctx.drawImage("qrc:/images/robot.png",(client.position[0])*Math.cos(-client.position[2])-(client.position[1])*Math.sin(-client.position[2]),(client.position[0])*Math.sin(-client.position[2])+(client.position[1])*Math.cos(-client.position[2]),25,25)
                       ctx.restore()
               }

               if(addMidpointEnable==true)
               {
                   ctx.save()
                   ctx.rotate(fiRadian)
                   ctx.translate(0,-4)
                   ctx.drawImage("qrc:/images/arrow.png",(firstPointX)*Math.cos(-fiRadian)-(firstPointY)*Math.sin(-fiRadian),(firstPointX)*Math.sin(-fiRadian)+(firstPointY)*Math.cos(-fiRadian),25,8)
                   ctx.restore()
               }
               if(setRobotPositionEnable==true)
               {
                   ctx.save()
                   ctx.rotate(fiRadian)
                   ctx.translate(-12.5,-12.5)
                   ctx.drawImage("qrc:/images/robot.png",(firstPointX)*Math.cos(-fiRadian)-(firstPointY)*Math.sin(-fiRadian),(firstPointX)*Math.sin(-fiRadian)+(firstPointY)*Math.cos(-fiRadian),25,25)
                   ctx.restore()
               }

           }
           MouseArea{
               anchors.fill: parent
               onPressed: {
                   if(mapCanvas.editMidpoint===false)
                   {
                       firstPointX=mouseX
                       firstPointY=mouseY
                       lastPointX=mouseX
                       lastPointY=mouseY
                   }
                   if(mapCanvas.addMidpoint==true){
                       mapCanvas.addMidpointEnable=true
                   }
                   else if(mapCanvas.setRobotPosition==true){
                       mapCanvas.setRobotPositionEnable=true
                   }
                   else if(mapCanvas.editMidpoint===true && mapCanvas.addMidpointEnable==false ){
                       mapCanvas.signalRemoveMidpoint(mouseX,mouseY)
                       firstPointX=mouseX
                       firstPointY=mouseY
                   }

                   mapCanvas.requestPaint()

               }
               onPositionChanged: {
                   lastPointX=mouseX
                   lastPointY=mouseY

                   if(!(mapCanvas.addMidpoint||mapCanvas.removeMidpoint || mapCanvas.editMidpoint || mapCanvas.setRobotPosition)){
                       var x=scrollMap.flickableItem.contentX+(firstPointX-lastPointX)
                       var y=scrollMap.flickableItem.contentY+(firstPointY-lastPointY)

                       if(x>=(-zoom*mapCanvas.width/2+mapCanvas.width/2)&&x<=(zoom*mapCanvas.width/2+mapCanvas.width/2-scrollMap.flickableItem.width))
                            scrollMap.flickableItem.contentX=x

                       if(y>=(-zoom*mapCanvas.height/2+mapCanvas.height/2)&&y<=(zoom*mapCanvas.height/2+mapCanvas.height/2-scrollMap.flickableItem.height))
                            scrollMap.flickableItem.contentY=y
                   }

                   fiRadian=Math.atan2((lastPointY-firstPointY),(lastPointX-firstPointX))
                   if(mapCanvas.editMidpointEnable==true){
                       fiRadian=mapCanvas.editMidpointRadian
                       firstPointX=lastPointX
                       firstPointY=lastPointY
                   }

                   flickX=scrollMap.flickableItem.contentX
                   flickY=scrollMap.flickableItem.contentY

                   mapCanvas.requestPaint()
               }

               onReleased: {
                   if(mapCanvas.addMidpoint && !mapCanvas.editMidpoint){
                       mapCanvas.signalAddIndexMidpoint(firstPointX,firstPointY,fiRadian, menuPanel.indexOfAddingMidpoint)
                   }
                   else if(mapCanvas.editMidpointEnable){
                       mapCanvas.editMidpointEnable=false
                   }
                   else if(mapCanvas.addMidpointEnable && mapCanvas.editMidpoint)
                   {
                       mapCanvas.signalAddIndexMidpoint(firstPointX,firstPointY,fiRadian, mapCanvas.indexOfEditMidpoint)
                       mapCanvas.addMidpointEnable=false
                   }
                   else if(mapCanvas.setRobotPosition)
                   {
                       mapCanvas.setRobotPositionEnable=false
                       mapCanvas.signalSetRobotPosition(firstPointX, firstPointY, fiRadian)
                   }
               }
               onDoubleClicked:{
                   if(mapCanvas.removeMidpoint)
                       mapCanvas.signalRemoveMidpoint(mouseX,mouseY)
               }
               onWheel: {
                   if(!(mapCanvas.addMidpoint || mapCanvas.editMidpoint || mapCanvas.removeMidpoint || mapCanvas.setRobotPosition)){
                       if (wheel.angleDelta.y>0)
                           zoom+=0.1
                       else
                           zoom+=-0.1

                       if(zoom<0.5)
                           zoom=0.5

                       var zoomItem

                       if(display.state=="maximized")
                           zoomItem=workspace
                       else{
                           zoomItem=drawMapWindow
                           if(zoom*mapCanvas.width<=zoomItem.width||zoom*mapCanvas.height<=zoomItem.height)
                               zoom+=0.1
                       }

                       if(zoom*mapCanvas.width<=zoomItem.width)
                           scrollMap.flickableItem.contentX=-zoom*mapCanvas.width/2+mapCanvas.width/2
                       else{
                           if(wheel.angleDelta.y<0)
                           {
                               scrollMap.flickableItem.contentX-=(scrollMap.flickableItem.contentX-(-zoomItem.width/2+mapCanvas.width/2))/(10*(zoom-zoomItem.width/mapCanvas.width+0.2))
                           }
                       }

                       if(zoom*mapCanvas.height<=zoomItem.height)
                           scrollMap.flickableItem.contentY=-zoom*mapCanvas.height/2+mapCanvas.height/2
                       else{
                           if(wheel.angleDelta.y<0)
                           {
                               scrollMap.flickableItem.contentY-=(scrollMap.flickableItem.contentY-(-zoomItem.height/2+mapCanvas.height/2))/(10*(zoom-zoomItem.height/mapCanvas.height+0.2))
                           }
                       }
                       flickX=scrollMap.flickableItem.contentX
                       flickY=scrollMap.flickableItem.contentY

                       //drawMapWindow.maxSizeChanged(zoom*mapCanvas.width,zoom*mapCanvas.height)
                   }
               }
           }
           Connections{
               target: menuPanel
               onSignalAddMidpoint: {
                   if(on){
                       if(mapCanvas.editMidpoint&&mapCanvas.addMidpointEnable)
                       {
                           mapCanvas.signalAddIndexMidpoint(firstPointX,firstPointY,fiRadian, mapCanvas.indexOfEditMidpoint)

                       }
                       mapCanvas.addMidpoint=true
                       mapCanvas.editMidpoint=false
                       mapCanvas.setRobotPosition=false
                       mapCanvas.setRobotPositionEnable=false
                       mapCanvas.editMidpointEnable=false
                   }
                   else{
                       mapCanvas.addMidpoint=false
                       mapCanvas.addMidpointEnable=false
                   }
               }
               onSignalRemoveMidpoint: {
                   if(on){
                       if(mapCanvas.editMidpoint&&mapCanvas.addMidpointEnable)
                       {
                           mapCanvas.signalAddIndexMidpoint(firstPointX,firstPointY,fiRadian, mapCanvas.indexOfEditMidpoint)

                       }
                       mapCanvas.addMidpoint=false
                       mapCanvas.removeMidpoint=true
                       mapCanvas.addMidpointEnable=false
                       mapCanvas.setRobotPosition=false
                       mapCanvas.setRobotPositionEnable=false
                       mapCanvas.editMidpoint=false
                       mapCanvas.editMidpointEnable=false
                   }
                   else
                       mapCanvas.removeMidpoint=false
               }
               onSignalRemoveAllMidpoint: {
                   if(mapCanvas.editMidpoint&&mapCanvas.addMidpointEnable)
                   {
                       mapCanvas.signalAddIndexMidpoint(firstPointX,firstPointY,fiRadian, mapCanvas.indexOfEditMidpoint)

                   }
                   mapCanvas.addMidpoint=false
                   mapCanvas.removeMidpoint=false
                   mapCanvas.addMidpointEnable=false
                   mapCanvas.setRobotPosition=false
                   mapCanvas.setRobotPositionEnable=false
                   mapCanvas.editMidpoint=false
                   mapCanvas.editMidpointEnable=false
               }
               onSignalEditMidpoint: {
                   if(on){
                       mapCanvas.editMidpoint=true
                       mapCanvas.addMidpoint=false
                       mapCanvas.addMidpointEnable=false
                       mapCanvas.addMidpoint=false
                       mapCanvas.removeMidpoint=false
                       mapCanvas.setRobotPosition=false
                       mapCanvas.setRobotPositionEnable=false
                   }
                   else{
                       if(mapCanvas.addMidpointEnable==true)
                       {
                           mapCanvas.signalAddIndexMidpoint(firstPointX,firstPointY,fiRadian, mapCanvas.indexOfEditMidpoint)

                       }

                       mapCanvas.editMidpoint=false
                       mapCanvas.editMidpointEnable=false
                       mapCanvas.addMidpointEnable=false
                   }

               }
               onSignalSetRobotPosition:{
                   if(on)
                   {
                       mapCanvas.setRobotPosition=true
                       if(mapCanvas.editMidpoint&&mapCanvas.addMidpointEnable)
                       {
                           mapCanvas.signalAddIndexMidpoint(firstPointX,firstPointY,fiRadian, mapCanvas.indexOfEditMidpoint)

                       }
                       mapCanvas.editMidpoint=false
                       mapCanvas.editMidpointEnable=false
                       mapCanvas.addMidpointEnable=false
                       mapCanvas.addMidpoint=false
                       mapCanvas.removeMidpoint=false

                   }
                   else
                   {
                       mapCanvas.setRobotPosition=false
                       mapCanvas.setRobotPositionEnable=false
                   }


               }
           }
           Connections{
               target: loader.item
               onUpdateMapWindow: {
                   mapCanvas.width=client.mapWidth;
                   mapCanvas.height=client.mapHeight;
                   mapCanvas.requestPaint();
               }
               onRePaintWindow:{
                   mapCanvas.requestPaint();
               }
               onUpdateScrollPosition:{
                   flickXInit=x
                   flickYInit=y
                   zoom=z
               }
               onSignalEditMidpoints:{
                   mapCanvas.editMidpointRadian=client.editPointIndex[0]
                   mapCanvas.indexOfEditMidpoint=client.editPointIndex[1]
                   mapCanvas.editMidpointEnable=true
                   mapCanvas.addMidpointEnable=true
                   mapCanvas.requestPaint()
               }
           }
        }
    }

    Component.onCompleted: {
        dockMe.connect(display.dockWindow)
        mapCanvas.signalAddMidpoint.connect(loader.item.addMidpoint)
        mapCanvas.signalRemoveMidpoint.connect(loader.item.removeMidpoint)
        mapCanvas.signalAddIndexMidpoint.connect(loader.item.addIndexMidpoints)
        mapCanvas.signalSetRobotPosition.connect(loader.item.setRobotPosition)
        setScrollPosition.connect(loader.item.setScrollPosition)
        closingWindow.connect(display.closingWindow)
    }

    onYChanged: {
        drawMapWindow.point = Qt.point(drawMapWindow.x-mainWindow.x,drawMapWindow.y-mainWindow.y)
    }

    onXChanged: {
        drawMapWindow.point = Qt.point(drawMapWindow.x-mainWindow.x,drawMapWindow.y-mainWindow.y)

    }
    onActiveChanged: {
        if(workspace.contains(drawMapWindow.point)){
            drawMapWindow.dockMe(drawMapWindow.width,drawMapWindow.height)
            drawMapWindow.setScrollPosition(scrollMap.flickableItem.contentX,scrollMap.flickableItem.contentY,zoom)
        }
        if(!active&&partMax){
            drawMapWindow.hide()
            drawMapWindow.width=lastWidth
            drawMapWindow.height=lastHeight
            drawMapWindow.showNormal()
            drawMapWindow.partMax=false
        }
    }
    onVisibilityChanged: {
        if(visibility==Window.Hidden)
            closingWindow()
        else{
            if(visibility==Window.Maximized)
            {
                counter=2

                if(zoom==0){
                    oldZoom=1
                }
                else
                    oldZoom=zoom

                oldFlickX=flickX
                oldFlickY=flickY

                state=2
            }
            if(visibility==Window.Windowed)
            {
                if(!drawMapWindow.partMax)
                {
                    zoom=oldZoom
                    if(state==1)
                        state=0
                }
            }
            if(visibility==3)
            {
                oldZoom=zoom
                oldFlickX=flickX
                oldFlickY=flickY
            }
        }
    }
    onWidthChanged: {
        resizeCallback(width,height)
    }
    onHeightChanged: {
        resizeCallback(width,height)
    }

    onAfterRendering: {
        if(counter==0)
        {
            scrollMap.flickableItem.contentX=flickXInit
            scrollMap.flickableItem.contentY=flickYInit
            flickX=flickXInit
            flickY=flickYInit
            counter--
        }
        if(state==2)
        {
            scrollMap.flickableItem.contentX=scrollMap.flickableItem.contentWidth/2-scrollMap.flickableItem.width/2
            scrollMap.flickableItem.contentY=scrollMap.flickableItem.contentHeight/2-scrollMap.flickableItem.height/2
            state--
            flickX=scrollMap.flickableItem.contentX
            flickY=scrollMap.flickableItem.contentY
        }
        if(state==0)
        {
            scrollMap.flickableItem.contentX=oldFlickX
            scrollMap.flickableItem.contentY=oldFlickY
            state--
            flickX=scrollMap.flickableItem.contentX
            flickY=scrollMap.flickableItem.contentY
        }
    }
}


