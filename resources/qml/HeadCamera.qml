import QtQuick 2.5
import QtQml.Models 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0

Item{
    property var workspace
    property int firstPointX
    property int firstPointY
    property int lastPointX
    property int lastPointY
    property double fiRadian
    property double scale: 1
    width:200
    height: width
    id:headCamera

    signal updateHeadCameraWindow()
    signal updateHeadPositionWindow()
    signal maxSizeChanged()

    function moveHeadWindow(vertical,horizontal)
    {
        image.signalMoveHead(vertical,horizontal)
    }

    function baseHeadWindow()
    {
        image.signalBaseHead()
    }

    function initWindow(){

    }
    function moveAbsHeadWindow(vertical,horizontal)
    {
        image.signalMoveAbsHead(vertical,horizontal)
    }

    function resize()
    {
        if(scale>1.3)
            scale=1.3
        content.width=scale*80
        circle.x=content.width/2-circle.width/2-client.headPosition[0]*(content.width-circle.width)/4//TODO mozliwy obrot o dany kat "4"
        circle.y=content.height/2-circle.height/2-client.headPosition[1]*(content.height-circle.height)/3
    }

    Image{
        id: image
        anchors.fill: parent
        source: "image://headCamera/camera"
        cache: false
        objectName: "headcamera"

        function updateHeadCamera()
        {
            var oldSource = source;
            source = "";
            source = oldSource;
            headCamera.updateHeadCameraWindow()
        }

        function updateHeadPosition()
        {
            image.update()
            headCamera.updateHeadPositionWindow()
        }

        signal signalMoveHead(double vertical, double horizontal)
        signal signalBaseHead()
        signal signalMoveAbsHead(double vertical, double horizontal)
        signal update()

        MouseArea{
            anchors.fill: parent
            onPressed: {
                firstPointX=mouseX
                firstPointY=mouseY
                lastPointX=mouseX
                lastPointY=mouseY
            }
            onPositionChanged: {
                lastPointX=mouseX
                lastPointY=mouseY
                image.signalMoveHead((lastPointX-firstPointX),(lastPointY-firstPointY))
                firstPointX=mouseX
                firstPointY=mouseY
            }

            onDoubleClicked:{
                image.signalBaseHead()
            }

            onWheel: {

            }
        }
    }
    Rectangle{
       id:content
       width: 80
       height: width
       radius: width/10
       x: 5
       y: 5

       color: Qt.rgba(1, 0, 0, 0.6)

       Rectangle{
           id: circle
           width: content.width/5
           height: width
           color: Qt.rgba(1, 1, 0, 0.6)
           radius: width*0.5
           x:content.width/2-width/2
           y:content.height/2-height/2
           z:2
       }
       Rectangle{
           height: parent.height
           width: 1
           anchors.verticalCenter: parent.verticalCenter
           anchors.horizontalCenter: parent.horizontalCenter
           z:-1
       }
       Rectangle{
           height: 1
           width: parent.width
           anchors.verticalCenter: parent.verticalCenter
           anchors.horizontalCenter: parent.horizontalCenter
           z:-1
       }
       Connections{
           target: image
           onUpdate:{
               circle.x=content.width/2-circle.width/2-client.headPosition[0]*(content.width-circle.width)/4//TODO mozliwy obrot o dany kat "4"
               circle.y=content.height/2-circle.height/2-client.headPosition[1]*(content.height-circle.height)/3
           }
       }
       MouseArea{
           anchors.fill: parent
           onPressed: {
               image.signalMoveAbsHead(4*(content.width/2-circle.width/2-mouseX+circle.width/2)/(content.width-circle.width),3*(content.height/2-circle.height/2-mouseY+circle.height/2)/(content.height-circle.height))
           }

       }
    }
    onWidthChanged:{
        if(width<height)
            scale=width/200
        resize()

    }
    onHeightChanged:{
        if(height<width)
            scale=height/200
        resize()
    }
}
