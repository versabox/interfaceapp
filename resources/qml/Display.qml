import QtQuick 2.5
import QtQuick.Window 2.2

Item {
    id: displayDock
    property alias title: display.title
    property alias content: loader.sourceComponent
    property alias iconSource: icon.source
    property alias state: display.state
    property alias smallWidth: display.smallWidth
    property alias smallHeight: display.smallHeight
    property var workspace
    property string nameOfWindow
    property bool stable: false

    signal getMeOnTop()
    signal maximizeMe()
    signal sizeChanged(int w,int h)

    y:6

    Image {
        id: dockIcon
        anchors.fill: displayDock
        source: iconSource
        opacity: 0.5
        fillMode: Image.PreserveAspectFit
    }

    MouseArea {
        id: docker
        anchors.fill: displayDock
        onDoubleClicked: {
            if (display.state === "docked"){
                displayDock.maximizeMe()
            }
            else{
                display.state = "docked"
                if(display.objectNewWindow!=null)
                    display.objectNewWindow.destroy(1)
            }
        }
        onPressed:{
            if(stable&&display.state=="stable"){
                display.state = "docked"
                maximizeMe()
            }
            getMeOnTop()
        }
    }

    Item {
        id: display
        property string title
        property int smallWidth: 200 //TODO: set this proportionally to maximized
        property int smallHeight: 300 //TODO: set this proportionally to maximized
        property int smallX: 0
        property int smallY: 0
        property int maxWidth: 5000
        property int maxHeight: 5000

        property var objectOffScreen
        property var objectNewWindow

        signal cordinateChanged(int x, int y)

        function dockWindow(w,h)
        {
            display.state=""

            var point=Qt.point(Math.abs(objectNewWindow.x-mainWindow.x)- displayBox.x - displayDock.x ,Math.abs(objectNewWindow.y-mainWindow.y)- workspace.height + displayDock.y + client.titleBarHeight + bar.height)
            display.x=point.x
            display.y=point.y
            display.width=w
            display.height=h+bar.height

            if(point.x>workspace.x + workspace.width - displayBox.x - displayDock.x - display.width){
                display.x=workspace.x + workspace.width - displayBox.x - displayDock.x - display.width
            }
            if(point.y>workspace.y + workspace.height - displayBox.y - displayDock.y - display.height){
                display.y=workspace.y + workspace.height - displayBox.y - displayDock.y - display.height
            }
            if(point.y<-(displayBox.y + displayDock.y - workspace.y)){
                display.y=-(displayBox.y + displayDock.y - workspace.y)
            }

            if(displayDock.contains(point)){
                display.state="docked"
            }
            display.objectNewWindow.destroy(1)
        }

        function offScreen(point)
        {
            var margin=100
            if(point.x<-margin||point.y<-margin-client.titleBarHeight||point.x>workspace.width+margin)
                return true
            else
                return false
        }

        function closingWindow(){
            if(display.state=="offscreen")
            {
                display.state="docked"
            }
        }


        Rectangle {
            id: bar
            height: 15
            color: "#FF999999"

            Text {
                id: barTitle
                anchors { fill: bar; leftMargin: icon.width }
                text: display.title
                font.pixelSize: bar.height
                elide: Text.ElideRight
            }

            MouseArea {
                id: dragger
                anchors.fill: parent
                drag.target: display
                drag.axis: Drag.XAndYAxis
                drag.maximumX: workspace.x + workspace.width - displayBox.x - displayDock.x - display.width
                drag.maximumY: workspace.y + workspace.height - displayBox.y - displayDock.y - display.height
                drag.minimumX: -(displayBox.x + displayDock.x - workspace.x)
                drag.minimumY: -(displayBox.y + displayDock.y - workspace.y)

                onPressed: {
                    if (display.state == "" || display.state == "docked")
                        displayDock.getMeOnTop();
                }

                onPositionChanged: {
                    if(nameOfWindow!=""){
                        var point = workspace.mapFromItem(dragger,mouseX,mouseY)

                        if(display.offScreen(point)){
                            var component
                            if(display.state!="offscreen"){
                                component = Qt.createComponent('OffScreen.qml')
                                display.objectOffScreen=component.createObject(display,
                                                       {"x": point.x-25,
                                                           "y": point.y,
                                                           "width": display.width,
                                                           "height": display.height,
                                                            "title": "Offscreen"})
                            }
                            display.cordinateChanged(point.x+mainWindow.x,point.y+mainWindow.y)
                            display.state="offscreen"
                        }
                        else{
                            if(workspace.contains(point)){
                                if(display.objectOffScreen != null){
                                    display.objectOffScreen.destroy()
                                    display.state=""
                                }
                            }
                            else{
                                if(display.state=="offscreen")
                                    display.cordinateChanged(point.x+mainWindow.x,point.y+mainWindow.y)
                            }
                        }
                    }
                }

                onReleased: {
                    if(nameOfWindow!=""){
                        var point = workspace.mapFromItem(dragger,mouseX,mouseY)

                        if(display.state=="offscreen")
                        {
                            var component = Qt.createComponent(nameOfWindow)
                            display.objectNewWindow=component.createObject(display,
                                                   {"x": point.x+mainWindow.x-25,
                                                       "y": point.y+mainWindow.y,
                                                       "width": display.width,
                                                       "height": display.height-bar.height,
                                                        "workspace": displayDock.workspace})
                            if(point.y+mainWindow.y===0){
                                display.objectNewWindow.showMaximized()
                            }
                            if(point.x+mainWindow.x===0||point.x+mainWindow.x==Screen.width-1){
                                display.objectNewWindow.partlyMaximize()
                            }
                            display.objectOffScreen.destroy()
                            loader.item.initWindow()
                       }
                    }
                }

                onDoubleClicked: {
//                    if(stable)
//                        display.state="stable"
//                    else
                        displayDock.maximizeMe()
                }

                states: [
                    State {
                        name: "dragged"
                        when: dragger.drag.active
                        ParentChange {
                            target: dragger
                            parent: displayDock
                        }
                    }
                ]
            }
        }

        Image {
            id: icon
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
        }

        Loader {
            id: loader
        }



        MouseArea {
            property var oldMouseX
            property var oldMouseY

            id: draggerSizeChange
            x: display.width-20
            y: display.height-20
            height: 40
            width: 40
            cursorShape: {
                if(!stable&&display.state!="docked") Qt.SizeFDiagCursor
            }

            onPressed: {
                oldMouseX = mouseX
                oldMouseY = mouseY
            }

            onPositionChanged: {
                if (pressed&&!stable) {
                    var w=display.width + (mouseX - oldMouseX)
                    var h=display.height + (mouseY - oldMouseY)
                    if(w>=icon.width&&w<=display.maxWidth)
                        display.width = w
                    if(h>=icon.height&&h<=display.maxHeight+bar.height)
                        display.height = h
                }
            }
        }

        onXChanged: {
            if (state == "" && dragger.drag.active)
                smallX = display.x
        }
        onYChanged: {
            if (state == "" && dragger.drag.active)
                smallY = display.y
        }
        onWidthChanged: {
            if (state == "")
                smallWidth = display.width
            displayDock.sizeChanged(display.width,display.height)
        }
        onHeightChanged: {
            if (state == "")
                smallHeight = display.height
            displayDock.sizeChanged(display.width,display.height)
        }

        states: [
            State {
                name: "docked"
                when: {
                    var point = dragger.mapToItem(displayDock, dragger.mouseX, dragger.mouseY)
                    displayDock.contains(point)
                }

                PropertyChanges { target: icon; visible: true; height: displayDock.height; width: displayDock.width;}
                PropertyChanges { target: bar; visible: true; height: displayDock.height+10; width: displayDock.width+10; color:"#00DDDDDD"; radius:30 }
                PropertyChanges { target: barTitle; text: icon.width}
                PropertyChanges { target: loader; visible: false}

                AnchorChanges {
                    target: display;
                    anchors { top: displayDock.top; left: displayDock.left; right: displayDock.right; bottom: displayDock.bottom }
                }
                AnchorChanges {
                    target: icon;
                    anchors { verticalCenter: display.verticalCenter; horizontalCenter: display.horizontalCenter }
                }

            },
            State {
                name: "" //default state - small window
                PropertyChanges { target: icon; visible: true; height: bar.height; width: bar.height;}
                PropertyChanges { target: bar; visible: true ; color:"#FF555555"; radius:0}
                PropertyChanges { target: loader; visible: true; }
                PropertyChanges { target: dockIcon; z:2}

                AnchorChanges {
                    target: bar;
                    anchors { top: display.top; left: display.left; right: display.right }
                }
                AnchorChanges {
                    target: display;
                    anchors { top: undefined; left: undefined; right: undefined; bottom: undefined}
                }
                AnchorChanges {
                    target: icon;
                    anchors { verticalCenter:bar.verticalCenter; horizontalCenter:bar.left} //top: bar.top; left: bar.left}
                }
                AnchorChanges {
                    target: loader;
                    anchors { top: bar.bottom; left: display.left; right: display.right; bottom: display.bottom}
                }

                PropertyChanges { target: display; width: smallWidth; height: smallHeight; x: smallX; y: smallY } // Unfortuately the order does matter.. :-/
            },
            State {
                name: "maximized"
                PropertyChanges { target: icon; visible: false}
                PropertyChanges { target: bar; visible: false}
                PropertyChanges { target: loader; visible: true}


                ParentChange {
                    target: display
                    parent: workspace
                }

                AnchorChanges {
                    target: loader;
                    anchors { top: display.top; left: display.left; right: display.right; bottom: display.bottom}
                }
                AnchorChanges {
                    target: display;
                    anchors { top: workspace.top; left: workspace.left; right: workspace.right; bottom: workspace.bottom}
                }
            },
            State {
                name: "offscreen"
                PropertyChanges { target: icon; visible: false}
                PropertyChanges { target: bar; visible: false}
                PropertyChanges { target: loader; visible: false}
                PropertyChanges { target: display; visible: false}
            },
            State {
                name: "stable"
                PropertyChanges { target: icon; visible: false}
                PropertyChanges { target: bar; visible: false}
                PropertyChanges { target: loader; visible: true}
                PropertyChanges { target: display; width:smallWidth; height:smallHeight}

                ParentChange {
                    target: display
                    parent: workspace
                }

                AnchorChanges {
                    target: loader;
                    anchors { top: display.top; left: display.left; right: display.right; bottom: display.bottom}
                }
                AnchorChanges {
                    target: display;
                    anchors { top: undefined; left: workspace.left; right: undefined; bottom: workspace.bottom}
                }
            }
        ]

        Connections{
            target: loader.item
            onMaxSizeChanged: {
                display.maxWidth=w
                display.maxHeight=h
            }
        }
    }
}



