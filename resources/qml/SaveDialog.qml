import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Dialog {
    width: 150
//    height: descriptionText ? 150 : 70
    visible: true
    title: "" //set it when create widnow

    property bool nameText: true
    property bool descriptionText: false
    property bool typeVisible: false
    property bool warningText: false
    property string nameShadowText: qsTr(client.translateIntoSelectedLanguage("EnterName", language))
    property string nameShadowDescription: qsTr(client.translateIntoSelectedLanguage("EnterDescription", language))
    property string warningString: qsTr("")
    property string oldNameText: ""
    property string oldDescriptionText: ""
    property var connectAcceptFunction
    property var connectRejectFunction
    property var typeModel: []

    standardButtons: StandardButton.Cancel | StandardButton.Ok

    signal signalAccept(string name, string description, int type)
    signal signalReject()

    Text {
        id: warning
        anchors.margins: 10
        visible: warningText
        width: parent.width
        height: warningText ? 30 : 0
        text: warningString
    }

    TextField{
        id: name
        anchors.margins: 10
        visible: nameText
        width: parent.width
        height: nameText ? 30 : 0
        focus: true
        placeholderText: nameShadowText
    }

    TextField{
        id: description
        anchors.top: name.bottom
        anchors.margins: 10
        visible: descriptionText
        width: parent.width
        height: descriptionText ? 60 : 0
        placeholderText: nameShadowDescription
    }

    ComboBox {
        id: type
        anchors.top: description.bottom
        anchors.margins: 10
        visible: typeVisible
        width: parent.width
        height: typeVisible ? 30 : 0
        model: typeModel
    }

    onAccepted: {

        signalAccept(name.text, description.text, type.currentIndex)
    }
    onRejected: {

        signalReject()
    }

    Component.onCompleted: {
        signalAccept.connect(connectAcceptFunction)
        if(connectRejectFunction){

            signalReject.connect(connectRejectFunction)
        }
        if(oldNameText !== ""){

            name.text = oldNameText
        }
        if(oldDescriptionText !==""){

            description.text = oldDescriptionText
        }
        name.forceActiveFocus()
    }
}
