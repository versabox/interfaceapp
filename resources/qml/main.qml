import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Window {
    id: mainWindow
    visible: true
    color: "#FFDDDDDD"

    width: 800
    height: 600
    signal signalSetKeyboardSpeed(double linear,double angular)
    signal signalKeyboardEnable()
    signal signalKeyboardDisable()
    signal signalHeadCameraMoveKeyboard(double horizontal,double vertical)

    Rectangle {
        id: workspace
        color: "transparent"
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        property int turn: 0
        property int linear: 0
        property double speed: 0.2
        property double headCameraVertical: 0.0
        property double headCameraHorizontal: 0.0
        property bool keyBoardActive: false

        focus: true
        Keys.enabled: true
        onActiveFocusChanged: {

            if(!active){
                mainWindow.signalKeyboardDisable()
                keyBoardActive = false
            }
        }
        Keys.onPressed:{
            if (event.isAutoRepeat){
                return ;
            }
            if (event.key === Qt.Key_Left) {
                turn=1;
                event.accepted = true;
            }
            if(event.key === Qt.Key_Right){
                turn=-1;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Up) {
                linear=1;
                event.accepted = true;
            }
            if(event.key === Qt.Key_Down){
                linear=-1;
                event.accepted = true;
            }
            if (event.key === Qt.Key_A) {
                headCameraHorizontal=2;
                event.accepted = true;
            }
            if(event.key === Qt.Key_D){
                headCameraHorizontal=-2;
                event.accepted = true;
            }
            if (event.key === Qt.Key_W) {
                headCameraVertical=1.5;
                event.accepted = true;
            }
            if(event.key === Qt.Key_S){
                headCameraVertical=-1.5;
                event.accepted = true;
            }

            if(event.key === Qt.Key_Shift){
                event.accepted = true;
                if(speed == 0.2)
                    speed=0.5
                else if(speed == 0.5)
                    speed=1
                else
                    speed=0.2
            }
            if(event.accepted && active){

                if(!keyBoardActive){
                    mainWindow.signalKeyboardEnable()
                    keyBoardActive = true
                }
                mainWindow.signalSetKeyboardSpeed(linear*speed, turn*speed)
                mainWindow.signalHeadCameraMoveKeyboard(headCameraHorizontal,headCameraVertical)
            }
        }
        Keys.onReleased:{
            if (event.isAutoRepeat)
                return ;
            if (event.key === Qt.Key_A) {
                headCameraHorizontal=0.0;
                event.accepted = true;
            }
            if(event.key === Qt.Key_D){
                headCameraHorizontal=0.0;
                event.accepted = true;
            }
            if (event.key === Qt.Key_W) {
                headCameraVertical=0.0;
                event.accepted = true;
            }
            if(event.key === Qt.Key_S){
                headCameraVertical=0.0;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Left) {
                turn=0;
                event.accepted = true;
            }
            if(event.key === Qt.Key_Right){
                turn=0;
                event.accepted = true;
            }
            if (event.key === Qt.Key_Up) {
                linear=0;
                event.accepted = true;
            }
            if(event.key === Qt.Key_Down){
                linear=0;
                event.accepted = true;
            }
            if(event.accepted && active){
                mainWindow.signalSetKeyboardSpeed(linear*speed, turn*speed)
                mainWindow.signalHeadCameraMoveKeyboard(headCameraHorizontal,headCameraVertical)

                if(linear == 0 && turn == 0 ){
                    mainWindow.signalKeyboardDisable()
                    keyBoardActive = false
                    speed=0.2
                }
            }
        }

        Image {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            id: vbIcon
            height: 276
            width: 420
            source: "qrc:/images/VBIcon.png"
        }
    }

    StatusPanel {
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
    }

    MenuPanel {
        id: menuPanel
        anchors.right: parent.right
        anchors.top: parent.top
        height: parent.height
        workspaceHeight: parent.height
        z: 1
    }

    DisplayBox {
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        workspace: workspace
    }
}
