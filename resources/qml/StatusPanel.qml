import QtQuick 2.0

Rectangle {
    id:statusPanelContent
    width: box.width + 20
    height: box.height + 20
    color: "#a1a1a1"
    objectName: "statusPanelContent"

    function batteryState(low, critical){
        var power=client.batteryPower

        battery.color = "#FFEBEBEB"
        if(low) {
            battery.color = "#FFFF00"
        }
        if(critical) {
            battery.color = "#FF0000"
        }

        if(power >=0){
            if(power>85){
                batteryRect.state = "battery4"
            }
            else if(power>65){
                batteryRect.state = "battery3"
            }
            else if(power>45){
                batteryRect.state = "battery2"
            }
            else if(power>25){
                batteryRect.state = "battery1"
            }
            else{
                batteryRect.state = "battery0"
            }

            battery.textItem="  " + power + "  %"
        }
        else if(power === -1){
            batteryRect.state = "charging"
            battery.textItem="charging"
        }
    }

    Column {
        id: box
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        function hideAllExcept(exceptId)
        {
            for (var i = 0; i < box.children.length; ++i)
                if (box.children[i].self !== exceptId)
                    box.children[i].hideDescription()
        }

        StatusItem {
            id:battery
            property string self: "battery"
            onDescriptionShown:box.hideAllExcept(self)
            textHeight: 20
            textWidth: 70
            animationItem: "qrc:/images/battery.gif"
            colorBox: "#ffebebeb"
            Rectangle{
                id:batteryRect
                state: "charging"
                states: [
                    State {
                        name:"charging"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery.gif" }
                    },
                    State {
                        name:"battery0"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery0.gif" }
                    },
                    State {
                        name:"battery1"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery1.gif" }
                    },
                    State {
                        name:"battery2"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery2.gif" }
                    },
                    State {
                        name:"battery3"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery3.gif" }
                    },
                    State {
                        name:"battery4"
                        PropertyChanges {target:battery; animationItem:"qrc:/images/battery4.gif" }
                    }
                ]
            }
        }
    }
}

