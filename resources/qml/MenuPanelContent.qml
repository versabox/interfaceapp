import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.2
import "qrc:/createPushButton.js" as CreateScript

Rectangle{
    id: menuPanelContent
    property var workspaceHeight
    property int ile:0
    property var win
    property int buttonWidth: 175
    property string language: "english"

    color: "gray"
    objectName: "menuPanelContent"
    width: parent.width
    height: parent.height

    function updateListOfMaps()
    {
        mapList.clear()
        for(var i=0;i<client.listOfMaps.length;i++){
            mapList.append({text: client.listOfMaps[i]})
        }
    }
    function refreshKeyPoseList() {
        CreateScript.refreshComponentList()
    }
    function addKeyPoseNow(name, description, typeOfKeyPose, typeOfTimeSet, time){

        menuPanelContent.signalAddKeyPoseAtRobotPosition(name, description, typeOfKeyPose, typeOfTimeSet, time)
        win.destroy()
    }
    function saveMapNow(name){

        menuPanel.saveMap(name)
        win.destroy()
    }
    function liftingUp(){

        if(liftUp.clicked){

            liftUp.clicked = false
        }
        else{

            liftUp.clicked = true
        }
    }

    function liftingDown(){

        if(liftDown.clicked){

            liftDown.clicked = false
        }
        else{

            liftDown.clicked = true
        }
    }

    function liftStatus(action, state){

        if(state == 0){

            if(action == 2){

                liftingUp()
                liftDown.clicked = false
            }
            else if(action == 1){

                liftingDown()
                liftUp.clicked = false
            }
        }
        else  if(state == 2){

            liftUp.clicked = true
            liftDown.clicked = false
        }
        else if(state == 1){

            liftUp.clicked = false
            liftDown.clicked = true
        }
    }

    function updateListOfKeyPosePath(){

        keyPosePathList.clear()
        for(var i = 0; i < client.keyPosePathList.length; i++){

            keyPosePathList.append({text: client.keyPosePathList[i]})
        }
    }

    function saveMidKeyPoseGraphAccept(){

        menuPanelBar.addMidKeyPoseGraph(false, true)
    }

    function saveMidKeyPoseGraphReject(){

        menuPanelBar.addMidKeyPoseGraph(false, false)
    }

    signal signalSetCameraSteeringPose(double horizontal, double vertical)
    signal signalTurnOnLaser(bool on)
    signal signalChangeMidpointsTolerance(double xy, double angle)
    signal signalLift(bool on)
    signal signalNewKeyPosePath(string name)
    signal signalStartPath(string name)
    signal signalAddKeyPoseAtRobotPosition(string name, string description, int typeOfKeyPose, int typeOfTimeSet, int time)
    signal signalLockerAction(int on)
    signal signalBrakeModeChanged(bool on)
    signal signalSuspendTaskQueue()
    signal signalUnsuspendTaskQueue()
    signal signalAbortTask()
    signal signalClearTaskQueue()


   Flickable {
        id: scrollMenuPanel
        anchors.fill: parent
        contentHeight: layout.height + 20
        boundsBehavior: Flickable.StopAtBounds

        MouseArea{
            anchors.fill: parent
            onWheel: {

            }
        }

        ColumnLayout{
            id: layout
            spacing: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.bottomMargin: 10

            Text{
                id:textMenuPanel
                text: client.translateIntoSelectedLanguage("Choose_Language", language)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            PushButton{
                id: polish
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "PL"
                onButtonClicked:{
                        polish.clicked = true
                        english.clicked = false

                        menuPanelBar.changeLanguage("polish")

                        language = "polish"
                        menuPanel.language = "polish"
                        contentKeyPosePath.item.language = "polish"

                    if(addMidKeyPoseGraph.clicked)
                    {
                        addMidKeyPoseGraph.text = client.translateIntoSelectedLanguage("Save_MidKeyPoseGraph", language)
                    }
                    else
                    {
                        addMidKeyPoseGraph.text =  client.translateIntoSelectedLanguage("Add_MidKeyPoseGraph", language)
                    }

                    if(turnOnLaser.clicked)
                    {
                        turnOnLaser.text = client.translateIntoSelectedLanguage("TurnOffLaser", language)
                    }
                    else
                    {
                        turnOnLaser.text =  client.translateIntoSelectedLanguage("TurnOnLaser", language)
                    }
                }
            }
            PushButton{
                id: english
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: "ENG"
                clicked: true
                onButtonClicked:{
                        english.clicked = true
                        polish.clicked = false

                        menuPanelBar.changeLanguage("english")

                        language = "english"
                        menuPanel.language = "english"
                        contentKeyPosePath.item.language = "english"

                    if(addMidKeyPoseGraph.clicked)
                    {
                        addMidKeyPoseGraph.text = client.translateIntoSelectedLanguage("Save_MidKeyPoseGraph", language)
                    }
                    else
                    {
                        addMidKeyPoseGraph.text =  client.translateIntoSelectedLanguage("Add_MidKeyPoseGraph", language)
                    }

                    if(turnOnLaser.clicked)
                    {
                        turnOnLaser.text = client.translateIntoSelectedLanguage("TurnOffLaser", language)
                    }
                    else
                    {
                        turnOnLaser.text =  client.translateIntoSelectedLanguage("TurnOnLaser", language)
                    }
                }
            }
            Text{
                text: client.translateIntoSelectedLanguage("Control_Panel", language)
                anchors.horizontalCenter: parent.horizontalCenter

            }
            PushButton{
                id: start
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25

                text: client.translateIntoSelectedLanguage("Start", language)

                onButtonClicked:{
                    menuPanelBar.startRobot()
                }
            }
            PushButton{
                id: stop
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25

                text: client.translateIntoSelectedLanguage("Stop", language)

                onButtonClicked:{
                    menuPanelBar.stopRobot()
                }
            }
            PushButton{
                id: setRobotPosition
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Set_Position", language)
                onButtonClicked:
                    if(!setRobotPosition.clicked){
                        deleteKeyPose.clicked=false
                        addKeyPose.clicked=false
                        setRobotPosition.clicked=true
                        menuPanelBar.setRobotPosition(true)
                    }else{
                        setRobotPosition.clicked=false
                        menuPanelBar.setRobotPosition(false)
                    }
            }
            Text{
                visible: false
                text: client.translateIntoSelectedLanguage("Set_type_of_ride", language)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ComboBox {
                id: rideStatus
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                model: [client.translateIntoSelectedLanguage("ToGoal", language),
                    client.translateIntoSelectedLanguage("SingleLoop", language),
                    client.translateIntoSelectedLanguage("InfiniteLoop", language),
                    client.translateIntoSelectedLanguage("RandomPoint", language)]
                onCurrentIndexChanged: {
                    menuPanelBar.changeRideMode(currentIndex)
                }
            }
            Text{
                text: client.translateIntoSelectedLanguage("Choose_Keypose_Path", language)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ComboBox {
                id: chooseKeyPosePath
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                model: ListModel{
                    id: keyPosePathList
                }
                onCurrentIndexChanged: {

                    menuPanelContent.signalNewKeyPosePath(chooseKeyPosePath.currentText)
                }
            }
            PushButton{
                id: startPath
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Start_Path", language)
                onButtonClicked:{
                    menuPanelContent.signalStartPath(chooseKeyPosePath.currentText)
                    setRobotPosition.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    menuPanelBar.removeMidpoint(false)
                    menuPanelBar.addMidpoint(false)
                }
            }
            RowLayout{
                spacing: 10
                anchors.left: parent.left
                anchors.leftMargin: 10

                Text {
                    text: qsTr(client.translateIntoSelectedLanguage("BrakeMode", language))
                }
                Switch{
                    id:brakeMode
                    checked: true
                    onCheckedChanged: signalBrakeModeChanged(brakeMode.checked)
                }
            }
            Rectangle{
                id:test
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }

            Text{
                id:textMapPanel
                text: client.translateIntoSelectedLanguage("Points_Panel", language)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            PushButton{
                id: suspendTaskQueue
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Suspend_task_queue", language)
                onButtonClicked:{
                    menuPanelContent.signalSuspendTaskQueue()
                }
            }
            PushButton{
                id: unsuspendTaskQueue
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Unsuspend_task_queue", language)
                onButtonClicked:{
                    menuPanelContent.signalUnsuspendTaskQueue()
                }
            }
            PushButton{
                id: abortTask
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Abort_current_task", language)
                onButtonClicked:{
                    menuPanelContent.signalAbortTask()
                }
            }
            PushButton{
                id: clearTaskQueue
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Clear_task_queue", language)
                onButtonClicked:{
                    menuPanelContent.signalClearTaskQueue()
                }
            }
            PushButton{
                id: addMidKeyPoseGraph
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: client.translateIntoSelectedLanguage("Add_MidKeyPoseGraph", language)
                onButtonClicked:{
                    if(!addMidKeyPoseGraph.clicked){
                        setRobotPosition.clicked=false
                        addMidKeyPoseGraph.clicked = true
                        addMidKeyPoseGraph.text = client.translateIntoSelectedLanguage("Save_MidKeyPoseGraph", language)
                        menuPanelBar.addMidKeyPoseGraph(true, false)
                    }
                    else{
                        addMidKeyPoseGraph.clicked = false
                        addMidKeyPoseGraph.text =  client.translateIntoSelectedLanguage("Add_MidKeyPoseGraph", language)
                        var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
                        win = component.createObject(menuPanelContent, {
                                                "title": client.translateIntoSelectedLanguage("MidKeyPoseGraph_Saver", language),
                                                "nameText": false,
                                                "descriptionText": false,
                                                "warningText": true,
                                                "warningString": client.translateIntoSelectedLanguage("DoYouWantToSaveChanges", language),
                                                "connectAcceptFunction": menuPanelContent.saveMidKeyPoseGraphAccept,
                                                "connectRejectFunction": menuPanelContent.saveMidKeyPoseGraphReject
                                                });
                    }
                }
            }
            RowLayout{
                spacing: 37
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visibleOfMidKeyPoseGraph
                    text: qsTr(client.translateIntoSelectedLanguage("Set_visible_of_graph", language))
                }
                PushButton{
                    id:visibleOfMidKeyPoseGraphPB
                    clicked: false
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    x: 130
                    onButtonClicked:{
                        if(!visibleOfMidKeyPoseGraphPB.clicked){

                            menuPanelBar.visibleOfMidKeyPoseGraph(true)
                        }
                        else{

                            menuPanelBar.visibleOfMidKeyPoseGraph(false)
                        }
                    }
                }
            }

            Rectangle{
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }

            Text{
                anchors.left: parent.left
                anchors.leftMargin: 20
                width: parent.width
                text: client.translateIntoSelectedLanguage("MidpointsTolerance", language)
            }

            RowLayout{
                spacing: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                Rectangle{
                    width: 40
                    color:"#00000000"
                    height:20
                    Text{
                        width:20
                        height: 20
                        text: "XY:"
                    }
                }
                SpinBox{
                    id: toleranceXY
                    decimals: 2
                    minimumValue: 0
                    stepSize: 0.5
                }
            }

            RowLayout{
                spacing: 20
                anchors.left: parent.left
                anchors.leftMargin: 20
                Rectangle{
                    width: 40
                    color:"#00000000"
                    height:20
                    Text{
                        width:20
                        height: 20
                        text: client.translateIntoSelectedLanguage("Angle", language)
                    }
                }
                SpinBox{
                    id: toleranceAngle
                    decimals: 2
                    minimumValue: 0
                    stepSize: 0.5
                }
            }

            PushButton{
                id: midpointsTolerance
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("SetTolerance", language)
                onButtonClicked:{
                    signalChangeMidpointsTolerance(toleranceXY.value,toleranceAngle.value)
                }
            }


            Rectangle{
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }
            Text{
                id:textArea2
                text: client.translateIntoSelectedLanguage("Map_Panel", language)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            ComboBox {
                id: mapChange
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                model: ListModel{
                    id: mapList
                }
                onPressedChanged: menuPanelBar.updateListOfMaps()
            }

            RowLayout{
                spacing: 37
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visualMap
                    text: qsTr(client.translateIntoSelectedLanguage("Visual_type_of_map", language))
                }
                PushButton{
                    id: visual
                    clicked: false
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    x: 130
                }
            }

            PushButton{
                id: loadMap
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Load_map", language)
                onButtonClicked:{
                    setRobotPosition.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    menuPanelBar.loadMap(mapChange.currentIndex, visual.clicked)
                }
            }
            PushButton{
                id: bulidMap
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Build_new_map", language)
                onButtonClicked:{
                    setRobotPosition.clicked=false
                    deleteKeyPose.clicked=false
                    addKeyPose.clicked=false
                    menuPanelBar.bulidNewMap()
                }
            }
            PushButton{
                id: saveMap
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Save_map", language)
                onButtonClicked:{
                    setRobotPosition.clicked=false
                    var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
                    win = component.createObject(menuPanelContent, {
                                            "title": client.translateIntoSelectedLanguage("KeyPose_Saver", language),
                                            "nameText": true,
                                            "descriptionText": false,
                                            "connectAcceptFunction": menuPanelContent.saveMapNow,
                                            });
                }
            }

            Rectangle{
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }
            Text{
                id:textArea1
                text: client.translateIntoSelectedLanguage("Keypose_Panel", language)
                anchors.horizontalCenter: parent.horizontalCenter
            }
            PushButton{
                id: addKeyPose
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Add_KeyPose", language)
                onButtonClicked:{
                    if(!addKeyPose.clicked){
                        setRobotPosition.clicked=false
                        deleteKeyPose.clicked=false
                        editKeyPose.clicked=false
                        addKeyPose.clicked=true
                        menuPanelBar.addKeyPose(true)
                    }else{
                        addKeyPose.clicked=false
                        menuPanelBar.addKeyPose(false)
                    }
                }
            }
            PushButton{
                id: addKeyPoseAtRobot
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: client.translateIntoSelectedLanguage("Add_KeyPose_at_Robot", language)
                onButtonClicked:{
                    var component = Qt.createComponent("qrc:/qml/KeyPoseSaveDialog.qml")
                    win = component.createObject(menuPanelContent, {
                                            "nameText": true,
                                            "descriptionText": true,
                                            "connectAcceptFunction": menuPanelContent.addKeyPoseNow,
                                            });
                    setRobotPosition.clicked=false
                    addKeyPose.clicked=false
                    deleteKeyPose.clicked=false
                    editKeyPose.clicked=false
                }
            }
            PushButton{
                id: editKeyPose
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Edit_KeyPose", language)
                onButtonClicked:{
                    if(!editKeyPose.clicked){
                        setRobotPosition.clicked=false
                        deleteKeyPose.clicked=false
                        addKeyPose.clicked=false
                        editKeyPose.clicked=true
                        menuPanelBar.editKeyPose(true)
                    }else{
                        editKeyPose.clicked=false
                        menuPanelBar.editKeyPose(false)
                    }
                }
            }
            PushButton{
                id: deleteKeyPose
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Delete_KeyPose", language)
                onButtonClicked:{
                    if(!deleteKeyPose.clicked){
                        setRobotPosition.clicked=false
                        addKeyPose.clicked=false
                        deleteKeyPose.clicked=true
                        menuPanelBar.deleteKeyPose(true)
                    }else{
                        deleteKeyPose.clicked=false
                        menuPanelBar.deleteKeyPose(false)
                    }
                }
            }
            PushButton{
                id: dockApproachFront
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Dock_approach_front", language)
                onButtonClicked:{
                    menuPanelBar.dockApproach(1);
                }
            }
            PushButton{
                id: dockApproachBack
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Dock_approach_back", language)
                onButtonClicked:{
                    menuPanelBar.dockApproach(-1);
                }
            }
            PushButton{
                id: dockDepartureFront
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Dock_departure_front", language)
                onButtonClicked:{
                    menuPanelBar.dockDeparture(1);
                }
            }
            PushButton{
                id: dockDepartureBack
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Dock_departure_back", language)
                onButtonClicked:{
                    menuPanelBar.dockDeparture(-1);
                }
            }
            PushButton{
                id: sideButton
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Finish_procedure", language)
                onButtonPressed: {
                    menuPanelBar.sideButtonClicked(true);
                }

                onButtonReleased: {
                    menuPanelBar.sideButtonClicked(false);
                }
            }
            PushButton{
                id: stopCharging
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Stop_charging", language)
                onButtonClicked:{
                    menuPanelBar.signalStopCharingButtonClicked();
                }
            }
            PushButton{
                id: skipButton
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Skip_KeyPose", language)
                onButtonClicked:{
                    menuPanelBar.skipButtonClicked();
                }
            }
            RowLayout{
                spacing: 37
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visibleOfAllKeyposeText
                    text: qsTr(client.translateIntoSelectedLanguage("Set_visible_of_KeyPose", language))
                }
                PushButton{
                    id:visibleOfAllKeypose
                    clicked: true
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    number_of_pushbutton: -1
                    x: 130
                }
            }
            Rectangle{
                id:keyPoseList
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: 10
                anchors.right: parent.right
                height: 1
                color: "#00000000"
                //border.color: "black"

            }
            Rectangle{
                id:cameraPanel
                anchors.top:keyPoseList.bottom
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }
            PushButton{
                id: cameraSteeringPose
                anchors.top: cameraPanel.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: client.translateIntoSelectedLanguage("CameraSteeringPose", language)
                onButtonClicked:{
                        setRobotPosition.clicked=false
                        addKeyPose.clicked=false
                        deleteKeyPose.clicked=false
                        menuPanelContent.signalSetCameraSteeringPose(0,-1.09)
                }
            }
            PushButton{
                id: turnOnLaser
                anchors.top: cameraSteeringPose.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: client.translateIntoSelectedLanguage("TurnOffLaser", language)
                clicked: true
                onButtonClicked:{
                    if(turnOnLaser.clicked){

                        turnOnLaser.clicked = false
                        menuPanelContent.signalTurnOnLaser(false)
                        turnOnLaser.text = client.translateIntoSelectedLanguage("TurnOnLaser", language)
                    }
                    else{

                        menuPanelContent.signalTurnOnLaser(true)
                        turnOnLaser.clicked = true
                        turnOnLaser.text = client.translateIntoSelectedLanguage("TurnOffLaser", language)
                    }

                }
            }
            PushButton{
                id: liftUp
                anchors.top: turnOnLaser.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Lift_up", language)
                onButtonClicked:{
                    menuPanelContent.signalLift(true)
                }
            }

            PushButton{
                id: liftDown
                anchors.top: liftUp.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Lift_down", language)
                onButtonClicked:{
                    menuPanelContent.signalLift(false)
                }
            }
            PushButton{
                id: lock
                anchors.top: liftDown.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Lock", language)
                onButtonClicked:{
                    menuPanelContent.signalLockerAction(0)
                }
            }

            PushButton{
                id: unlock
                anchors.top: lock.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("Unlock", language)
                onButtonClicked:{
                    menuPanelContent.signalLockerAction(1)
                }
            }
            PushButton{
                id: resetDiagnostics
                anchors.top: unlock.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 25
                text: client.translateIntoSelectedLanguage("ResetDiagnostics", language)
                onButtonClicked:{
                    menuPanel.resetDiagnosticsAction(true)
                }
            }
            PushButton{
                id: addWarningZone
                anchors.top: resetDiagnostics.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: client.translateIntoSelectedLanguage("Add_warning_zone", language)
                onButtonClicked:{
                    if(!addWarningZone.clicked){
                        setRobotPosition.clicked=false
                        addWarningZone.clicked = true
                        var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
                        win = component.createObject(menuPanelContent, {
                                                "title": "Choose warning zone type",
                                                "nameText": false,
                                                "descriptionText": false,
                                                "typeVisible": true,
                                                "connectAcceptFunction": menuPanel.addWarningZone,
                                                "typeModel": ["POLYGON", "CIRCLE"],
                                                });
                    }
                    else{

                        addWarningZone.clicked = false
                        menuPanel.stopEditing()
                    }
                }
            }
            PushButton{
                id: deleteWarningZone
                anchors.top: addWarningZone.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                text: client.translateIntoSelectedLanguage("Delete_warning_zone", language)
                onButtonClicked:{
                    if(!deleteWarningZone.clicked){
                        setRobotPosition.clicked=false
                        addWarningZone.clicked = false
                        deleteWarningZone.clicked = true
                        menuPanel.deleteWarningZone()
                    }
                    else{

                        deleteWarningZone.clicked = false
                        menuPanel.stopEditing()
                    }
                }
            }
            RowLayout{
                id:visibleOfWarningZone
                spacing: 17
                anchors.top: deleteWarningZone.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                Text {
                    id: visibleOfWarningZoneText
                    text: qsTr(client.translateIntoSelectedLanguage("Set_visible_of_warning_zone", language))
                }
                PushButton{
                    id:visibleOfWarningZonePB
                    clicked: false
                    width: 25
                    height: 25
                    dynamicCheckBox: true
                    text:""
                    number_of_pushbutton: -50
                    x: 130
                    onButtonClicked:{
                        if(!visibleOfWarningZonePB.clicked){

                            menuPanelBar.visibleOfWarningZone(true)
                        }
                        else{

                            menuPanelBar.visibleOfWarningZone(false)
                        }
                    }
                }
            }
            PushButton{
                id: addNoDrivingArea
                anchors.top: visibleOfWarningZone.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                width: buttonWidth
                height: 50
                visible: false
                text: "         Add \nNoDrivingArea"
                onButtonClicked:{
                    if(!addNoDrivingArea.clicked){
                        setRobotPosition.clicked=false
                        addNoDrivingArea.clicked = true
                        addNoDrivingArea.text = "         Save \nNoDrivingArea"
                        menuPanelBar.addNoDrivingArea(true)
                    }
                    else{

                        addNoDrivingArea.clicked = false
                        addNoDrivingArea.text =  "         Add \nNoDrivingArea"
                        menuPanelBar.addNoDrivingArea(false)
                    }
                }
            }

            Rectangle{
                id:last
                anchors.top: addNoDrivingArea.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.rightMargin: -10
                anchors.right: parent.right
                height: 2
                color: "black"
            }
        }

        states: [
            State {
                name:"shownMenu"
                PropertyChanges {target: scrollMenuPanel; contentY:textMenuPanel.y}
            },
            State {
                name:"shownMap"
                PropertyChanges { target: scrollMenuPanel; contentY:textMapPanel.y}
            },
            State {
                name:"shownArea2"
                PropertyChanges {target: scrollMenuPanel; contentY:textArea2.y}
            },
            State {
                name:"shownArea1"
                PropertyChanges { target: scrollMenuPanel; contentY:textArea1.y}
            },
            State {
                name:"top"
                PropertyChanges { target: scrollMenuPanel; contentY:0}
            },
            State {
                name:"bottom"
                PropertyChanges { target: scrollMenuPanel; contentY:scrollMenuPanel.contentHeight-scrollMenuPanel.height}
            }
        ]

        transitions: [
            Transition {
                    NumberAnimation { target:scrollMenuPanel; properties: "contentY" ; easing.type: Easing.OutExpo; duration: 1500 }
            }
        ]

        Connections{
            target: menuPanelBar
            onSignalShownMenu:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textMenuPanel.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownMenu"
                }
            }
            onSignalShownMap:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textMapPanel.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownMap"
                }
            }
            onSignalShownArea2:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textArea2.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownArea2"
                }
            }
            onSignalShownArea1:{
                if(layout.height<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="top"

                }
                else if(layout.height-textArea1.y<=scrollMenuPanel.height)
                {
                    scrollMenuPanel.state="bottom"
                }
                else
                {
                    scrollMenuPanel.state="shownArea1"
                }
            }
            onSignalChangeValueOfSpinBox:{
                    spinIndexOfAddPoint.maximumValue=client.midpoints.length/3+1
                    spinIndexOfAddPoint.value=client.midpoints.length/3+1
                    spinIndexOfStartPoint.maximumValue=client.midpoints.length/3+1
            }
        }
    }
}
