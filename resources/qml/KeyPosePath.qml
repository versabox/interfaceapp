import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.2
import QtQml.Models 2.1
import QtQuick.Controls.Styles 1.4

Rectangle{
    id: keyPosePath
    width: parent.width
    height: parent.height
    objectName: "keyPosePath"
    color: "gray"

    property var win
    property string nameForDescription: ""
    property string textForDescription: ""
    property string keyPoseTypeForDescription: ""
    property string keyPoseTimeForDescription: ""
    property string currentGridKeyPoseName: ""
    property string nameOfEditKeyPosePath: ""
    property string descriptionOfEditKeyPosePath: ""
    property bool descriptionType: true // true if KeyPose description, false if keyPosePath description
    property string language: "english"

    signal signalAddNextKeyPoseToPath(string name)
    signal signalMoveListItem(int fromItemIndex, int toItemIdex)
    signal signalDeleteItemFromListView(int itemIndex)
    signal signalSaveKeyPosePath(string name, string description)
    signal signalLoadKeyPosePath(string name)
    signal signalSendToKeyPose(string name)
    signal startKeyPosePath(bool cyclic)
    function setCurrentIndexOfListView(index_){

        keyPosePathList.positionViewAtIndex(index_, ListView.Visible)
        keyPosePathList.currentIndex = index_
    }

    function saveKeyPosePath(name, description){

        keyPosePath.signalSaveKeyPosePath(name, description)
        win.destroy()
    }

    Rectangle{
        id:gridKeyposesViewRectangle
        height: parent.height/2
        width: 3 * (parent.width / 5)
        border.width: 0
        color: "#00000000"
        clip: true

        TabView{
            anchors.fill: parent
            style:
                TabViewStyle{


                frameOverlap: 1
                        tab: Rectangle {
                            color: styleData.selected ? "gray" :"darkgray"
                            implicitWidth: Math.max(text.width + 4, 80)
                            implicitHeight: 20
                            border.width: 0
                            radius: 2
                            Text {
                                id: text
                                anchors.centerIn: parent
                                text: styleData.title
                                font.bold: styleData.selected
                                color: "black"
                            }
                        }
                        frame: Rectangle { color: "gray" }
            }

            Tab{
                title: qsTr(client.translateIntoSelectedLanguage("Keyposes", language))
                id:keyPoseTab

                signal signalClicked()

                ScrollView{

                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.margins: 5
                    anchors.left: parent.left
                    width: parent.width
                    style:
                        ScrollViewStyle{
                            transientScrollBars: false
                            handle: Item {
                                implicitWidth: 14
                                implicitHeight: 26

                                Rectangle {
                                    color: "#424246"
                                    anchors.fill: parent
                                    anchors.topMargin: 6
                                    anchors.leftMargin: 4
                                    anchors.rightMargin: 4
                                    anchors.bottomMargin: 6
                                }
                            }
                            scrollBarBackground: Item {
                                implicitWidth: 14
                                implicitHeight: 26

                            }
                            decrementControl: Rectangle{
                                color: "#00000000"
                            }
                            incrementControl: Rectangle{
                                color: "#00000000"
                            }
                        }

                    GridView{
                        id:gridKeyPoseView
                        anchors.fill: parent
                        focus: true
                        clip: true
                        cellHeight: 60
                        cellWidth: 140
                        currentIndex: -1

                        model: keyPoseGridViewModel
                        delegate:
                            Item {
                                id:delegateItem
                                width: gridKeyPoseView.cellWidth
                                height: gridKeyPoseView.cellHeight
                                property string keyPoseNameString: model.modelData.name

                                Rectangle{
                                    id:delegateRect
                                    color: "#00000000"
                                    anchors.topMargin: 3
                                    anchors.top: parent.top
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    width: 40
                                    height: 40

                                    Image {
                                        id: image
                                        fillMode: Image.PreserveAspectFit
                                        source: model.modelData.imageSource
                                        anchors.fill: parent
                                    }
                                }
                                Text {
                                    id: nameText
                                    text: model.modelData.name
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    anchors.bottom: parent.bottom
                                    anchors.bottomMargin: 3
                                    font.pointSize: 12
                                }

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {

                                        gridKeyPoseView.currentIndex = index
                                        keyPoseTab.signalClicked()
                                    }
                                    onDoubleClicked: {
                                        gridKeyPoseView.currentIndex = -1
                                        keyPosePath.signalAddNextKeyPoseToPath(keyPoseNameString)
                                    }
                                }

                                states: [
                                     State {
                                        name: "none"
                                        when: (gridKeyPoseView.currentIndex == -1)
                                        PropertyChanges {target: keyPosePath;
                                            nameForDescription: "";
                                            textForDescription: "";
                                            currentGridKeyPoseName: "";
                                            keyPoseTypeForDescription:"";
                                            keyPoseTimeForDescription:""
                                        }
                                     },
                                     State {
                                        name: "selected"
                                        when: delegateItem.GridView.isCurrentItem
                                         PropertyChanges {target: nameText; font.pointSize: 14}
                                         PropertyChanges {target: keyPosePath;
                                             nameForDescription: model.modelData.name;
                                             textForDescription: model.modelData.description;
                                             currentGridKeyPoseName: model.modelData.name;
                                             keyPoseTypeForDescription:model.modelData.type;
                                             keyPoseTimeForDescription:model.modelData.time
                                         }
                                     }
                                 ]
                                 transitions: Transition {
                                    PropertyAnimation { target: nameText; properties: "font.pointSize"}
                                 }
                                 Connections{
                                     target:button1
                                     onButtonClicked:{

                                         if(delegateItem.GridView.isCurrentItem){

                                             keyPosePath.signalSendToKeyPose(model.modelData.name)
                                         }
                                     }
                                 }
                            }

                        highlight: Rectangle { color: "#55AA0000"; radius: 5 }
                        Connections{
                            target: keyPosePathList
                            onSignalClicked: {

                                gridKeyPoseView.currentIndex = -1
                            }
                        }
                        Connections{
                            target: keyPosePathTab
                            onSignalClicked: {

                                gridKeyPoseView.currentIndex = -1
                                typeText.visible = false
                                typeTextContent.visible = false
                                timeText.visible = false
                                timeTextContent.visible = false
                            }
                        }
                    }
                }
            }

            Tab{
                id:keyPosePathTab
                title: qsTr(client.translateIntoSelectedLanguage("Keypose_Paths", language))


                signal signalClicked()

                ScrollView{
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.margins: 5
                    anchors.left: parent.left
                    width: parent.width
                    style:
                        ScrollViewStyle{
                            transientScrollBars: false
                            handle: Item {
                                implicitWidth: 14
                                implicitHeight: 26

                                Rectangle {
                                    color: "#424246"
                                    anchors.fill: parent
                                    anchors.topMargin: 6
                                    anchors.leftMargin: 4
                                    anchors.rightMargin: 4
                                    anchors.bottomMargin: 6
                                }
                            }
                            scrollBarBackground: Item {
                                implicitWidth: 14
                                implicitHeight: 26

                            }
                            decrementControl: Rectangle{
                                color: "#00000000"
                            }
                            incrementControl: Rectangle{
                                color: "#00000000"
                            }
                        }

                    GridView{
                        id:gridKeyPosePathView
                        objectName: "gridKeyPosePathView"
                        anchors.fill: parent
                        focus: true
                        clip: true
                        cellHeight: 60
                        cellWidth: 95
                        currentIndex: -1

                        model: keyPosePathGridViewModel
                        delegate:
                            Item {
                                id:delegatePathItem
                                width: gridKeyPosePathView.cellWidth
                                height: gridKeyPosePathView.cellHeight
                                property string keyPoseNameString: model.modelData.name

                                Rectangle{
                                    id:delegatePathRect
                                    color: "#00000000"
                                    anchors.topMargin: 3
                                    anchors.top: parent.top
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    width: 40
                                    height: 40

                                    Image {
                                        id: imagePath
                                        fillMode: Image.PreserveAspectFit
                                        source: model.modelData.imageSource
                                        anchors.fill: parent
                                    }
                                }
                                Text {
                                    id: nameTextPath
                                    text: model.modelData.name
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    anchors.bottom: parent.bottom
                                    anchors.bottomMargin: 3
                                    font.pointSize: 12
                                }

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {

                                        gridKeyPosePathView.currentIndex = index
                                        keyPosePathTab.signalClicked()
                                    }
                                    onDoubleClicked: {

                                        keyPosePath.signalLoadKeyPosePath(keyPoseNameString)
                                        keyPosePath.nameOfEditKeyPosePath = model.modelData.name
                                        keyPosePath.descriptionOfEditKeyPosePath = model.modelData.description
                                    }
                                }

                                states: [
                                     State {
                                        name: "none"
                                        when: (gridKeyPosePathView.currentIndex == -1)
                                        PropertyChanges {target: keyPosePath;
                                            nameForDescription: "";
                                            textForDescription: ""}
                                     },
                                     State {
                                        name: "selected"
                                        when: delegatePathItem.GridView.isCurrentItem
                                         PropertyChanges {target: nameTextPath; font.pointSize: 14}
                                         PropertyChanges {target: keyPosePath;
                                             nameForDescription: model.modelData.name;
                                             textForDescription: model.modelData.description}
                                     }
                                 ]
                                 transitions: Transition {
                                    PropertyAnimation { target: nameTextPath; properties: "font.pointSize"}
                                 }
                            }

                        highlight: Rectangle { color: "#55AA0000"; radius: 5 }
                        Connections{
                            target: keyPosePathList
                            onSignalClicked: {

                                gridKeyPosePathView.currentIndex = -1
                                keyPosePath.nameOfEditKeyPosePath = ""
                                keyPosePath.descriptionOfEditKeyPosePath = ""
                            }
                        }
                        Connections{
                            target: keyPoseTab
                            onSignalClicked: {

                                gridKeyPosePathView.currentIndex = -1
                                keyPosePath.nameOfEditKeyPosePath = ""
                                keyPosePath.descriptionOfEditKeyPosePath = ""
                                typeText.visible = true
                                typeTextContent.visible = true
                                timeText.visible = true
                                timeTextContent.visible = true
                            }
                        }
                    }
                }
            }
        }
    }

    DelegateModel {
        id: listViewVisualModel

        model: keyPoseListViewModel
        delegate: Item {
            id:delegateItemList
            width: 200
            height: 30
            property string keyPoseNameString: model.modelData.name

            MouseArea {
                id:listViewDragArea
                anchors { left: parent.left; right: parent.right }
                height: content.height

                property bool held: false

                drag.axis: Drag.YAxis
                drag.target: held ? content : undefined

                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onPressAndHold: listViewDragArea.held = true
                onPressed: {
                    if(pressedButtons == Qt.LeftButton){

                        keyPosePathList.oldPlace(index)
                    }
                    else if(pressedButtons == Qt.RightButton && delegateItemList.ListView.isCurrentItem){

                        keyPosePath.signalDeleteItemFromListView(index)
                    }
                }
                onReleased: {

                    if(keyPosePathList.oldIndex !==-1 && keyPosePathList.newIndex !== -1){
                        keyPosePath.signalMoveListItem(keyPosePathList.oldIndex,keyPosePathList.newIndex)
                    }

                    listViewDragArea.held = false
                    keyPosePathList.newPlace(-1)
                    keyPosePathList.oldPlace(-1)
                }
                onClicked: {

                    keyPosePathList.currentIndex = index
                    keyPosePathList.signalClicked()
                }

                Rectangle{
                    id:content
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    height: delegateItemList.height
                    width: delegateItemList.width
                    color: "#00000000"
                    border.color: "#55AA0000"
                    radius: 5
                    border.width: 0

                    Drag.active: listViewDragArea.held
                    Drag.source: listViewDragArea
                    Drag.hotSpot.x: width / 2
                    Drag.hotSpot.y: height / 2

                    Text {
                        id: indexText
                        text: index + 1
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        font.pointSize: 12
                    }

                    Rectangle{
                        id:imageRect
                        color: "#00000000"
                        anchors.topMargin: 2
                        anchors.top: parent.top
                        anchors.left: indexText.right
                        anchors.leftMargin: 10
                        width: 25
                        height: 25

                        Image {
                            id: imageList
                            fillMode: Image.PreserveAspectFit
                            source: model.modelData.imageSource
                            anchors.fill: parent
                        }
                    }
                    Text {
                        id: nameTextList
                        text: model.modelData.name
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.left: imageRect.right
                        anchors.leftMargin: 10
                        font.pointSize: 12
                    }

                }
                DropArea {
                    anchors { fill: parent; margins: 10 }
                    onEntered: {
                        listViewVisualModel.items.move(index)
                        keyPosePathList.newPlace(index)
                    }
                }
           }

           states: [
                State {
                   when: listViewDragArea.held

                   ParentChange  {target: content; parent: keyPosePath }
                   AnchorChanges {target: content; anchors.horizontalCenter: undefined; anchors.verticalCenter: undefined }
                   PropertyChanges {target: content; border.width: 2}
                },
                State {
                    name: "none"
                    when: (keyPosePathList.currentIndex == -1)
                    PropertyChanges {target: keyPosePath;
                        nameForDescription: "";
                        textForDescription: "";
                        currentGridKeyPoseName: "";
                        keyPoseTypeForDescription:"";
                        keyPoseTimeForDescription:""
                    }
                },
                State {
                    name: "selected"
                    when: delegateItemList.ListView.isCurrentItem
                     PropertyChanges {target: nameTextList; font.pointSize: 14}
                     PropertyChanges {target: content; border.width: 100}
                     PropertyChanges {target: keyPosePath;
                         nameForDescription: model.modelData.name;
                         textForDescription: model.modelData.description;
                         currentGridKeyPoseName: model.modelData.name;
                         keyPoseTypeForDescription:model.modelData.type;
                         keyPoseTimeForDescription:model.modelData.time
                     }
                }
            ]
            transitions: Transition {
                PropertyAnimation { target: nameTextList; properties: "font.pointSize"}
            }
            Connections{
                target: keyPosePathTab
                onSignalClicked: {

                    keyPosePathList.currentIndex = -1
                }
            }
            Connections{
                target: keyPoseTab
                onSignalClicked: {

                    keyPosePathList.currentIndex = -1
                }
            }
        }
    }

    Rectangle{
        id: keyPosePathListRectangle
        anchors.top: gridKeyposesViewRectangle.bottom
        height: parent.height/2
        width: 3 * (parent.width / 5)
        color: "#00000000"
        clip: true

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            id: keyPosePathText
            text: qsTr(client.translateIntoSelectedLanguage("KeyPose_Path", language))
            font.pointSize: 10
            font.bold: true
        }

        ScrollView{
            anchors.top: keyPosePathText.bottom
            anchors.bottom: parent.bottom
            anchors.margins: 5
            width: parent.width
            style:
                ScrollViewStyle{
                    transientScrollBars: false
                    handle: Item {
                        implicitWidth: 14
                        implicitHeight: 26

                        Rectangle {
                            color: "#424246"
                            anchors.fill: parent
                            anchors.topMargin: 6
                            anchors.leftMargin: 4
                            anchors.rightMargin: 4
                            anchors.bottomMargin: 6
                        }
                    }
                    scrollBarBackground: Item {
                        implicitWidth: 14
                        implicitHeight: 26

                    }
                    decrementControl: Rectangle{
                        color: "#00000000"
                    }
                    incrementControl: Rectangle{
                        color: "#00000000"
                    }
                }

            ListView{
                id:keyPosePathList
                anchors.fill: parent
                model: listViewVisualModel
                clip: true
                currentIndex: -1

                signal signalClicked()

                property int newIndex: -1
                property int oldIndex: -1
                function newPlace(index_){

                    newIndex = index_
                }
                function oldPlace(index_){

                    oldIndex = index_
                }
            }
        }
    }

    ColumnLayout{
        id:buttonLayout
        anchors.top: parent.top
        anchors.left: gridKeyposesViewRectangle.right
        anchors.leftMargin: 10
        width: 2 * (parent.width / 5)
        height: parent.height/2
        clip: true

        PushButton{
            id: saveKeyPosePath
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            text: client.translateIntoSelectedLanguage("Save_KeyPose_Path", language)
            onButtonClicked:{

                var component = Qt.createComponent("qrc:/qml/SaveDialog.qml")
                win = component.createObject(keyPosePath, {
                                        "title": "Keypose Path Saver",
                                        "nameText": true,
                                        "descriptionText": true,
                                        "connectAcceptFunction": keyPosePath.saveKeyPosePath,
                                        "oldNameText": keyPosePath.nameOfEditKeyPosePath,
                                        "oldDescriptionText": keyPosePath.descriptionOfEditKeyPosePath
                                        });
            }
        }

        PushButton{
            id: button1
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            text: client.translateIntoSelectedLanguage("Send_To_KeyPose", language)
            onButtonClicked:{

            }
        }

        PushButton{
            id: button2
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: true
            text: client.translateIntoSelectedLanguage("Start_Temporary_Path", language)
            onButtonClicked:{

                keyPosePath.startKeyPosePath(cyclicPath.clicked)
            }
        }

        PushButton{
            id: button3
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: false
            text: "Button3"
            onButtonClicked:{

            }
        }

        PushButton{
            id: button4
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: false
            text: "Button4"
            onButtonClicked:{

            }
        }

        PushButton{
            id: button5
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: false
            text: "Button5"
            onButtonClicked:{

            }
        }

        PushButton{
            id: button6
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: false
            text: "Button6"
            onButtonClicked:{

            }
        }

        PushButton{
            id: button7
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: false
            text: "Button7"
            onButtonClicked:{

            }
        }

        PushButton{
            id: button8
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: false
            text: "Button8"
            onButtonClicked:{

            }
        }

        PushButton{
            id: button9
            anchors.left: parent.left
            width: parent.width - 20
            height: 25
            visible: true
            text: client.translateIntoSelectedLanguage("Show_Path_On_Map", language)
            onButtonClicked:{
                if(button9.clicked){

                    menuPanel.graphOfKeyPosePath(false)
                    button9.clicked = false
                    button9.text = client.translateIntoSelectedLanguage("Show_Path_On_Map", language)
                }
                else{

                    menuPanel.graphOfKeyPosePath(true)
                    button9.clicked = true
                    button9.text = client.translateIntoSelectedLanguage("Hide_Path_Of_Map", language)
                }
            }
        }
        Rectangle{
            anchors.left: parent.left
            width: parent.width - 20
            color: "#00000000"
            height: cyclicPath.width

            Text {
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                text: qsTr(client.translateIntoSelectedLanguage("Cyclic_Path", language))
            }

            PushButton{
                id:cyclicPath
                anchors.right: parent.right
                clicked: false
                width: 25
                height: 25
                dynamicCheckBox: true
                text:""
                number_of_pushbutton: -50

                onButtonClicked:{

                }
            }
        }
    }

    Rectangle{
        id: descriptionRectangle
        anchors.top: buttonLayout.bottom
        anchors.left: keyPosePathListRectangle.right
        anchors.margins: 10
        anchors.topMargin: 30
        height: parent.height/2
        width: 2 * (parent.width / 5)
        color: "#00000000"
        clip: true

        ScrollView{
            anchors.fill: parent
            anchors.margins: 5
            style:
                ScrollViewStyle{
                    transientScrollBars: true
                    handle: Item {
                        implicitWidth: 14
                        implicitHeight: 26

                        Rectangle {
                            color: "#424246"
                            anchors.fill: parent
                            anchors.topMargin: 6
                            anchors.leftMargin: 4
                            anchors.rightMargin: 4
                            anchors.bottomMargin: 6
                        }
                    }
                    scrollBarBackground: Item {
                        implicitWidth: 14
                        implicitHeight: 26

                    }
                    decrementControl: Rectangle{
                        color: "#00000000"
                    }
                    incrementControl: Rectangle{
                        color: "#00000000"
                    }
                }
            contentItem:
                ColumnLayout{
                anchors.margins: 5

                clip: true
                Text {
                    id: nameDescriptionText
                    text: keyPosePath.descriptionType ? qsTr(client.translateIntoSelectedLanguage("KeyPose_Name", language)) : qsTr(client.translateIntoSelectedLanguage("KeyPose_Path_Name", language))
                    font.bold: true
                }
                Text {
                    id: nameDescriptionTextContent
                    text: keyPosePath.nameForDescription
                }
                Text {
                    id: descriptionText
                    text: qsTr(client.translateIntoSelectedLanguage("Description", language))
                    font.bold: true
                }
                Text {
                    id: descriptionTextContent
                    anchors.leftMargin: 30
                    text: keyPosePath.textForDescription
                }
                Text {
                    id: typeText
                    text: qsTr(client.translateIntoSelectedLanguage("KeyPose_Type", language))
                    font.bold: true
                }
                Text {
                    id: typeTextContent
                    anchors.leftMargin: 30
                    text: keyPosePath.keyPoseTypeForDescription
                }
                Text {
                    id: timeText
                    text: qsTr(client.translateIntoSelectedLanguage("Waiting_Time", language))
                    font.bold: true
                }
                Text {
                    id: timeTextContent
                    anchors.leftMargin: 30
                    text: keyPosePath.keyPoseTimeForDescription
                }
            }
        }
    }
}
