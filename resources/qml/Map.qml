import QtQuick 2.5
import QtQml.Models 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import QtQuick.Window 2.2
import VbType 1.0

Item{

    id:drawMap
    property var workspace
    property var display
    property int firstPointX
    property int firstPointY
    property int lastPointX
    property int lastPointY
    property double fiRadian
    property double zoom: 1.0
    property int flickX
    property int flickY


    signal updateMapWindow()
    signal rePaintWindow()
    signal updateScrollPosition(int x, int y,double z)
    signal signalEditMidpoints()
    signal maxSizeChanged(int w,int h)


    function addMidpoint(pointX,pointY,fi){
        mapCanvas.signalAddMidpoint(pointX,pointY,fi)
    }
    function addIndexMidpoints(pointX,pointY,fi,index){
        mapCanvas.signalAddIndexMidpoint(pointX,pointY,fi,index)
    }

    function removeMidpoint(mouseX,mouseY){
        mapCanvas.signalRemoveMidpoint(mouseX,mouseY)
    }
    function setRobotPosition(firstPointX, firstPointY, fiRadian){
        mapCanvas.signalSetRobotPosition(firstPointX, firstPointY, fiRadian)
    }
    function addKeyPose(pointX,pointY,angle,name){
        mapCanvas.signalAddKeyPose(pointX,pointY,angle,name)
    }

    function deleteKeyPose(mouseX,mouseY){
        mapCanvas.signalDeleteKeyPose(mouseX,mouseY)
    }

    function setScrollPosition(x,y,z){
        scrollMap.flickableItem.contentX=x
        scrollMap.flickableItem.contentY=y
        flickX=x
        flickY=y
        zoom=z
    }

    function initWindow(){
        updateScrollPosition(scrollMap.flickableItem.contentX,scrollMap.flickableItem.contentY,zoom)
        updateMapWindow()
    }

    ScrollView{

        id: scrollMap
        width: parent.width
        height: parent.height

        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff

        Rectangle {

            anchors.fill: parent
            color: "#cdcdcd"
            MouseArea{
                parent: scrollMap
                anchors.fill: parent
                acceptedButtons: Qt.NoButton
                onWheel: {
                    mapCanvas.zoomContent(wheel.angleDelta.y/1000)
                }
            }
        }
        Canvas{
            objectName: "map"
            id:mapCanvas
            scale: zoom
            width: 100
            height: 100
            state: "none"

            property var editingPointInfo
            property var win
            property var component : Qt.createComponent("qrc:/qml/SaveDialog.qml")
            property var componentKeyPose : Qt.createComponent("qrc:/qml/KeyPoseSaveDialog.qml")
            property bool visibleOfWarningZone: false
            property bool visibleOfMidPoints: true
            property bool visibleOfMidKeyPoseGraph: false
            property bool visibleOfNoDrivingArea: false
            property bool drawNewKeyPosePath: false
            property bool enableForDrawing: false
            property var mapImage
            property string oldMapUrl: ""
            property string warningZoneNameToDelete: ""

            //midPoints
            signal signalAddMidpoint(double firstPointX,double firstPointY,double fiRadian,int index)
            signal signalRemoveMidpoint(double firstMeterX, double firstMeterY);
            signal signalRemoveMidpointIndex(int index);
            signal signalRemoveAllMidpoint();

            //keyPoses
            signal signalAddKeyPose(string name, string description, double firstMeterX, double firstMeterY,
                                    double fiRadian,  int typeOfKeyPose, int typeOfTimeSet, int time);
            signal signalDeleteKeyPose(double firstMeterX, double firstMeterY);
            signal signalDeleteKeyPoseName(string name);
            signal signalSendToKeyPose(string name);

            //warningArea
            signal signalAddWarningZoneCircle(double x, double y, double range, string name, int type);
            signal signalAddWarningZonePolygon(string name, int type);
            signal signalDeleteWarningZone(string name);

            //midKeyPoseGraph
            signal signalSaveMidKeyPoseGraph(bool on, bool accepted);

            //keyPosePath
            signal signalStartKeyPosePath(bool cyclic);

            //robot
            signal signalSetRobotPosition(double X, double Y, double fi);

            //map
            signal signalSaveMap(string name);

            function updateMap(){

                if(oldMapUrl !== ""){
                    mapCanvas.unloadImage(oldMapUrl)
                }

                mapCanvas.width=client.mapWidth
                mapCanvas.height=client.mapHeight
                mapCanvas.loadImage(client.mapAdress)
                oldMapUrl = client.mapAdress
                mapCanvas.requestPaint()
                drawMap.updateMapWindow()
                drawMap.maxSizeChanged(zoom*mapCanvas.width,zoom*mapCanvas.height)
            }
            function rePaint(){
                mapCanvas.requestPaint()
                drawMap.rePaintWindow()
            }

            function addKeyPoseNow(name, description, typeOfKeyPose, typeOfTimeSet, time){

                mapCanvas.signalAddKeyPose(name, description, firstPointX, firstPointY, fiRadian, typeOfKeyPose, typeOfTimeSet, time)
                mapCanvas.enableForDrawing = false
            }

            function addKeyPoseRejected(){

                mapCanvas.signalAddKeyPose(mapCanvas.editingPointInfo.getName, mapCanvas.editingPointInfo.getDescription, mapCanvas.editingPointInfo.getX, mapCanvas.editingPointInfo.getY, mapCanvas.editingPointInfo.getFi, mapCanvas.editingPointInfo.getQmlType, mapCanvas.editingPointInfo.getTimeSetType, mapCanvas.editingPointInfo.getTime)
                mapCanvas.enableForDrawing = false
            }

            function addWarningZoneCircleNow(name, description, type){

                mapCanvas.signalAddWarningZoneCircle(firstPointX, firstPointY, Math.sqrt(Math.pow((lastPointX-firstPointX), 2) + Math.pow((lastPointY-firstPointY), 2)), name, type)
                mapCanvas.enableForDrawing = false
            }
            function addWarningZoneCircleRejected(){

                mapCanvas.enableForDrawing = false
            }

            function addWarningZonePolygonNow(name, description, type){

                mapCanvas.signalAddWarningZonePolygon(name, type)
                mapCanvas.enableForDrawing = false
            }
            function addWarningZonePolygonRejected(){

                mapCanvas.enableForDrawing = false
                client.newWarningZone.clearShape()
            }
            function zoomContent(zoomValue) {

                if(mapCanvas.state === "none"){

                 zoom += zoomValue

                 if(zoomValue<0 && (mapCanvas.width<30 || zoom*mapCanvas.height<30))
                 {
                     zoom -= zoomValue
                     return;
                 }

                 var zoomItem

                 if(display.state==="maximized")
                     zoomItem=workspace
                 else{
                     zoomItem=drawMap
                     if(zoomValue<0 && (zoom*mapCanvas.width<=zoomItem.width||zoom*mapCanvas.height<=zoomItem.height)) {
                         zoom -= zoomValue
                         return;
                     }
                 }

                 if(zoom*mapCanvas.width<=zoomItem.width)
                     scrollMap.flickableItem.contentX=-zoom*mapCanvas.width/2+mapCanvas.width/2
                 else{
                     if(zoomItem<0)
                     {
                         scrollMap.flickableItem.contentX-=(scrollMap.flickableItem.contentX-(-zoomItem.width/2+mapCanvas.width/2))/(10*(zoom-zoomItem.width/mapCanvas.width+0.2))
                     }
                 }

                 if(zoom*mapCanvas.height<=zoomItem.height)
                     scrollMap.flickableItem.contentY=-zoom*mapCanvas.height/2+mapCanvas.height/2
                 else{
                     if(zoomItem<0)
                     {
                         scrollMap.flickableItem.contentY-=(scrollMap.flickableItem.contentY-(-zoomItem.height/2+mapCanvas.height/2))/(10*(zoom-zoomItem.height/mapCanvas.height+0.2))
                     }
                 }
                 flickX=scrollMap.flickableItem.contentX
                 flickY=scrollMap.flickableItem.contentY

                 drawMap.maxSizeChanged(zoom*mapCanvas.width,zoom*mapCanvas.height)
                }
            }

           onImageLoaded: {

               var ctx = getContext("2d")
               mapImage = ctx.createImageData(client.mapAdress)
//               if(oldMapUrl !== ""){
//                   mapCanvas.unloadImage(oldMapUrl)
//               }
               mapCanvas.requestPaint()

               flickX=scrollMap.flickableItem.contentX
               flickY=scrollMap.flickableItem.contentY

               mapCanvas.zoomContent(0)
           }

           onPaint: {
               var ctx = getContext("2d")
               if(mapImage){
                  ctx.drawImage(mapImage,0,0)
                  var angle
                   for(var i=0; i<client.path.length/2; i++){
                       ctx.save()
                       ctx.fillStyle=Qt.rgba(0, 0, 255, 0.5)
                       ctx.fillRect(client.path[2*i],client.path[2*i+1],3,3)
                       ctx.restore()
                   }

                   if(visibleOfMidPoints){

                       for(var i=0; i<3*(client.midpoints.length); i=i+3){
                           angle=client.midpoints[i+2]
                           ctx.save()
                           ctx.rotate(angle)
                           ctx.translate(0,-4)
                           for(var j=0; j<client.path.length/2; j++){
                               ctx.fillStyle=Qt.rgba(0.62, 0.24, 0.55, 0.5)
                               ctx.fillRect((client.midpoints[i])*Math.cos(-angle)-(client.midpoints[i+1])*Math.sin(-angle),(client.midpoints[i])*Math.sin(-angle)+(client.midpoints[i+1])*Math.cos(-angle),3,3)
                           }
                           ctx.restore()
                       }
                   }
                   var tempKeyPoses = client.keyPoseList
                   for(var i=0; i<tempKeyPoses.length; i++){
                       if(client.visibleOfKeyPoses[i] === true){
                           angle=tempKeyPoses[i].getFi
                           ctx.save()
                           ctx.rotate(angle)
                           ctx.translate(0,-4)
                           ctx.drawImage("qrc:/images/arrow.png",(tempKeyPoses[i].getX)*Math.cos(-angle)-(tempKeyPoses[i].getY)*Math.sin(-angle),(tempKeyPoses[i].getX)*Math.sin(-angle)+(tempKeyPoses[i].getY)*Math.cos(-angle),25,8)
                           ctx.restore()
                           ctx.strokeText(tempKeyPoses[i].getName, tempKeyPoses[i].getX+5, tempKeyPoses[i].getY)
                       }
                   }

                   ctx.save()
                   angle = client.position[2]-client.headPosition[0]-Math.PI/4
                   ctx.rotate(angle)
                   ctx.drawImage("qrc:/images/radar.png",(client.position[0])*Math.cos(-angle)-(client.position[1])*Math.sin(-angle),(client.position[0])*Math.sin(-angle)+(client.position[1])*Math.cos(-angle))
                   ctx.restore()

                   ctx.save()
                   ctx.rotate(client.position[2])
                   ctx.translate(-12.5,-12.5)
                   ctx.drawImage("qrc:/images/robot.png",(client.position[0])*Math.cos(-client.position[2])-(client.position[1])*Math.sin(-client.position[2]),(client.position[0])*Math.sin(-client.position[2])+(client.position[1])*Math.cos(-client.position[2]),25,25)
                   ctx.restore()

                   for(var i=0;i< client.laserScan.length; i++){
                       ctx.save()
                       var angle=client.position[2]+client.angleOfLaserScan/2-((i)/client.laserScan.length)*client.angleOfLaserScan
                       ctx.fillStyle=Qt.rgba(255, 0, 0, 1)
                       ctx.fillRect(client.position[0]+client.laserScan[i]*Math.cos(angle)+(2*Math.cos(-client.position[2]))-1,client.position[1]+client.laserScan[i]*Math.sin(angle)+(2*Math.sin(client.position[2]))-1,2,2)
                       ctx.restore()
                   }
               }
//midPoints - keypose
               if((mapCanvas.state === "addMidPoint"
                   || mapCanvas.state === "editMidPoint"
                   || mapCanvas.state === "editMidPointEditing"
                   || mapCanvas.state === "addKeypose"
                   || mapCanvas.state === "editKeypose"
                   || mapCanvas.state === "editKeyposeEditing")
                       && mapCanvas.enableForDrawing)
               {
                   ctx.save()
                   ctx.rotate(fiRadian)
                   ctx.translate(0,-4)
                   ctx.drawImage("qrc:/images/arrow.png",(firstPointX)*Math.cos(-fiRadian)-(firstPointY)*Math.sin(-fiRadian),(firstPointX)*Math.sin(-fiRadian)+(firstPointY)*Math.cos(-fiRadian),25,8)
                   ctx.restore()
               }
               if(mapCanvas.state === "setRobotPosition" && mapCanvas.enableForDrawing)
               {
                   ctx.save()
                   ctx.rotate(fiRadian)
                   ctx.translate(-12.5,-12.5)
                   ctx.drawImage("qrc:/images/robot.png",(firstPointX)*Math.cos(-fiRadian)-(firstPointY)*Math.sin(-fiRadian),(firstPointX)*Math.sin(-fiRadian)+(firstPointY)*Math.cos(-fiRadian),25,25)
                   ctx.restore()
               }

               ctx.lineWidth=1

//midKeyPoseGraph
               ctx.fillStyle=Qt.rgba(255, 255, 255, 1)
               ctx.strokeStyle = "black"
               ctx.lineWidth = 1

               if(mapCanvas.visibleOfMidKeyPoseGraph
                       || mapCanvas.state === "addMidKeyPoseGraph"
                       || mapCanvas.state === "editMidKeyPoseGraph"
                       || mapCanvas.state === "deleteMidKeyPoseGraph"){

                   if(!(mapCanvas.enableForDrawing && mapCanvas.state === "editMidKeyPoseGraph")){

                       for (var i = 0; i <client.midKeyPoseGraphList.length; i++){
                           for(var j = 0; j < client.midKeyPoseGraphList[i].edge.length; j++){
                               ctx.beginPath()
                               ctx.moveTo(client.midKeyPoseGraphList[i].X, client.midKeyPoseGraphList[i].Y)
                               ctx.lineTo(client.midKeyPoseGraphList[i].edge[j].X, client.midKeyPoseGraphList[i].edge[j].Y)
                               ctx.stroke()
                               ctx.strokeStyle = "black"
                               ctx.closePath()

                               //arrow
                               angle=Math.atan2((client.midKeyPoseGraphList[i].edge[j].Y-client.midKeyPoseGraphList[i].Y),(client.midKeyPoseGraphList[i].edge[j].X-client.midKeyPoseGraphList[i].X)) + Math.PI/2
                               ctx.save()
                               ctx.translate(client.midKeyPoseGraphList[i].edge[j].X,client.midKeyPoseGraphList[i].edge[j].Y)
                               ctx.rotate(angle)
                               ctx.beginPath()
                               ctx.moveTo(0, 0)
                               ctx.lineTo(-3,5)
                               ctx.lineTo(3,5)
                               ctx.lineTo(0,0)
                               ctx.strokeStyle = "black"
                               ctx.fillStyle = "black"
                               ctx.fill()
                               ctx.stroke()
                               ctx.closePath()
                               ctx.restore()

                           }
                       }
                   }
                   else{

                       for (var i = 0; i <client.midKeyPoseGraphList.length; i++){
                           for(var j = 0; j < client.midKeyPoseGraphList[i].edge.length; j++){
                               ctx.beginPath()
                               if(client.midKeyPoseGraphList[i] === client.midKeyPoseGraphList[mapCanvas.editingPointInfo]){

                                   ctx.moveTo(lastPointX, lastPointY)
                               }
                               else{

                                   ctx.moveTo(client.midKeyPoseGraphList[i].X, client.midKeyPoseGraphList[i].Y)
                               }

                               if(client.midKeyPoseGraphList[i].edge[j] === client.midKeyPoseGraphList[mapCanvas.editingPointInfo]){

                                   ctx.lineTo(lastPointX, lastPointY)
                               }
                               else{

                                   ctx.lineTo(client.midKeyPoseGraphList[i].edge[j].X, client.midKeyPoseGraphList[i].edge[j].Y)
                               }

                               ctx.stroke()
                               ctx.strokeStyle = "black"
                               ctx.closePath()

                               //arrow
                               ctx.save()

                               if(client.midKeyPoseGraphList[i] === client.midKeyPoseGraphList[mapCanvas.editingPointInfo]){

                                   angle = Math.atan2((client.midKeyPoseGraphList[i].edge[j].Y-lastPointY),(client.midKeyPoseGraphList[i].edge[j].X-lastPointX)) + Math.PI/2
                                   ctx.translate(client.midKeyPoseGraphList[i].edge[j].X,client.midKeyPoseGraphList[i].edge[j].Y)
                               }
                               else if(client.midKeyPoseGraphList[i].edge[j] === client.midKeyPoseGraphList[mapCanvas.editingPointInfo]){

                                   angle = Math.atan2((lastPointY-client.midKeyPoseGraphList[i].Y),(lastPointX-client.midKeyPoseGraphList[i].X)) + Math.PI/2
                                   ctx.translate(lastPointX,lastPointY)
                               }
                               else{

                                   angle = Math.atan2((client.midKeyPoseGraphList[i].edge[j].Y-client.midKeyPoseGraphList[i].Y),(client.midKeyPoseGraphList[i].edge[j].X-client.midKeyPoseGraphList[i].X)) + Math.PI/2
                                   ctx.translate(client.midKeyPoseGraphList[i].edge[j].X,client.midKeyPoseGraphList[i].edge[j].Y)
                               }
                               ctx.rotate(angle)
                               ctx.beginPath()
                               ctx.moveTo(0, 0)
                               ctx.lineTo(-3,5)
                               ctx.lineTo(3,5)
                               ctx.lineTo(0,0)
                               ctx.strokeStyle = "black"
                               ctx.fillStyle = "black"
                               ctx.fill()
                               ctx.stroke()
                               ctx.closePath()
                               ctx.restore()

                           }
                       }
                       //mark editing
                       ctx.lineWidth = 0.1
                       ctx.fillStyle=Qt.rgba(0, 200, 0, 0.3)
                       ctx.beginPath()
                       ctx.arc(lastPointX, lastPointY, 10, 2 * Math.PI, 0.0, false)
                       ctx.fill()
                       ctx.stroke()
                       ctx.closePath()
                       ctx.restore()

                       ctx.fillStyle=Qt.rgba(255, 255, 255, 1)
                       ctx.strokeStyle = "black"
                       ctx.lineWidth = 1
                   }


               }
               if(mapCanvas.state === "addMidKeyPoseGraph" && mapCanvas.enableForDrawing){

                   var point = client.getIndexOfMidKeyPose(lastPointX, lastPointY)
                   if(point !== -1){

                       lastPointX = client.midKeyPoseGraphList[point].X
                       lastPointY = client.midKeyPoseGraphList[point].Y
                   }

                   ctx.beginPath()
                   ctx.moveTo(firstPointX, firstPointY)
                   ctx.lineTo(lastPointX, lastPointY)
                   ctx.stroke()
                   ctx.closePath()
                   //arrow
                   angle=Math.atan2((lastPointY-firstPointY),(lastPointX-firstPointX)) + Math.PI/2
                   ctx.save()
                   ctx.translate(lastPointX,lastPointY)
                   ctx.rotate(angle)
                   ctx.beginPath()
                   ctx.moveTo(0, 0)
                   ctx.lineTo(-3,5)
                   ctx.lineTo(3,5)
                   ctx.lineTo(0,0)
                   ctx.strokeStyle = "black"
                   ctx.fill()
                   ctx.stroke()
                   ctx.closePath()
                   ctx.restore()
                }
               if(mapCanvas.state === "addMidKeyPoseGraph"
                       || mapCanvas.state === "editMidKeyPoseGraph"
                       || mapCanvas.state === "deleteMidKeyPoseGraph"){

                   point = client.getIndexOfMidKeyPose(canvasMouseArea.mouseX, canvasMouseArea.mouseY)
                   if(point !== -1){

                       //mark near
                       ctx.lineWidth = 0.1
                       ctx.fillStyle=Qt.rgba(255, 50, 0, 0.8)
                       ctx.beginPath()
                       ctx.arc(client.midKeyPoseGraphList[point].X, client.midKeyPoseGraphList[point].Y, 6, 2 * Math.PI, 0.0, false)
                       ctx.fill()
                       ctx.stroke()
                       ctx.closePath()
                       ctx.restore()

                       ctx.fillStyle=Qt.rgba(255, 255, 255, 1)
                       ctx.strokeStyle = "black"
                       ctx.lineWidth = 1
                   }

               }

//warningZone
               var patern = ctx.createPattern("blue", Qt.FDiagPattern)
               if(mapCanvas.visibleOfWarningZone
                       || mapCanvas.state === "warningZoneCircle"
                       || mapCanvas.state === "warningZonePolygon"){

                   for(i = 0; i < client.warningZoneList.length; i++){
                       if(client.warningZoneList[i].Type === 10){
                           ctx.lineWidth = 1
                           ctx.fillStyle=Qt.rgba(0, 0, 255, 1)
                           ctx.fillRect(client.warningZoneList[i].Shape[0].x - 2, client.warningZoneList[i].Shape[0].y - 2 , 4, 4)

                           ctx.fillStyle = patern
                           ctx.beginPath()
                           ctx.arc(client.warningZoneList[i].Shape[0].x, client.warningZoneList[i].Shape[0].y, client.warningZoneList[i].Shape[1].x, 2 * Math.PI, 0.0, false)
                           ctx.fill()
                           ctx.stroke()

                           ctx.strokeStyle = "black"
                           ctx.lineWidth = 1

                           ctx.strokeText(client.warningZoneList[i].Name, client.warningZoneList[i].Shape[0].x + 3, client.warningZoneList[i].Shape[0].y)
                       }
                       else {
                           ctx.lineWidth=2
                           ctx.beginPath()

                           for(var j = 0; j < client.warningZoneList[i].Shape.length; j++){

                               ctx.lineTo(client.warningZoneList[i].Shape[j].x, client.warningZoneList[i].Shape[j].y)
                           }
                           ctx.fillStyle = patern
                           ctx.strokeStyle = "black"
                           ctx.rect(ctx.width, ctx.hight, -ctx.width, -ctx.hight)
                           ctx.fill();
                           ctx.stroke()
                           ctx.closePath()
                       }
                   }
               }
               if(mapCanvas.state === "warningZoneCircle"
                       && mapCanvas.enableForDrawing){

                   ctx.fillStyle=Qt.rgba(0, 0, 255, 1)
                   ctx.fillRect(firstPointX - 2, firstPointY - 2 , 4, 4)

                   ctx.fillStyle = patern
                   ctx.lineWidth = 1
                   ctx.beginPath()
                   ctx.arc(firstPointX, firstPointY, Math.sqrt(Math.pow((lastPointX-firstPointX), 2) + Math.pow((lastPointY-firstPointY), 2)), 0.0, 2 * Math.PI, false)
                   ctx.fill()
                   ctx.stroke()
                   ctx.lineWidth=1
               }

               if(mapCanvas.state === "warningZonePolygon"){

                   ctx.lineWidth=2
                   ctx.beginPath()

                   for(var j = 0; j < client.newWarningZone.Shape.length; j++){

                       ctx.lineTo(client.newWarningZone.Shape[j].x, client.newWarningZone.Shape[j].y)
                   }
                   ctx.fillStyle = patern
                   ctx.strokeStyle = "black"
                   ctx.rect(ctx.width, ctx.hight, -ctx.width, -ctx.hight)
                   ctx.fill();
                   ctx.stroke()
                   ctx.closePath()
                   if(mapCanvas.enableForDrawing){

                       ctx.beginPath()
                       ctx.moveTo(firstPointX, firstPointY)
                       ctx.lineTo(lastPointX, lastPointY)
                       ctx.stroke()
                       ctx.closePath()
                   }
                   ctx.lineWidth=1

                   point = client.newWarningZone.getIndexOfPoint(canvasMouseArea.mouseX, canvasMouseArea.mouseY)
                   if(point !== -1){

                       //mark near
                       ctx.lineWidth = 0.1
                       ctx.fillStyle=Qt.rgba(255, 50, 0, 0.8)
                       ctx.beginPath()
                       ctx.arc(client.newWarningZone.Shape[point].x, client.newWarningZone.Shape[point].y, 6, 2 * Math.PI, 0.0, false)
                       ctx.fill()
                       ctx.stroke()
                       ctx.closePath()
                       ctx.restore()

                       ctx.fillStyle=Qt.rgba(255, 255, 255, 1)
                       ctx.strokeStyle = "black"
                       ctx.lineWidth = 1
                   }
               }

               if(mapCanvas.state === "deleteWaningZone"){

                   var zoneName = ""
                   for(i = 0; i < client.warningZoneList.length; i++){

                       var index = client.warningZoneList[i].getIndexOfPoint(canvasMouseArea.mouseX, canvasMouseArea.mouseY)
                       if(index !== -1){
                           zoneName = client.warningZoneList[i].Name
                       }
                   }
                   mapCanvas.warningZoneNameToDelete = zoneName
                   for(i = 0; i < client.warningZoneList.length; i++){
                       if(client.warningZoneList[i].Name === zoneName){
                           patern = ctx.createPattern("red", Qt.DiagCrossPattern)
                       }
                       else{
                           patern = ctx.createPattern("blue", Qt.FDiagPattern)
                       }

                       if(client.warningZoneList[i].Type === 10){
                           ctx.lineWidth = 1
                           ctx.fillStyle=Qt.rgba(0, 0, 255, 1)
                           ctx.fillRect(client.warningZoneList[i].Shape[0].x - 2, client.warningZoneList[i].Shape[0].y - 2 , 4, 4)

                           ctx.fillStyle = patern
                           ctx.beginPath()
                           ctx.arc(client.warningZoneList[i].Shape[0].x, client.warningZoneList[i].Shape[0].y, client.warningZoneList[i].Shape[1].x, 2 * Math.PI, 0.0, false)
                           ctx.fill()
                           ctx.stroke()

                           ctx.strokeStyle = "black"
                           ctx.lineWidth = 1

                           ctx.strokeText(client.warningZoneList[i].Name, client.warningZoneList[i].Shape[0].x + 3, client.warningZoneList[i].Shape[0].y)
                       }
                       else {
                           ctx.lineWidth=2
                           ctx.beginPath()

                           for(var j = 0; j < client.warningZoneList[i].Shape.length; j++){

                               ctx.lineTo(client.warningZoneList[i].Shape[j].x, client.warningZoneList[i].Shape[j].y)
                           }
                           ctx.fillStyle = patern
                           ctx.strokeStyle = "black"
                           ctx.rect(ctx.width, ctx.hight, -ctx.width, -ctx.hight)
                           ctx.fill();
                           ctx.stroke()
                           ctx.closePath()
                       }
                   }
               }

//KeyPosePath graph draw
               if(mapCanvas.drawNewKeyPosePath){

                   for(var i = 1; i < keyPoseListViewModel.length; i++){
                       ctx.beginPath()
                       ctx.moveTo(client.getKeyPosePositionFromName(keyPoseListViewModel[i-1].name).x, client.getKeyPosePositionFromName(keyPoseListViewModel[i-1].name).y)
                       ctx.lineTo(client.getKeyPosePositionFromName(keyPoseListViewModel[i].name).x, client.getKeyPosePositionFromName(keyPoseListViewModel[i].name).y)
                       ctx.stroke()
                       ctx.strokeStyle = "black"
                       ctx.closePath()

                       //arrow
                       angle = Math.atan2((client.getKeyPosePositionFromName(keyPoseListViewModel[i].name).y-client.getKeyPosePositionFromName(keyPoseListViewModel[i-1].name).y),
                                        (client.getKeyPosePositionFromName(keyPoseListViewModel[i].name).x-client.getKeyPosePositionFromName(keyPoseListViewModel[i-1].name).x)) + Math.PI/2
                       ctx.save()
                       ctx.translate(client.getKeyPosePositionFromName(keyPoseListViewModel[i].name).x, client.getKeyPosePositionFromName(keyPoseListViewModel[i].name).y)
                       ctx.rotate(angle)
                       ctx.beginPath()
                       ctx.moveTo(0, 0)
                       ctx.lineTo(-3,5)
                       ctx.lineTo(3,5)
                       ctx.lineTo(0,0)
                       ctx.strokeStyle = "black"
                       ctx.fill()
                       ctx.stroke()
                       ctx.closePath()
                       ctx.restore()
                   }
               }
//noDrivingArea
               var patern = ctx.createPattern("blue", Qt.FDiagPattern)
               if(mapCanvas.state === "addNoDrivingArea" || mapCanvas.visibleOfNoDrivingArea){
//                   for(var i = 0; i < client.noDrivingArea.length; i++){

//                       ctx.lineWidth=2
//                       ctx.beginPath()

//                       for(var j = 0; j < client.noDrivingArea[i].length; j++){

//                           ctx.lineTo(client.noDrivingArea[i][j].x, client.noDrivingArea[i][j].y)
//                       }
//                       ctx.fillStyle = patern
//                       ctx.strokeStyle = "black"
//                       ctx.fill();
//                       ctx.stroke()
//                       ctx.closePath()
//                       if(client.noDrivingArea[i].length > 0){

//                           ctx.beginPath()
//                           ctx.moveTo(lastPointX, lastPointY)
//                           ctx.lineTo(client.noDrivingArea[i][0].x, client.noDrivingArea[i][0].y)
//                           ctx.stroke()
//                           ctx.closePath()
//                       }
//                   }
                   if(mapCanvas.state === "addNoDrivingArea"){

                       ctx.lineWidth=2
                       ctx.beginPath()

                       for(var j = 0; j < client.newNoDrivingArea.length; j++){

                           ctx.lineTo(client.newNoDrivingArea[j].x, client.newNoDrivingArea[j].y)
                       }
                       ctx.fillStyle = patern
                       ctx.strokeStyle = "black"
                       ctx.rect(ctx.width, ctx.hight, -ctx.width, -ctx.hight)
                       ctx.fill();
                       ctx.stroke()
                       ctx.closePath()
                       if(mapCanvas.enableForDrawing){

                           ctx.beginPath()
                           ctx.moveTo(firstPointX, firstPointY)
                           ctx.lineTo(lastPointX, lastPointY)
                           ctx.stroke()
                           ctx.closePath()
                       }
                       ctx.lineWidth=1

                       point = client.getIndexOfNoDrivingPoint(canvasMouseArea.mouseX, canvasMouseArea.mouseY)
                       if(point !== -1){

                           //mark near
                           ctx.lineWidth = 0.1
                           ctx.fillStyle=Qt.rgba(255, 50, 0, 0.8)
                           ctx.beginPath()
                           ctx.arc(client.newNoDrivingArea[point].x, client.newNoDrivingArea[point].y, 6, 2 * Math.PI, 0.0, false)
                           ctx.fill()
                           ctx.stroke()
                           ctx.closePath()
                           ctx.restore()

                           ctx.fillStyle=Qt.rgba(255, 255, 255, 1)
                           ctx.strokeStyle = "black"
                           ctx.lineWidth = 1
                       }
                   }
               }
               delete ctx
               gc()
           }

           MouseArea{
               id: canvasMouseArea
               anchors.fill: parent
               acceptedButtons: Qt.LeftButton | Qt.RightButton
               property var clickedButton
               onPressed: {

                   clickedButton = pressedButtons
                   if(pressedButtons == Qt.LeftButton){

                       if(mapCanvas.state !== "editKeyposeEditing"){
                           firstPointX = mouseX
                           firstPointY = mouseY
                       }

                       if(mapCanvas.state === "addMidPoint"){
                           mapCanvas.enableForDrawing = true
                       }

                       else if (mapCanvas.state === "editMidPoint"){

                           mapCanvas.editingPointInfo = {}
                           mapCanvas.editingPointInfo = client.getMidpointToRemove(firstPointX, firstPointY)
                           if(mapCanvas.editingPointInfo !== null){

                               mapCanvas.signalRemoveMidpointIndex(mapCanvas.editingPointInfo[1])
                               fiRadian = mapCanvas.editingPointInfo[0]
                               mapCanvas.enableForDrawing = true
                           }
                       }

                       else if (mapCanvas.state === "addKeypose"){
                           mapCanvas.enableForDrawing = true
                       }

                       else if (mapCanvas.state === "editKeypose"){

                           mapCanvas.editingPointInfo = {}
                           mapCanvas.editingPointInfo = client.getKeyPoseToDelete(firstPointX, firstPointY)
                           if(mapCanvas.editingPointInfo !== null){

                               mapCanvas.signalDeleteKeyPoseName(mapCanvas.editingPointInfo.getName)
                               fiRadian = mapCanvas.editingPointInfo.getFi
                               mapCanvas.enableForDrawing = true
                           }
                       }

                       else if (mapCanvas.state === "addMidKeyPoseGraph"){

                           mapCanvas.enableForDrawing = true
                       }

//                       else if (mapCanvas.state === "editMidKeyPoseGraph"){

//                       }

                       else if (mapCanvas.state === "warningZoneCircle"){

                           mapCanvas.enableForDrawing = true
                       }
                       else if (mapCanvas.state === "warningZonePolygon"){

                           var indexPoint = client.newWarningZone.getIndexOfPoint(firstPointX, firstPointY)
                           if(indexPoint === -1){

                               client.newWarningZone.addToShape(firstPointX, firstPointY)
                           }

                           lastPointX = mouseX
                           lastPointY = mouseY
                           mapCanvas.enableForDrawing = true
                       }

                       else if (mapCanvas.state === "setRobotPosition"){

                           mapCanvas.enableForDrawing = true
                       }
                       else if (mapCanvas.state === "addNoDrivingArea"){

                           var indexPoint = client.getIndexOfNoDrivingPoint(firstPointX, firstPointY)
                           if(indexPoint === -1){

                               client.addNoDrivingPoint(firstPointX, firstPointY)

                           }

                           lastPointX = mouseX
                           lastPointY = mouseY
                           mapCanvas.enableForDrawing = true
                       }

                    }

//                    else if(pressedButtons == Qt.RightButton){

//                   }
                    mapCanvas.requestPaint()
               }
               onPositionChanged: {

                   lastPointX = mouseX
                   lastPointY = mouseY

                   if(mapCanvas.state === "addMidPoint"){

                       fiRadian = Math.atan2((lastPointY - firstPointY),(lastPointX - firstPointX))
                   }

                   else if (mapCanvas.state === "editMidPoint"){

                       firstPointX = mouseX
                       firstPointY = mouseY
                   }

                   else if (mapCanvas.state === "editMidPointEditing"){

                       fiRadian = Math.atan2((lastPointY - firstPointY),(lastPointX - firstPointX))
                   }

                   else if (mapCanvas.state === "addKeypose"){

                       fiRadian = Math.atan2((lastPointY - firstPointY),(lastPointX - firstPointX))
                   }

                   else if (mapCanvas.state === "editKeypose"){

                       firstPointX = mouseX
                       firstPointY = mouseY
                   }

                   else if (mapCanvas.state === "editKeyposeEditing"){

                       fiRadian = Math.atan2((lastPointY - firstPointY),(lastPointX - firstPointX))
                   }

//                   else if (mapCanvas.state === "addMidKeyPoseGraph"){

//                   }

//                   else if (mapCanvas.state === "editMidKeyPoseGraph"){

//                   }

//                   else if (mapCanvas.state === "warningZoneCircle"){

//                   }

//                   else if (mapCanvas.state === "addNoDrivingArea"){

//                   }

                   else if (mapCanvas.state === "setRobotPosition"){

                       fiRadian = Math.atan2((lastPointY - firstPointY),(lastPointX - firstPointX))
                   }

                   else if(mapCanvas.state === "none"){

                       var x = scrollMap.flickableItem.contentX + (firstPointX - lastPointX)
                       var y = scrollMap.flickableItem.contentY + (firstPointY - lastPointY)

                       if(x >= (-zoom * mapCanvas.width / 2 + mapCanvas.width / 2)
                               && x <= (zoom * mapCanvas.width / 2 + mapCanvas.width / 2 - scrollMap.flickableItem.width))
                       {

                           scrollMap.flickableItem.contentX = x
                       }


                       if( y >= (-zoom * mapCanvas.height / 2 + mapCanvas.height / 2)
                               && y <= (zoom * mapCanvas.height / 2 + mapCanvas.height / 2 - scrollMap.flickableItem.height))
                       {

                           scrollMap.flickableItem.contentY=y
                       }

                       flickX = scrollMap.flickableItem.contentX
                       flickY = scrollMap.flickableItem.contentY
                   }

                   mapCanvas.requestPaint()
               }

               onReleased: {

                   if(mapCanvas.state === "addMidPoint" && mapCanvas.enableForDrawing){

                       mapCanvas.signalAddMidpoint(firstPointX, firstPointY, fiRadian, menuPanel.indexOfAddingMidpoint)
                       mapCanvas.enableForDrawing = false
                   }

                   else if (mapCanvas.state === "editMidPoint" && mapCanvas.enableForDrawing){

                       if(mapCanvas.editingPointInfo !== null){

                           mapCanvas.state = "editMidPointEditing"
                       }
                   }

                   else if (mapCanvas.state === "editMidPointEditing"){

                       mapCanvas.signalAddMidpoint(firstPointX, firstPointY, fiRadian, mapCanvas.editingPointInfo[1])
                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "editMidPoint"
                   }

                   else if (mapCanvas.state === "addKeypose" && mapCanvas.enableForDrawing){

                       mapCanvas.enableForDrawing = false
                       mapCanvas.win = mapCanvas.componentKeyPose.createObject(mapCanvas, {
                                               "nameText": true,
                                               "descriptionText": true,
                                               "connectAcceptFunction": mapCanvas.addKeyPoseNow,
                                               "connectRejectFunction": false,
                                               });
                   }
                   else if (mapCanvas.state === "editKeypose"){

                       if(mapCanvas.editingPointInfo !== null){

                           mapCanvas.state = "editKeyposeEditing"
                       }
                   }

                   else if (mapCanvas.state === "editKeyposeEditing"){

                       mapCanvas.enableForDrawing = false
                       mapCanvas.win = mapCanvas.componentKeyPose.createObject(mapCanvas, {
                                               "title": "Keypose Saver",
                                               "nameText": true,
                                               "descriptionText": true,
                                               "connectAcceptFunction": mapCanvas.addKeyPoseNow,
                                               "connectRejectFunction": mapCanvas.addKeyPoseRejected,
                                               "oldNameText": mapCanvas.editingPointInfo.getName,
                                               "oldDescriptionText": mapCanvas.editingPointInfo.getDescription,
                                               "oldType": mapCanvas.editingPointInfo.getQmlType,
                                               "oldTime": mapCanvas.editingPointInfo.getTime,
                                               "oldRadioButtonParam": mapCanvas.editingPointInfo.getTimeSetType
                                               });
                       mapCanvas.state = "editKeypose"
                   }

                   else if (mapCanvas.state === "addMidKeyPoseGraph" && mapCanvas.enableForDrawing){

                       client.addMidKeyPoseGraph(firstPointX, firstPointY, lastPointX, lastPointY)
                       firstPointX = lastPointX
                       firstPointY = lastPointY
                       mapCanvas.enableForDrawing = false
                   }

                   else if (mapCanvas.state === "editMidKeyPoseGraph"){

                       if(mapCanvas.enableForDrawing){

                           client.editMidKeyPoint(lastPointX, lastPointY, client.midKeyPoseGraphList[mapCanvas.editingPointInfo].X, client.midKeyPoseGraphList[mapCanvas.editingPointInfo].Y)
                       }
                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "addMidKeyPoseGraph"
                   }

                   else if (mapCanvas.state === "warningZoneCircle" && mapCanvas.enableForDrawing){

                       mapCanvas.enableForDrawing = false
                       mapCanvas.win = mapCanvas.component.createObject(mapCanvas, {
                                               "title": "Warning Zone Saver",
                                               "nameText": true,
                                               "descriptionText": true,
                                               "typeVisible": true,
                                               "connectAcceptFunction": mapCanvas.addWarningZoneCircleNow,
                                               "typeModel": ['CROSSING', 'DOCKING', 'FORBIDDEN', 'INFO', 'SILENT', 'DOOR_CONTROL', 'FOLLOW_PATH', 'FAST_RIDE', 'SLOW_RIDE', 'STOP', 'NO_RECOVERY_BEHAVIOR', 'FOLLOW_PATH_CONDITIONAL_ZONE'],
                                               });
                   }
                   else if (mapCanvas.state === "warningZonePolygon" && clickedButton === Qt.LeftButton){

                       client.newWarningZone.addToShape(lastPointX, lastPointY)
                       firstPointX = lastPointX
                       firstPointY = lastPointY
                       mapCanvas.enableForDrawing = false
                   }
                   else if (mapCanvas.state === "setRobotPosition" && mapCanvas.enableForDrawing){

                       mapCanvas.enableForDrawing = false
//                       client.setRobotPosition(firstPointX, firstPointY, fiRadian)
                       mapCanvas.signalSetRobotPosition(firstPointX, firstPointY, fiRadian)

                   }
                   else if (mapCanvas.state === "addNoDrivingArea" && clickedButton === Qt.LeftButton){

                       client.addNoDrivingPoint(lastPointX, lastPointY)
                       firstPointX = lastPointX
                       firstPointY = lastPointY
                       mapCanvas.enableForDrawing = false
                   }
               }
               onDoubleClicked:{

                    if(mapCanvas.state === "deleteMidPoint"){

                        mapCanvas.signalRemoveMidpoint(mouseX,mouseY)
                    }

                    else if(mapCanvas.state === "deleteKeypose"){

                        mapCanvas.signalDeleteKeyPose(mouseX, mouseY)
                    }

                    if(pressedButtons == Qt.RightButton){

                        if (mapCanvas.state === "deleteWaningZone"){

                            mapCanvas.signalDeleteWarningZone(mapCanvas.warningZoneNameToDelete)
                        }
                    }
                }
               onWheel: {

                    mapCanvas.zoomContent(wheel.angleDelta.y/1000)
                }
               onPressAndHold: {

                    if(pressedButtons == Qt.RightButton){
                        if(mapCanvas.state === "addMidKeyPoseGraph"){

                            mapCanvas.state = "editMidKeyPoseGraph"
                            mapCanvas.editingPointInfo = {}
                            mapCanvas.editingPointInfo = client.getIndexOfMidKeyPose(mouseX, mouseY)
                            if(mapCanvas.editingPointInfo !==-1 ){

                                mapCanvas.enableForDrawing = true
                            }
                            lastPointX = mouseX
                            lastPointY = mouseY
                        }
                    }
                }
               onClicked: {

                    if(clickedButton === Qt.RightButton){

                        if(mapCanvas.state === "addMidKeyPoseGraph"){

                            mapCanvas.state = "deleteMidKeyPoseGraph"
                            firstPointX = mouseX
                            firstPointY = mouseY
                        }
                        else if(mapCanvas.state === "deleteMidKeyPoseGraph"){

                            client.deleteMidKeyPoseGraphPoint(firstPointX, firstPointY, mouseX, mouseY)
                            mapCanvas.state = "addMidKeyPoseGraph"
                        }
                        else if (mapCanvas.state === "addNoDrivingArea"){

                            var tempPoint = client.deleteLastNoDrivingPoint()

                            firstPointX = tempPoint.x
                            firstPointY = tempPoint.y
                        }
                        else if (mapCanvas.state === "warningZonePolygon"){

                            var index = client.newWarningZone.getIndexOfPoint(lastPointX,lastPointY)
                            var tempPoint
                            if(index !== -1){

                                tempPoint = client.newWarningZone.deleteFromShape(index)
                            }
                            else{

                                tempPoint = client.newWarningZone.deleteLastPointFromShape()
                            }
                            if(tempPoint){

                                firstPointX = tempPoint.x
                                firstPointY = tempPoint.y
                            }
                        }
                    }
                }
            }

           states: [
               State { name: "none"; },
               //midpoint
               State { name: "addMidPoint"; },
               State { name: "deleteMidPoint"; },
               State { name: "editMidPoint"; },
               State { name: "editMidPointEditing"; },
               //keypose
               State { name: "addKeypose"; },
               State { name: "deleteKeypose"; },
               State { name: "editKeypose"; },
               State { name: "editKeyposeEditing"; },
               //MidKeyPoseGraph
               State { name: "addMidKeyPoseGraph";
                   PropertyChanges { target: canvasMouseArea; hoverEnabled: true}},
               State { name: "editMidKeyPoseGraph";
                   PropertyChanges { target: canvasMouseArea; hoverEnabled: true}},
               State { name: "deleteMidKeyPoseGraph";
                   PropertyChanges { target: canvasMouseArea; hoverEnabled: true}},
               //warningZone
               State { name: "warningZoneCircle"; },
               State { name: "warningZonePolygon";
                    PropertyChanges { target: canvasMouseArea; hoverEnabled: true}},
               State { name: "deleteWaningZone";
                    PropertyChanges { target: canvasMouseArea; hoverEnabled: true}},
               //robot
               State { name: "setRobotPosition"; },
               //noDrivingArea
               State { name: "addNoDrivingArea";
                   PropertyChanges { target: canvasMouseArea; hoverEnabled: true}},
               State { name: "deleteNoDrivingArea"; }
           ]

           Connections{
               target: menuPanel
               onSignalAddMidpoint: {

                   if(on){

                       mapCanvas.state = "addMidPoint"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "none"
                   }
               }
               onSignalRemoveMidpoint: {

                   if(on){

                       mapCanvas.state = "deleteMidPoint"

                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "none"
                   }
               }
               onSignalRemoveAllMidpoint: {

                   mapCanvas.enableForDrawing = false
                   mapCanvas.state = "none"
               }
               onSignalEditMidpoint: {

                   if(on){

                       mapCanvas.state = "editMidPoint"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       if(mapCanvas.editingPointInfo
                          && mapCanvas.state === "editMidPointEditing"){

                           addIndexMidpoints(firstPointX, firstPointY, fiRadian, mapCanvas.editingPointInfo[1])
                       }
                       mapCanvas.state = "none"
                   }
               }
               onSignalSetRobotPosition:{

                   if(on){

                       mapCanvas.state = "setRobotPosition"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "none"
                   }
               }
               onSignalAddKeyPose: {
                   if(on){

                       mapCanvas.state = "addKeypose"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "none"
                   }
               }
               onSignalDeleteKeyPose: {
                   if(on){

                       mapCanvas.state = "deleteKeypose"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "none"
                   }
               }
               onSignalAddMidKeyPoseGraph:{
                   if(on){

                       mapCanvas.state = "addMidKeyPoseGraph"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "none"
                   }
               }
               onSignalSaveNoDrivingArea:{

                   mapCanvas.enableForDrawing = false
                   mapCanvas.state = "none"
               }
               onSignalDeleteNoDrivingArea:{

                   mapCanvas.enableForDrawing = false
                   mapCanvas.state = "none"
               }
               onSignalVisibleOfMidKeyPoseGraph:{

                   if(on){
                       mapCanvas.visibleOfMidKeyPoseGraph = true
                   }
                   else{
                       mapCanvas.visibleOfMidKeyPoseGraph = false
                   }
               }
               onSignalAddWarningZoneCircle:{

                   mapCanvas.state = "warningZoneCircle"
               }
               onSignalAddWarningZonePolygon:{

                   mapCanvas.state = "warningZonePolygon"

               }
               onSignalVisibleOfWarningZone:{

                   if(on){

                     mapCanvas.visibleOfWarningZone = true
                   }
                   else{

                     mapCanvas.visibleOfWarningZone = false
                   }
               }
               onSignalGraphOfKeyPosePath:{

                   if(on){

                       mapCanvas.drawNewKeyPosePath = true
                   }
                   else{

                       mapCanvas.drawNewKeyPosePath = false
                   }
               }
               onSignalEditKeyPose:{

                   if(on){

                       mapCanvas.state = "editKeypose"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       if(mapCanvas.editingPointInfo !== null
                          && mapCanvas.state === "editKeyposeEditing"){
                           mapCanvas.win = mapCanvas.componentKeyPose.createObject(mapCanvas, {
                                                   "title": "Keypose Saver",
                                                   "nameText": true,
                                                   "descriptionText": true,
                                                   "connectAcceptFunction": mapCanvas.addKeyPoseNow,
                                                   "connectRejectFunction": mapCanvas.addKeyPoseRejected,
                                                   "oldNameText": mapCanvas.editingPointInfo.getName,
                                                   "oldDescriptionText": mapCanvas.editingPointInfo.getDescription,
                                                   "oldType": mapCanvas.editingPointInfo.getQmlType,
                                                   "oldTime": mapCanvas.editingPointInfo.getTime,
                                                   "oldRadioButtonParam": mapCanvas.editingPointInfo.getTimeSetType
                                                   });
                           mapCanvas.state = "editKeypose"
                       }
                       mapCanvas.state = "none"
                   }
               }
               onSignalAddNoDrivingArea:{

                   if(on){

                       mapCanvas.state = "addNoDrivingArea"
                   }
                   else{

                       mapCanvas.enableForDrawing = false
                       mapCanvas.state = "none"
                   }
               }
               onSignalStopEditing: {

                   if(mapCanvas.state === "warningZonePolygon"){

                       mapCanvas.win = mapCanvas.component.createObject(mapCanvas, {
                                                 "title": "Warning Zone Saver",
                                                 "nameText": true,
                                                 "descriptionText": true,
                                                 "typeVisible": true,
                                                 "connectAcceptFunction": mapCanvas.addWarningZonePolygonNow,
                                                 "connectRejectFunction": mapCanvas.addWarningZonePolygonRejected,
                                                 "typeModel": ['CROSSING', 'DOCKING', 'FORBIDDEN', 'INFO', 'SILENT', 'DOOR_CONTROL', 'FOLLOW_PATH', 'FAST_RIDE', 'SLOW_RIDE', 'STOP', 'NO_RECOVERY_BEHAVIOR', 'FOLLOW_PATH_CONDITIONAL_ZONE'],
                                                 });
                   }

                   mapCanvas.enableForDrawing = false
                   mapCanvas.state = "none"
               }
               onSignalDeleteWarningZone:{
                   mapCanvas.state = "deleteWaningZone"
               }
           }
           Connections{
               property int counter: 2
               property int oldWidth
               property int oldHeight
               property double deltaW: 0
               property double deltaH: 0
               target: display
               onStateChanged: {
                   if(counter!=-1)
                       counter=2;
                   if(display.state==="offscreen")
                       counter=-1
               }

               onSizeChanged:{

                   if(counter>0){
                       if(display.state==="maximized"){
                           if(zoom*mapCanvas.width>=workspace.width)
                                scrollMap.flickableItem.contentX=scrollMap.flickableItem.contentWidth/2-scrollMap.flickableItem.width/2
                           else
                                scrollMap.flickableItem.contentX=-zoom*mapCanvas.width/2+mapCanvas.width/2

                           if(zoom*mapCanvas.height>=workspace.height)
                                scrollMap.flickableItem.contentY=scrollMap.flickableItem.contentHeight/2-scrollMap.flickableItem.height/2
                           else
                               scrollMap.flickableItem.contentY=-zoom*mapCanvas.height/2+mapCanvas.height/2

                       }
                       else{
                           scrollMap.flickableItem.contentX=scrollMap.flickableItem.contentWidth/2-scrollMap.flickableItem.width/2
                           scrollMap.flickableItem.contentY=scrollMap.flickableItem.contentHeight/2-scrollMap.flickableItem.height/2
                       }
                       counter--
                       oldWidth=w
                       oldHeight=h
                       flickX=scrollMap.flickableItem.contentX
                       flickY=scrollMap.flickableItem.contentY
                   }else{
                       deltaW+=(w-oldWidth)/2
                       deltaH+=(h-oldHeight)/2

                       if(display.state==="maximized"){
                           if(zoom*mapCanvas.width>=workspace.width)
                           {
                               var x=flickX-Math.round(deltaW)

                               if(x>=(-zoom*mapCanvas.width/2+mapCanvas.width/2)&&x<=(zoom*mapCanvas.width/2+mapCanvas.width/2-scrollMap.flickableItem.width))
                                    scrollMap.flickableItem.contentX=x
                               else{
                                   if(x<=(-zoom*mapCanvas.width/2+mapCanvas.width/2))
                                       scrollMap.flickableItem.contentX=-zoom*mapCanvas.width/2+mapCanvas.width/2
                                   else
                                       scrollMap.flickableItem.contentX=zoom*mapCanvas.width/2+mapCanvas.width/2-scrollMap.flickableItem.width
                               }
                           }else
                                scrollMap.flickableItem.contentX=-zoom*mapCanvas.width/2+mapCanvas.width/2

                           if(zoom*mapCanvas.height>=workspace.height)
                           {
                               var y=flickY-Math.round(deltaH)

                               if(y>=(-zoom*mapCanvas.height/2+mapCanvas.height/2)&&y<=(zoom*mapCanvas.height/2+mapCanvas.height/2-scrollMap.flickableItem.height))
                                    scrollMap.flickableItem.contentY=y
                               else{
                                   if(y<=(-zoom*mapCanvas.height/2+mapCanvas.height/2))
                                        scrollMap.flickableItem.contentY=-zoom*mapCanvas.height/2+mapCanvas.height/2
                                   else
                                       scrollMap.flickableItem.contentY=zoom*mapCanvas.height/2+mapCanvas.height/2-scrollMap.flickableItem.height
                               }
                           }else
                               scrollMap.flickableItem.contentY=-zoom*mapCanvas.height/2+mapCanvas.height/2

                           deltaW-=Math.round(deltaW)
                           deltaH-=Math.round(deltaH)
                           flickX=scrollMap.flickableItem.contentX
                           flickY=scrollMap.flickableItem.contentY
                       }else{
                           var x=flickX-Math.round(deltaW)
                           var y=flickY-Math.round(deltaH)

                           if(x>=(-zoom*mapCanvas.width/2+mapCanvas.width/2)&&x<=(zoom*mapCanvas.width/2+mapCanvas.width/2-scrollMap.flickableItem.width))
                                scrollMap.flickableItem.contentX=x
                           else{
                               if(x<=(-zoom*mapCanvas.width/2+mapCanvas.width/2))
                                   scrollMap.flickableItem.contentX=-zoom*mapCanvas.width/2+mapCanvas.width/2
                               else
                                   scrollMap.flickableItem.contentX=zoom*mapCanvas.width/2+mapCanvas.width/2-scrollMap.flickableItem.width
                           }

                           if(y>=(-zoom*mapCanvas.height/2+mapCanvas.height/2)&&y<=(zoom*mapCanvas.height/2+mapCanvas.height/2-scrollMap.flickableItem.height))
                                scrollMap.flickableItem.contentY=y
                           else{
                               if(y<=(-zoom*mapCanvas.height/2+mapCanvas.height/2))
                                    scrollMap.flickableItem.contentY=-zoom*mapCanvas.height/2+mapCanvas.height/2
                               else
                                   scrollMap.flickableItem.contentY=zoom*mapCanvas.height/2+mapCanvas.height/2-scrollMap.flickableItem.height
                           }

                           deltaW-=Math.round(deltaW)
                           deltaH-=Math.round(deltaH)
                           flickX=scrollMap.flickableItem.contentX
                           flickY=scrollMap.flickableItem.contentY
                       }
                       oldWidth=w
                       oldHeight=h
                   }
                   drawMap.maxSizeChanged(zoom*mapCanvas.width,zoom*mapCanvas.height)
                }
            }
        }
    }
}
