import QtQuick 2.0

Rectangle {
    id: statusItem
    height: 30
    width: 30
    color: "#ffebebeb"
    property alias animationItem: animationIcon.source
    property alias textItem: informationText.text
    property alias textHeight: decription.height
    property alias textWidth: decription.width
    property alias colorBox: statusItem.color

    signal descriptionShown()

    function hideDescription() {
        hideDelayer.running = false
    }

    onStateChanged: {
            descriptionShown()
    }
    AnimatedImage{
        id:animationIcon
        anchors.fill:statusItem

        MouseArea {
            id: hoverDetector
            anchors.fill: animationIcon
            hoverEnabled: true

            onExited: hideDelayer.running = true
        }

        Item {
            id: decription
            x: 30
            y: 5
            opacity: 0

            Rectangle{
                width: parent.width
                height: parent.height
                color: "#aaebebeb"
                Text {
                    id: informationText
                }
            }

            MouseArea {
                id: hoverDetectorInDescription
                anchors.fill: decription
                hoverEnabled: parent.opacity !== 0
                propagateComposedEvents: true

                onExited: hideDelayer.running = true
            }
        }

        Timer {
            id: hideDelayer
            interval: 1000
            running: false
            repeat: false
        }
    }

    states: [
        State {
            name: "mouseOver"
            when: {
//                loader.sourceComponent && ( hoverDetector.containsMouse || hoverDetectorInDescription.containsMouse || hideDelayer.running )
                hoverDetector.containsMouse || hoverDetectorInDescription.containsMouse || hideDelayer.running
            }

            PropertyChanges { target: decription; opacity: 1}
        }
    ]

    transitions: [
        Transition {
            PropertyAnimation { target: decription; properties: "opacity"; easing.type: Easing.OutQuint; duration: 500}
        }
    ]
}

