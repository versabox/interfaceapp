import QtQuick 2.0
import QtQuick.Window 2.2

Window{

    id: window

    visible: true
    flags: Qt.WA_TranslucentBackground | Qt.WindowDoesNotAcceptFocus | Qt.FramelessWindowHint
    color: "#00000000"

    property int lastWidth
    property int lastHeight

    Rectangle{
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0.8)
    }

    Connections{
        target: display
        onCordinateChanged: {
            window.x=x-25
            window.y=y
            if(y==0){
                window.showMaximized()
            }
            else{
                if(x==0){
                    window.height=Screen.height-30//TODO - wysokość paska
                    window.width=Screen.width/2
                }
                else{
                    window.height=lastHeight
                    window.width=lastWidth
                    window.showNormal()
                }
            }
        }
    }
    Component.onCompleted: {
        lastHeight=window.height
        lastWidth=window.width
    }
}

