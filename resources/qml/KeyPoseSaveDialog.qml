import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Dialog {
    id:dialogWindow
    width: 150
    visible: true
    title: client.translateIntoSelectedLanguage("KeyPose_Saver", language)

    property bool nameText: true
    property bool descriptionText: false
    property bool warningText: false
    property string language: "english"
    property string nameShadowText: qsTr(client.translateIntoSelectedLanguage("EnterName", language))
    property string nameShadowDescription: qsTr(client.translateIntoSelectedLanguage("EnterDescription", language))
    property string oldNameText: ""
    property string oldDescriptionText: ""
    property int oldType: -1
    property int oldTime: -1
    property int oldRadioButtonParam: -1
    property var connectAcceptFunction
    property var connectRejectFunction
    property int checkdRadioButton: 1

    standardButtons: StandardButton.Cancel | StandardButton.Ok

    signal signalAccept(string name, string description, int typeOfKeyPose, int typeOfTimeSet, int time)
    signal signalReject()

    TextField{
        id: name
        anchors.margins: 10
        visible: nameText
        width: parent.width
        height: 30
        focus: true
        placeholderText: nameShadowText
    }

    TextField{
        id: description
        anchors.top: name.bottom
        anchors.margins: 10
        visible: descriptionText
        width: parent.width
        height: 60
        placeholderText: nameShadowDescription
    }

    ComboBox {
        id: keyPoseType
        anchors.top: description.bottom
        anchors.margins: 10
        width: parent.width
        model: [client.translateIntoSelectedLanguage("ButtonWithDeadline", language),
            client.translateIntoSelectedLanguage("Simple", language),
            client.translateIntoSelectedLanguage("Trolley", language),
            client.translateIntoSelectedLanguage("Door", language),
            client.translateIntoSelectedLanguage("GiveWay", language),
            client.translateIntoSelectedLanguage("Charging", language),
            client.translateIntoSelectedLanguage("Lift", language)]
        onCurrentIndexChanged: {
            if(currentIndex === 0 || currentIndex === 1){
                radioButtonLayout.height = 90
                radioButtonLayout.visible = true
                radioSetTime.visible = true
                radioDefault.visible = true
                radioWait.visible = true
                timeDedline.visible = true
                timeDedline.height = 30
                dialogWindow.height = 200
            }
            else{
                radioButtonLayout.height = 0
                radioButtonLayout.visible = false
                radioSetTime.visible = false
                radioDefault.visible = false
                radioWait.visible = false
                timeDedline.visible = false
                timeDedline.height = 0
                dialogWindow.height = 150
            }
        }
    }
    ExclusiveGroup{
        id:timeButtonGroup
    }
    ColumnLayout{
        id:radioButtonLayout
        anchors.top: keyPoseType.bottom
        anchors.topMargin: 10
        width: parent.width
        RadioButton{
            id:radioSetTime
            text:client.translateIntoSelectedLanguage("SetTime", language)
            exclusiveGroup: timeButtonGroup
            onCheckedChanged: {
                if(checked){
                    checkdRadioButton = 0
                }
            }
        }
        RadioButton{
            id:radioDefault
            text:client.translateIntoSelectedLanguage("DeafultTime", language)
            checked: true
            exclusiveGroup: timeButtonGroup
            onCheckedChanged: {
                if(checked){
                    checkdRadioButton = 1
                }
            }
        }
        RadioButton{
            id:radioWait
            text:client.translateIntoSelectedLanguage("WaitForButton", language)
            exclusiveGroup: timeButtonGroup
            onCheckedChanged: {
                if(checked){
                    checkdRadioButton = 2
                }
            }
        }
        TextField{
            id: timeDedline
            height: 30
            placeholderText: client.translateIntoSelectedLanguage("EnterTime", language)
            validator: IntValidator{bottom: 0;}
            onFocusChanged: {
                if(timeDedline.activeFocus === true){
                    radioSetTime.checked = true
                }
            }
        }
    }

    onAccepted: {
        signalAccept(name.text, description.text, keyPoseType.currentIndex,
                     checkdRadioButton, parseInt(timeDedline.text,10))
    }
    onRejected: {

        signalReject()
    }

    Component.onCompleted: {
        signalAccept.connect(connectAcceptFunction)

        if(connectRejectFunction){
            signalReject.connect(connectRejectFunction)
        }

        if(oldNameText !== ""){

            name.text = oldNameText
        }
        if(oldDescriptionText !==""){

            description.text = oldDescriptionText
        }
        if(oldType !== -1){
            keyPoseType.currentIndex = oldType
        }
        if(oldTime >= 0){
            timeDedline.text = oldTime
        }
        if(oldRadioButtonParam === 0){
            radioSetTime.checked = true
        }
        if(oldRadioButtonParam === 1){
            radioDefault.checked = true
        }
        if(oldRadioButtonParam === 2){
            radioWait.checked = true
        }
        name.forceActiveFocus()
    }
}
