import QtQuick 2.0
import QtQml.Models 2.2

Rectangle {
    property int dockedWidth: 100
    property int dockedHeight: 80
    property var workspace

    id: displayBox
    height: dockedHeight
    width: box.width
    color: "#FFDDDDDD"
    radius: 10
    objectName: "displaybox"

    signal signalShowMap(int width, int height)
    signal signalMapWindowVisableChanged(bool visible)
    signal signalCameraWindowVisableChanged(bool visible)
    signal signalDisableSteering(bool enable)

    Row {
        id: box
        Display {
            id: camera
            property var self: camera
            title: "Camera"
            content: HeadCamera{}
            iconSource: "qrc:/images/CamraIcon.png"
            state: "docked"
            height: dockedHeight-10
            width: dockedWidth-10
            smallHeight: 320
            smallWidth: 480
            workspace: displayBox.workspace
            nameOfWindow: "HeadCameraWindow.qml"

            onGetMeOnTop:{
                for (var i = 0; i < box.children.length; ++i)
                {
                    if (box.children[i].title === self.title)
                        box.children[i].z = 2
                    else if (box.children[i].state === "maximized")
                        box.children[i].z = 0
                    else
                        box.children[i].z = 1
                }
            }

            onMaximizeMe:{
                for (var i = 0; i < box.children.length; ++i)
                {
                    if (box.children[i].state === "maximized" && box.children[i].title !== self.title)
                    {
                        box.children[i].state = ""
                        box.children[i].z = 1
                    }
                    else if (box.children[i].title === self.title)
                    {
                        box.children[i].state = "maximized"
                        box.children[i].z = 0
                    }
                }
            }
            onStateChanged: {
                if(state=="docked")
                    signalCameraWindowVisableChanged(false)
                else
                    signalCameraWindowVisableChanged(true)
            }
        }
        Display {
            id: map
            property var self: map
            title: "Map"
            content: Map {workspace: displayBox.workspace; display: map}
            iconSource: "qrc:/images/MapIcon.png"
            state: "docked"
            height: dockedHeight-10
            width: dockedWidth-10
            workspace: displayBox.workspace
            nameOfWindow: "MapWindow.qml"

            onGetMeOnTop:{
                if(map.state=="docked")
                    displayBox.signalShowMap(1000,1000)
                for (var i = 0; i < box.children.length; ++i)
                {
                    if (box.children[i].title === self.title)
                        box.children[i].z = 2
                    else if (box.children[i].state === "maximized")
                        box.children[i].z = 0
                    else
                        box.children[i].z = 1
                }
            }
            onMaximizeMe:{
                if(map.state=="docked")
                    displayBox.signalShowMap(1000,1000)
                for (var i = 0; i < box.children.length; ++i)
                {
                    if (box.children[i].state === "maximized" && box.children[i].title !== self.title)
                    {
                        box.children[i].state = ""
                        box.children[i].z = 1
                    }
                    else if (box.children[i].title === self.title)
                    {
                        box.children[i].state = "maximized"
                        box.children[i].z = 0
                    }
                }
            }
            onStateChanged: {
                if(state=="docked")
                    signalMapWindowVisableChanged(false)
                else
                    signalMapWindowVisableChanged(true)
            }
        }
        Display {
            id: joystick
            property var self: joystick
            title: "Joystick"
            content: Joystick {}
            iconSource: "qrc:/images/JoystickIcon.png"
            state: "docked"
            height: dockedHeight-10
            width: dockedWidth-10
            workspace: displayBox.workspace
            nameOfWindow: ""
            smallHeight: 150
            smallWidth: smallHeight
            stable: true
            onStateChanged: {
                if(joystick.state == "docked"){
                    displayBox.signalDisableSteering(false)
                }
                else{
                    displayBox.signalDisableSteering(true)
                }
            }
            onGetMeOnTop:{
                for (var i = 0; i < box.children.length; ++i)
                {
                    if (box.children[i].title === self.title){
                        box.children[i].z = 2
                    }
                    else if (box.children[i].state === "maximized")
                        box.children[i].z = 0
                    else
                        box.children[i].z = 1
                }
            }
            onMaximizeMe:{
                for (var i = 0; i < box.children.length; ++i)
                {
                    if (box.children[i].state === "maximized" && box.children[i].title !== self.title)
                    {
                        box.children[i].z = 1
                    }
                    else if (box.children[i].title === self.title)
                    {
                        box.children[i].state = "stable"
                        box.children[i].z = 0
                    }
                }
            }
        }
    }
}

