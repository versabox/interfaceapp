import QtQuick 2.5

Item {
    id: item
    signal maxSizeChanged()
    signal signalJoystickEnable()
    signal signalJoystickValueChanged(double linear,double angular)
    signal signalJoystickDisable()
    objectName: "joystick"
    property int joy: 0

    function getVelocity(linearTextValue, angularTextValue, linear, angular){
        linearText.text=linearTextValue
        angularText.text=angularTextValue
        if(joy == 0){
            circle.x= (-1.0*(content.width/2)*angular*0.7)+content.width/2-circle.height/2
            circle.y= (-1.0*(content.height/2)*linear*0.7)+content.height/2-circle.height/2
        }


    }

    Rectangle {
        color: Qt.rgba(0.8, 0, 0, 0.2)
        anchors.fill: parent
        Text{
            id:linearText
            anchors.top:parent.top
            anchors.right: parent.right
            text: "0%"
        }
        Text{
            id:angularText
            anchors.top:parent.top
            anchors.left: parent.left
            text: "0%"
        }

        Rectangle{
            id: content
            color: "red"
            width:100
            height:100
            radius: 20
            anchors.centerIn: parent

            Rectangle{
                id: circle
                color: "yellow"
                width:40
                height: width
                x: parent.width/2-width/2
                y: parent.height/2-height/2
                radius: width/2

                MouseArea{
                    id: dragger
                    anchors.fill: parent
                    drag.target: parent
                    drag.axis: Drag.XAndYAxis
                    drag.maximumX: content.width-circle.width/2
                    drag.maximumY: content.height-circle.height/2
                    drag.minimumX: -circle.width/2
                    drag.minimumY: -circle.height/2

                    onPressed: {
                        item.signalJoystickEnable()
                        joy=1;
                    }
                    onPositionChanged: {
                        item.signalJoystickValueChanged(-2*(circle.y-content.height/2+circle.height/2)/content.height,-2*(circle.x-content.width/2+circle.width/2)/content.width)
                    }
                    onReleased: {
                        item.signalJoystickDisable()
                        joy=0
                    }
                }
            }
        }
    }

    states: [
        State {
            name: "dragged"
            when: !dragger.drag.active
            PropertyChanges {
                target: circle
                x: content.width/2-circle.width/2
                y: content.height/2-circle.height/2
            }
        }
    ]
    transitions: [
        Transition {
            from: ""; to: "dragged"
            NumberAnimation { target:circle; properties: "x,y" ; easing.type: Easing.InOutQuad; duration: 300}
        }
    ]
}


