import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.2

Rectangle {
    id: menuPanelBar
    state: "hidden"
    height: workspaceHeight
    color: "#00000000"
    objectName: "menuPanelBar"
    property var workspaceHeight
    property int indexOfAddingMidpoint: 0
    property int hiddenWidth: 20
    property int chooseBarWidth: 30
    property int contentWidth: 200
    property string chooseColor: "#5E100C"
    property string chooseTextColor: "#E8E8E8"
    property string color1: "#FF2A20"
    property string color2: "#C72119"
    property string language: "english"
    // other colors "#DD251C" "#BA1F18" "#EA271E"

    signal signalAddMidpoint(bool on)
    signal signalRemoveMidpoint(bool on)
    signal signalEditMidpoint(bool on)
    signal signalRemoveAllMidpoint()
    signal signalSetRobotPosition(bool on)
    signal signalStartRobot()
    signal signalStopRobot()
    signal signalShownMenu()
    signal signalShownMap()
    signal signalShownArea1()
    signal signalShownArea2()
    signal signalLoadMap(int i, bool on)
    signal signalChangeRideMode(int i)
    signal signalChangeValueOfSpinBox()
    signal signalChangeStartingPoint(int which)
    signal signalAddKeyPose(bool on)
    signal signalDeleteKeyPose(bool on)
    signal signalAddKeyPoseAtRobotPosition(string name)
    signal signalSendToKeyPose(string name)
    signal signalBulidNewMap()
    signal signalSaveMap(string name)
    signal signalUpdateListOfMaps()
    signal signalVisibleKeyPose(int Id, bool on)
    signal signalAddMidKeyPoseGraph(bool on, bool accepted)
    signal signalSaveNoDrivingArea()
    signal signalDeleteNoDrivingArea()
    signal signalVisibleOfMidKeyPoseGraph(bool on)
    signal signalAddWarningZoneCircle()
    signal signalAddWarningZonePolygon()
    signal signalDeleteWarningZone()
    signal signalStopEditing()
    signal signalVisibleOfWarningZone(bool on)
    signal signalStateChanged()
    signal signalGraphOfKeyPosePath(bool on)
    signal signalEditKeyPose(bool on)
    signal signalAddNoDrivingArea(bool on)
    signal signalDockDeparture(int side);
    signal signalDockApproach(int side);
    signal signalSideButtonClicked(bool on);
    signal signalStopCharingButtonClicked();
    signal signalSkipButtonClicked();
    signal signalResetDiagnosticsAction(bool on)
    signal signalChangeLanguage(string lang)

    function addMidpoint (on){
        menuPanelBar.signalAddMidpoint(on)
    }
    function removeMidpoint (on){
        menuPanelBar.signalRemoveMidpoint(on)
    }
    function editMidpoint (on){
        menuPanelBar.signalEditMidpoint(on)
    }
    function removeAllMidpoint () {
        menuPanelBar.signalRemoveAllMidpoint()
    }
    function startRobot () {
        menuPanelBar.signalStartRobot()
    }
    function stopRobot () {
        menuPanelBar.signalStopRobot()
    }
    function loadMap (i, on) {
        menuPanelBar.signalLoadMap(i, on)
    }
    function changeRideMode (i) {
        menuPanelBar.signalChangeRideMode(i)
    }
    function changeValueOfSpinBox () {
        menuPanelBar.signalChangeValueOfSpinBox()
    }
    function setRobotPosition (on) {
        menuPanelBar.signalSetRobotPosition(on)
    }
    function changeRobot (which){
        menuPanelBar.signalChangeRobot(which)
    }
    function changeStartingPoint(which){
        menuPanelBar.signalChangeStartingPoint(which)
    }
    function addKeyPose (on){
        menuPanelBar.signalAddKeyPose(on)
    }
    function deleteKeyPose (on){
        menuPanelBar.signalDeleteKeyPose(on)
    }
    function addKeyPoseAtRobotPosition(name){
        menuPanelBar.signalAddKeyPoseAtRobotPosition(name)
    }
    function sendToKeyPose(name){
        menuPanelBar.signalSendToKeyPose(name)
    }
    function bulidNewMap(){
        menuPanelBar.signalBulidNewMap()
    }
    function saveMap(name){
        menuPanelBar.signalSaveMap(name)
    }
    function updateListOfMaps(){
        menuPanelBar.signalUpdateListOfMaps()
    }
    function visibleKeyPose(Id,on){
        menuPanelBar.signalVisibleKeyPose(Id,on)
    }
    function addMidKeyPoseGraph(on,accepted){
        menuPanelBar.signalAddMidKeyPoseGraph(on,accepted)
    }
    function saveNoDrivingArea(){
        menuPanelBar.signalSaveNoDrivingArea()
    }
    function deleteNoDrivingArea(){
        menuPanelBar.signalDeleteNoDrivingArea()
    }
    function visibleOfMidKeyPoseGraph(on){

        menuPanelBar.signalVisibleOfMidKeyPoseGraph(on)
    }
    function addWarningZone(name, description, type){
        if(type === 0){

            menuPanelBar.signalAddWarningZonePolygon()
        }
        else{

            menuPanelBar.signalAddWarningZoneCircle()
        }
    }
    function deleteWarningZone(){
        menuPanelBar.signalDeleteWarningZone()
    }
    function visibleOfWarningZone(on){
        menuPanelBar.signalVisibleOfWarningZone(on)
    }
    function graphOfKeyPosePath(on){
        menuPanelBar.signalGraphOfKeyPosePath(on)
    }
    function editKeyPose(on){
        menuPanelBar.signalEditKeyPose(on)
    }
    function addNoDrivingArea(on){
        menuPanelBar.signalAddNoDrivingArea(on)
    }
    function stopEditing(){
        menuPanelBar.signalStopEditing()
    }
    function dockDeparture(side){
        menuPanelBar.signalDockDeparture(side)
    }
    function dockApproach(side){
        menuPanelBar.signalDockApproach(side)
    }
    function sideButtonClicked(on){
        menuPanelBar.signalSideButtonClicked(on)
    }
    function stopCharingButtonClicked(){
        menuPanelBar.signalStopCharingButtonClicked()
    }
    function skipButtonClicked(){
        menuPanelBar.signalSkipButtonClicked()
    }
    function changeLanguage(lang){
        menuPanelBar.signalChangeLanguage(lang)
    }

    onStateChanged: {
        menuPanelBar.signalStateChanged()
    }
    function resetDiagnosticsAction (on){
        menuPanelBar.signalResetDiagnosticsAction(on)
    }

    Rectangle{
        id:bar
        anchors.top: parent.top
        anchors.left: parent.left
        height: parent.height
        width: 2 * hiddenWidth
        color: "#00000000"

        Rectangle{
            id: firstBar
            anchors.top: parent.top
            anchors.left: parent.left
            height: parent.height
            width: hiddenWidth
            color: "#00000000"

           Rectangle {
                id: barMenu
                color: color2
                height: parent.height/4
                width: parent.width
                anchors.right: parent.right

                Text {
                    id: panelTextMenu
                    text: client.translateIntoSelectedLanguage("Control_Panel", language)
                    rotation: -90
                    anchors.horizontalCenter: barMenu.horizontalCenter
                    anchors.verticalCenter: barMenu.verticalCenter
                }

            }

            Rectangle {
                id: barMap
                color: color1
                height: parent.height/4
                width: parent.width
                anchors.right: parent.right
                anchors.top: barMenu.bottom

                Text {
                    id: panelTextMap
                    text: client.translateIntoSelectedLanguage("Points_Panel", language)
                    rotation: -90
                    anchors.horizontalCenter: barMap.horizontalCenter
                    anchors.verticalCenter: barMap.verticalCenter
                }
            }

            Rectangle {
                 id: barArea2
                 color: color2
                 height: parent.height/4
                 width: parent.width
                anchors.right: parent.right
                anchors.top: barMap.bottom

                 Text {
                     id: panelTextArea2
                     text: client.translateIntoSelectedLanguage("Map_Panel", language)
                     rotation: -90
                     anchors.horizontalCenter: barArea2.horizontalCenter
                     anchors.verticalCenter: barArea2.verticalCenter
                 }
             }

             Rectangle {
                 id: barArea1
                 color: color1
                 height: parent.height/4
                 width: parent.width
                anchors.right: parent.right
                anchors.top: barArea2.bottom

                 Text {
                     id: panelTextArea1
                     text: client.translateIntoSelectedLanguage("Keypose_Panel", language)
                     rotation: -90
                     anchors.horizontalCenter: barArea1.horizontalCenter
                     anchors.verticalCenter: barArea1.verticalCenter
                 }
             }
             MouseArea {
                 id: mouseMenuDetector
                 anchors.fill: barMenu
                 propagateComposedEvents: true

                 onClicked: {
                     if(menuPanelBar.state=="shownMenu")
                         menuPanelBar.state = "hidden"
                     else{
                         menuPanelBar.state = "shownMenu"
                         menuPanelBar.signalShownMenu()
                     }
                 }
             }

             MouseArea {
                 id: mouseMapDetector
                 anchors.fill: barMap
                 propagateComposedEvents: true

                 onClicked: {
                     if(menuPanelBar.state=="shownMap")
                         menuPanelBar.state = "hidden"
                     else{
                         menuPanelBar.state = "shownMap"
                         menuPanelBar.signalShownMap()
                     }
                 }
             }

             MouseArea {
                 id: mouseArea2Detector
                 anchors.fill: barArea2
                 propagateComposedEvents: true

                 onClicked: {
                     if(menuPanelBar.state=="shownArea2")
                         menuPanelBar.state = "hidden"
                     else{
                         menuPanelBar.state = "shownArea2"
                         menuPanelBar.signalShownArea2()

                     }
                 }
             }

             MouseArea {
                 id: mouseArea1Detector
                 anchors.fill: barArea1
                 propagateComposedEvents: true

                 onClicked: {
                     if(menuPanelBar.state=="shownArea1")
                         menuPanelBar.state = "hidden"
                     else{
                         menuPanelBar.state = "shownArea1"
                         menuPanelBar.signalShownArea1()

                     }
                 }
             }
        }

        Rectangle{
            id:secondBar
            anchors.top: parent.top
//            anchors.left: firstBar.right
            anchors.right: parent.right
            height: parent.height
            width: hiddenWidth
            color: "#00000000"

           Rectangle {
                id: secondBarMenu1
                color: color1
                height: parent.height/4
                width: parent.width
                anchors.right: parent.right

                Text {
                    id: secondBarText1
                    text: client.translateIntoSelectedLanguage("KeyPose_Path", language)
                    rotation: -90
                    anchors.horizontalCenter: secondBarMenu1.horizontalCenter
                    anchors.verticalCenter: secondBarMenu1.verticalCenter
                }
            }

            Rectangle {
                id: secondBarMenu2
                color: color2
                height: parent.height/4
                width: parent.width
                anchors.right: parent.right
                anchors.top: secondBarMenu1.bottom

                Text {
                    id: secondBarText2
                    text: ""
                    rotation: -90
                    anchors.horizontalCenter: secondBarMenu2.horizontalCenter
                    anchors.verticalCenter: secondBarMenu2.verticalCenter
                }
            }

            Rectangle {
                 id: secondBarMenu3
                 color: color1
                 height: parent.height/4
                 width: parent.width
                anchors.right: parent.right
                anchors.top: secondBarMenu2.bottom

                 Text {
                     id: secondBarText3
                     text: ""
                     rotation: -90
                     anchors.horizontalCenter: secondBarMenu3.horizontalCenter
                     anchors.verticalCenter: secondBarMenu3.verticalCenter
                 }
             }

             Rectangle {
                 id: secondBarMenu4
                 color: color2
                 height: parent.height/4
                 width: parent.width
                anchors.right: parent.right
                anchors.top: secondBarMenu3.bottom

                 Text {
                     id: secondBarText4
                     text: ""
                     rotation: -90
                     anchors.horizontalCenter: secondBarMenu4.horizontalCenter
                     anchors.verticalCenter: secondBarMenu4.verticalCenter
                 }
             }

             MouseArea {
                 id: mouseSecondBarTextDetector1
                 anchors.fill: secondBarMenu1
                 propagateComposedEvents: true

                 onClicked: {
                     if(menuPanelBar.state=="shownSecondBarMenu1")
                         menuPanelBar.state = "hidden"
                     else{
                         menuPanelBar.state = "shownSecondBarMenu1"
                     }
                 }
             }
        }
    }

    Loader{
        id: content
        height: menuPanelBar.height
        anchors.left: /*menuPanelBar.right*/bar.right
        sourceComponent:
            MenuPanelContent{
            id:menuPanelContent
            }
    }
    Loader{
        id: contentKeyPosePath
        height: menuPanelBar.height
        anchors.left: menuPanelBar.right
        property int contentWidth: 500
        sourceComponent:
            KeyPosePath{
            id: keyPosePath
            width: contentWidth
        }
    }

    states: [
        State {
            name: "hidden"
            PropertyChanges { target: content;  width: 0}
            PropertyChanges { target: contentKeyPosePath;  width: 0}
            PropertyChanges { target: menuPanelBar;  width: 2 * hiddenWidth}
        },
        State {
            name: "shownMenu"
            PropertyChanges { target: barMenu; color: chooseColor}
            PropertyChanges { target: content; width: contentWidth}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentWidth}
            PropertyChanges { target: panelTextMenu; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: content; anchors.left: bar.right}
        },
        State {
            name: "shownMap"
            PropertyChanges { target: barMap; color: chooseColor}
            PropertyChanges { target: content; width: contentWidth}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentWidth}
            PropertyChanges { target: panelTextMap; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: content; anchors.left: bar.right}
        },
        State {
            name: "shownArea2"
            PropertyChanges { target: barArea2; color: chooseColor}
            PropertyChanges { target: content; width: contentWidth}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentWidth}
            PropertyChanges { target: panelTextArea2; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: content; anchors.left: bar.right}
        },
        State {
            name: "shownArea1"
            PropertyChanges { target: barArea1; color: chooseColor}
            PropertyChanges { target: content; width: contentWidth}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentWidth}
            PropertyChanges { target: panelTextArea1; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: content; anchors.left: bar.right}
        },
        State {
            name: "shownSecondBarMenu1"
            PropertyChanges { target: secondBarMenu1; color: chooseColor}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentKeyPosePath.contentWidth}
            PropertyChanges { target: secondBarText1; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: contentKeyPosePath; anchors.left: bar.right }
            AnchorChanges   { target: content; anchors.left: menuPanelBar.right }

        },
        State {
            name: "shownSecondBarMenu2"
            PropertyChanges { target: secondBarMenu2; color: chooseColor}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentWidth}
            PropertyChanges { target: secondBarText2; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: content; anchors.left: menuPanelBar.right }
        },
        State {
            name: "shownSecondBarMenu3"
            PropertyChanges { target: secondBarMenu3; color: chooseColor}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentWidth}
            PropertyChanges { target: secondBarText3; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: content; anchors.left: menuPanelBar.right }
        },
        State {
            name: "shownSecondBarMenu4"
            PropertyChanges { target: secondBarMenu4; color: chooseColor}
            PropertyChanges { target: menuPanelBar;  width:  2 * hiddenWidth + contentWidth}
            PropertyChanges { target: secondBarText4; font.bold: true; color: chooseTextColor}
            AnchorChanges   { target: content; anchors.left: menuPanelBar.right }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation { target:menuPanelBar; properties: "width" ; easing.type: Easing.OutExpo; duration: 1000 }
            ColorAnimation  { properties: "color"; duration: 1000}
            AnchorAnimation { targets: [contentKeyPosePath, content]; duration: 0}
        },
        Transition {
            to: "hidden"
            NumberAnimation { target:content; properties: "width" ; easing.type: Easing.OutExpo; duration: 2000 }
            NumberAnimation { target:contentKeyPosePath; properties: "width" ; easing.type: Easing.OutExpo; duration: 2000 }
            NumberAnimation { target:menuPanelBar; properties: "width" ; easing.type: Easing.OutExpo; duration: 1000 }
            ColorAnimation  { properties: "color"; duration: 1000}
            AnchorAnimation { targets: [contentKeyPosePath, content]; duration: 1000}
        }
    ]
}
