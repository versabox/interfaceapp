import QtQuick 2.0

Item{
    id: pushButton

    property var text
    property bool clicked
    property bool dynamicKeyPose: false
    property bool dynamicCheckBox: false
    property int  number_of_pushbutton: -2

    signal buttonClicked
    signal buttonPressed
    signal buttonReleased

    Rectangle {
        id: background
        anchors.fill: pushButton
//        color: "#BA1F18"//"#A0A0A0"
        radius: 8
        gradient: normalGradient


        Gradient {
            id:normalGradient
            GradientStop { position: 0.0; color: "#EA271E" }
            GradientStop { position: 1.0; color: "#BA1F18" }
        }
        Gradient {
            id:clickedGradient
            GradientStop { position: 0.0; color: "#D8D8D8" }
            GradientStop { position: 1.0; color: "gray" }
        }
        state:{
            if(clicked)
                "clicked"
            else
                mouseArea.pressed ? "clicked" : ""
        }

        states:[
            State{
                name: "clicked"
                PropertyChanges { target: background; color: "#D8D8D8"; gradient: clickedGradient }
                PropertyChanges { target: name; color: "black"}
            },
            State{
                name: "clicked_mark"
                PropertyChanges { target: background; color: "#D828D8"}
            }
        ]
        Image {
            id: mark
            anchors.fill: parent
            source: "qrc:/images/checkmark.png"
            visible: false
            state:{
                if(pushButton.clicked && pushButton.dynamicCheckBox)
                    "clicked_mark"
                else
                    mouseArea.pressed ? "clicked" : ""
            }
            states:[
                State{
                    name: "clicked_mark"
                    PropertyChanges { target: mark; visible: true }
                }
            ]
        }
    }

    Text {
        property int textSize : 11
        id: name
        text: pushButton.text
        anchors.centerIn: pushButton
        color: "#E8E8E8"
        font.pointSize: textSize
    }

    MouseArea {
        id: mouseArea
        anchors.fill: pushButton


        onPressed: {
            buttonPressed()
        }
        onReleased: {
            buttonReleased()
        }

        onClicked:{
            buttonClicked()

            if(pushButton.dynamicCheckBox){

                if(pushButton.clicked){

                    pushButton.clicked = false
                }
                else{

                    pushButton.clicked = true
                }
                menuPanelBar.visibleKeyPose(number_of_pushbutton, pushButton.clicked)
            }

            if(dynamicKeyPose){

                menuPanelBar.sendToKeyPose(text)
            }
        }
    }
}




