#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickView>
#include <QDebug>
#include <QObject>
#include <QQuickImageProvider>
#include <QQmlComponent>
#include <iostream>
#include "include/interface/headcameraimageprovider.h"
#include "include/interface/client.h"
#include "include/interface/findqmlobject.h"
#include "include/interface/contextpropertyrefresher.h"
#include "QEventLoop"

using namespace std;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QThread t;

    QQmlApplicationEngine engine;
    QQmlContext* context = engine.rootContext();

    ContextPropertyRefresher contextPropertyRefresher_(context);

    Client myClient(context);

    qmlRegisterType<KeyPoseElement>("VbType",1,0,"KeyPoseElement");
    qmlRegisterType<MidKeyPoseGraphElement>("VbType",1,0,"MidKeyPoseGraphElement");
    qmlRegisterType<WarningZoneElement>("VbType", 1, 0, "WarningZoneElement");
    qmlRegisterType<KeyPosePathViewElement>("VbType", 1, 0, "KeyPosePathViewElement");
    qmlRegisterType<Point>("VbType", 1, 0, "Point");

    context->setContextProperty("client", &myClient);
    context->setContextProperty("keyPoseListViewModel", QVariant::fromValue(myClient.keyPoseListViewList));
    context->setContextProperty("keyPoseGridViewModel", QVariant::fromValue(myClient.keyPoseGridViewList));
    context->setContextProperty("keyPosePathGridViewModel", QVariant::fromValue(myClient.keyPosePathGridViewList));

    HeadCameraImageProvider *headCameraImageProvider = new HeadCameraImageProvider;

    engine.addImageProvider("headCamera",headCameraImageProvider);

    engine.load(QUrl(QStringLiteral("qrc:/qml/LogInWindow.qml")));

    QObject *obj1=engine.rootObjects()[0];

    myClient.moveToThread(&t);
    myClient.changeLanguage("english");

    t.start();

    QEventLoop loop;

    QObject::connect(obj1,SIGNAL(signalAccept(QString,QString,int,QString, int)), &myClient,SLOT(changeRobot(QString,QString,int,QString, int)));
    QObject::connect(obj1,SIGNAL(signalReject()),&loop,SLOT(quit()));
    QObject::connect(&myClient,&Client::signalLogInRejected,&loop,&QEventLoop::exit);
    QObject::connect(&myClient,SIGNAL(signalLogInAccepted(QVariant)),obj1,SLOT(loginStatus(QVariant)));

    int i=loop.exec();

    if(i==0)
    {
        QTimer::singleShot(0,&app,SLOT(quit()));
        t.quit();
        t.wait();
        return app.exec();
    }

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    QObject *obj=engine.rootObjects()[1];

    QObject *displayBox=obj->findChild<QObject*>("displaybox");
    QObject *menuPanel=obj->findChild<QObject*>("menuPanelBar");
    QObject *menuPanelContent=obj->findChild<QObject*>("menuPanelContent");
    QObject *map=FindItemByName(&engine,"map");
    QObject *headCamera=FindItemByName(&engine,"headcamera");
    QObject *joystick=obj->findChild<QObject*>("joystick");
    QObject *statusPanelContent=FindItemByName(&engine,"statusPanelContent");
    QObject *keyPosePath = obj->findChild<QObject*>("keyPosePath");

    QObject::connect(&myClient,SIGNAL(updateMapNow()),map,SLOT(updateMap()));
    QObject::connect(&myClient,SIGNAL(updateImageMapOnly()),map,SLOT(rePaint()));
    QObject::connect(&myClient,SIGNAL(headPositionChanged()),headCamera,SLOT(updateHeadPosition()));
    QObject::connect(&myClient,SIGNAL(newHeadImageReceived(QImage)),headCameraImageProvider,SLOT(newImage(QImage)));
    QObject::connect(displayBox, SIGNAL(signalShowMap(int,int)),&myClient,SLOT(showMap(int,int)), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalStartRobot()),&myClient,SLOT(startRobot()), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalStopRobot()),&myClient,SLOT(stopRobot()), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalRemoveAllMidpoint()),&myClient,SLOT(removeAllMidpoint()), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalLoadMap(int,bool)),&myClient,SLOT(loadMap(int,bool)), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalChangeRideMode(int)),&myClient,SLOT(setRideMode(int)), Qt::QueuedConnection);
    QObject::connect(&myClient,SIGNAL(midpointsChanged()),menuPanel,SLOT(changeValueOfSpinBox()));
    QObject::connect(headCamera,SIGNAL(signalMoveHead(double,double)),&myClient,SLOT(moveHead(double,double)),Qt::QueuedConnection);
    QObject::connect(headCamera,SIGNAL(signalBaseHead()),&myClient,SLOT(baseHead()),Qt::QueuedConnection);
    QObject::connect(headCamera,SIGNAL(signalMoveAbsHead(double,double)),&myClient,SLOT(moveAbsHead(double,double)),Qt::QueuedConnection);
    QObject::connect(&myClient,SIGNAL(updateHeadCameraNow()),headCamera,SLOT(updateHeadCamera()),Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalChangeRideMode(int)),&myClient,SLOT(setRideMode(int)), Qt::QueuedConnection);
    QObject::connect(&myClient, SIGNAL(signalUpdateListOfMaps()), menuPanelContent, SLOT(updateListOfMaps()), Qt::QueuedConnection);
    QObject::connect(joystick, SIGNAL(signalJoystickEnable()),&myClient,SLOT(joystickEnable()), Qt::QueuedConnection);
    QObject::connect(joystick, SIGNAL(signalJoystickDisable()),&myClient,SLOT(joystickDisable()), Qt::QueuedConnection);
    QObject::connect(joystick, SIGNAL(signalJoystickValueChanged(double,double)),&myClient,SLOT(joystickValueChanged(double,double)), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalChangeStartingPoint(int)),&myClient,SLOT(changeStartingPoint(int)), Qt::QueuedConnection);
    QObject::connect(&myClient,SIGNAL(refreshKeyPoseQmlList()),menuPanelContent,SLOT(refreshKeyPoseList()),Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalSendToKeyPose(QString)),&myClient,SLOT(sendToKeyPose(QString)), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalBulidNewMap()),&myClient,SLOT(bulidNewMap()), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalUpdateListOfMaps()),&myClient,SLOT(getListOfMaps()), Qt::QueuedConnection);
    QObject::connect(displayBox, SIGNAL(signalMapWindowVisableChanged(bool)),&myClient,SLOT(mapWindowVisableChanged(bool)), Qt::QueuedConnection);
    QObject::connect(displayBox, SIGNAL(signalCameraWindowVisableChanged(bool)),&myClient,SLOT(cameraWindowVisableChanged(bool)), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalSetCameraSteeringPose(double,double)),&myClient,SLOT(moveAbsHead(double,double)), Qt::QueuedConnection);
    QObject::connect(obj, SIGNAL(signalSetKeyboardSpeed(double,double)),&myClient,SLOT(joystickValueChanged(double,double)), Qt::QueuedConnection);
    QObject::connect(obj, SIGNAL(signalKeyboardEnable()),&myClient,SLOT(joystickEnable()), Qt::QueuedConnection);
    QObject::connect(obj, SIGNAL(signalKeyboardDisable()),&myClient,SLOT(joystickDisable()), Qt::QueuedConnection);
    QObject::connect(obj, SIGNAL(signalHeadCameraMoveKeyboard(double,double)),&myClient,SLOT(setHeadCameraSpeedKeyboard(double,double)), Qt::QueuedConnection);
    QObject::connect(displayBox, SIGNAL(signalDisableSteering(bool)),&myClient,SLOT(setEnableSteering(bool)), Qt::QueuedConnection);
    QObject::connect(displayBox, SIGNAL(signalCameraWindowVisableChanged(bool)),&myClient,SLOT(setEnableMoveHeadKeyboard(bool)), Qt::QueuedConnection);
    QObject::connect(&myClient, SIGNAL(sendVelocity(QVariant,QVariant,QVariant,QVariant)), joystick, SLOT(getVelocity(QVariant,QVariant,QVariant,QVariant)), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalTurnOnLaser(bool)),&myClient,SLOT(changeRobotInfoType(bool)), Qt::QueuedConnection);
    QObject::connect(&myClient, SIGNAL(signalRefreshBatteryState(QVariant, QVariant)), statusPanelContent, SLOT(batteryState(QVariant, QVariant)), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalChangeMidpointsTolerance(double,double)),&myClient,SLOT(changeMidpointsTolerance(double,double)), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalVisibleKeyPose(int,bool)),&myClient,SLOT(setVisibleOfKeyPoses(int,bool)), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalLift(bool)),&myClient,SLOT(liftHandler(bool)), Qt::QueuedConnection);
    QObject::connect(&myClient, SIGNAL(signalLiftAction(QVariant,QVariant)),menuPanelContent,SLOT(liftStatus(QVariant,QVariant)), Qt::QueuedConnection);
    QObject::connect(&myClient, SIGNAL(signalRefreshContextProperties(QVariant,QString)), &contextPropertyRefresher_, SLOT(refreshProperty(QVariant,QString)));
    QObject::connect(menuPanel, SIGNAL(signalStateChanged()),&myClient,SLOT(refreshMenuContent()), Qt::QueuedConnection);
    QObject::connect(menuPanel, SIGNAL(signalChangeLanguage(QString)),&myClient,SLOT(changeLanguage(QString)));

    //keyPosePath
    QObject::connect(keyPosePath, SIGNAL(signalAddNextKeyPoseToPath(QString)),&myClient,SLOT(keyPoseGridViewChooseHandler(QString)), Qt::QueuedConnection);
    QObject::connect(keyPosePath, SIGNAL(signalMoveListItem(int,int)),&myClient,SLOT(moveItemInListView(int,int)), Qt::QueuedConnection);
    QObject::connect(keyPosePath, SIGNAL(signalDeleteItemFromListView(int)),&myClient,SLOT(deleteItemFromListView(int)), Qt::QueuedConnection);
    QObject::connect(keyPosePath, SIGNAL(signalSaveKeyPosePath(QString,QString)),&myClient,SLOT(saveKeyPosePath(QString,QString)), Qt::QueuedConnection);
    QObject::connect(keyPosePath, SIGNAL(signalLoadKeyPosePath(QString)),&myClient,SLOT(loadKeyPosePath(QString)), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalStartPath(QString)),&myClient,SLOT(startKeyPosePath(QString)), Qt::QueuedConnection);
    QObject::connect(keyPosePath, SIGNAL(signalSendToKeyPose(QString)),&myClient,SLOT(sendToKeyPose(QString)), Qt::QueuedConnection);
    QObject::connect(keyPosePath, SIGNAL(startKeyPosePath(bool)),&myClient,SLOT(startKeyPosePath(bool)), Qt::QueuedConnection);

    QObject::connect(&myClient , SIGNAL(signalSetCurrentIndexOfListView(QVariant)),keyPosePath,SLOT(setCurrentIndexOfListView(QVariant)), Qt::QueuedConnection);
    QObject::connect(&myClient , SIGNAL(signalKeyPosePathList()),menuPanelContent,SLOT(updateListOfKeyPosePath()), Qt::QueuedConnection);

    //midPoints
    QObject::connect(map , SIGNAL(signalAddMidpoint(double,double,double,int)),&myClient,SLOT(addMidpoint(double,double,double,int)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalRemoveMidpoint(double,double)),&myClient,SLOT(removeMidpoint(double,double)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalRemoveMidpointIndex(int)),&myClient,SLOT(removeMidpointIndex(int)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalRemoveAllMidpoint()),&myClient,SLOT(removeAllMidpoint()), Qt::QueuedConnection);

    //keyPoses
    QObject::connect(map , SIGNAL(signalAddKeyPose(QString,QString,double,double,double,int,int,int)),
                     &myClient,SLOT(addKeyPose(QString,QString,double,double,double,int,int,int)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalDeleteKeyPose(double,double)),&myClient,SLOT(deleteKeyPose(double,double)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalDeleteKeyPoseName(QString)),&myClient,SLOT(deleteKeyPoseName(QString)), Qt::QueuedConnection);
    QObject::connect(menuPanelContent , SIGNAL(signalAddKeyPoseAtRobotPosition(QString,QString,int,int,int)),
                     &myClient,SLOT(addKeyPoseAtRobotPosition(QString,QString,int,int,int)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalSendToKeyPose(QString)),&myClient,SLOT(sendToKeyPose(QString)), Qt::QueuedConnection);

    //warningArea
    QObject::connect(map , SIGNAL(signalAddWarningZoneCircle(double,double,double,QString,int)),&myClient,SLOT(addWarningZone(double,double,double,QString,int)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalAddWarningZonePolygon(QString,int)),&myClient,SLOT(addWarningZone(QString,int)), Qt::QueuedConnection);
    QObject::connect(map , SIGNAL(signalDeleteWarningZone(QString)),&myClient,SLOT(deleteWarningZone(QString)), Qt::QueuedConnection);

    //midKeyPoseGraph
    QObject::connect(menuPanel , SIGNAL(signalAddMidKeyPoseGraph(bool,bool)),&myClient,SLOT(saveMidKeyPoseGraph(bool,bool)), Qt::QueuedConnection);

    //keyPosePath
    QObject::connect(map , SIGNAL(signalStartKeyPosePath(bool)),&myClient,SLOT(startKeyPosePath(bool)), Qt::QueuedConnection);

    //robot
    QObject::connect(map , SIGNAL(signalSetRobotPosition(double,double,double)),&myClient,SLOT(setRobotPosition(double,double,double)), Qt::QueuedConnection);
    QObject::connect(menuPanel , SIGNAL(signalDockDeparture(int)),&myClient,SLOT(dockDeparture(int)), Qt::QueuedConnection);
    QObject::connect(menuPanel , SIGNAL(signalDockApproach(int)),&myClient,SLOT(dockApproach(int)), Qt::QueuedConnection);
    QObject::connect(menuPanel , SIGNAL(signalSideButtonClicked(bool)),&myClient,SLOT(sideButtonClicked(bool)), Qt::QueuedConnection);
    QObject::connect(menuPanel , SIGNAL(signalStopCharingButtonClicked()),&myClient,SLOT(stopCharingButtonClicked()), Qt::QueuedConnection);
    QObject::connect(menuPanel , SIGNAL(signalSkipButtonClicked()),&myClient,SLOT(skipButtonClicked()), Qt::QueuedConnection);

    //map
    QObject::connect(map , SIGNAL(signalSaveMap(QString)),&myClient,SLOT(saveMap(QString)), Qt::QueuedConnection);
    QObject::connect(menuPanel , SIGNAL(signalSaveMap(QString)),&myClient,SLOT(saveMap(QString)), Qt::QueuedConnection);

    //locker
    QObject::connect(menuPanelContent, SIGNAL(signalLockerAction(int)),&myClient,SLOT(lockerHandler(int)), Qt::QueuedConnection);

    //brake
    QObject::connect(menuPanelContent, SIGNAL(signalBrakeModeChanged(bool)),&myClient,SLOT(brakeModeHandler(bool)), Qt::QueuedConnection);

    //reset diagnostics
    QObject::connect(menuPanel, SIGNAL(signalResetDiagnosticsAction(bool)),&myClient,SLOT(resetDiagnosticsAction(bool)), Qt::QueuedConnection);

    //task queue
    QObject::connect(menuPanelContent, SIGNAL(signalSuspendTaskQueue()),&myClient,SLOT(suspendTaskQueue()), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalUnsuspendTaskQueue()),&myClient,SLOT(unsuspendTaskQueue()), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalAbortTask()),&myClient,SLOT(abortTask()), Qt::QueuedConnection);
    QObject::connect(menuPanelContent, SIGNAL(signalClearTaskQueue()),&myClient,SLOT(clearTaskQueue()), Qt::QueuedConnection);

    //startApp
    QTimer::singleShot(0, &myClient, SLOT(startApp()));

    int k=app.exec();

    t.quit();
    t.wait();

    return k;
}
