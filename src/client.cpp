#include "include/interface/client.h"

Client::Client(QQmlContext* &context,QObject *parent):
    context_(context) ,QObject(parent)
{
    horizontalMoveHead=0;
    verticalMoveHead=0;
    qmlAngleOfLaserScan=M_PI;

}

Client::~Client()
{
    logOut();
}

QString Client::translateIntoSelectedLanguage(QString id, QString language)
{
    muteDictionary.lock();
    for (auto& x: dictionaries) {
        if(id == x.first){
            muteDictionary.unlock();
            return x.second;
        }
      }
    return {};
}

void Client::changeLanguage(QString language)
{
    muteDictionary.lock();
    dictionaries.clear();

    QString jsonContent;
    QString dictionaryPath = ":/dictionary/" + language + ".json";
    QFile file(dictionaryPath);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    jsonContent = (QString)file.readAll();
    file.close();

    QJsonDocument JsonToUtf8 = QJsonDocument::fromJson(jsonContent.toUtf8());
    QJsonObject dictionaryObject = JsonToUtf8.object();
    QJsonArray dictionaryContent = dictionaryObject["dictionary"].toArray();

    for(auto jsonIterator : dictionaryContent)
    {
         if (jsonIterator.toObject().contains("id") && jsonIterator.toObject()["id"].isString() &&
              jsonIterator.toObject().contains("translation") && jsonIterator.toObject()["translation"].isString())
              {
                  dictionaries.emplace(jsonIterator.toObject()["id"].toString(), jsonIterator.toObject()["translation"].toString());
              }
    }
    muteDictionary.unlock();
}

QList<double> Client::getMidpointToRemove(double firstMeterX, double firstMeterY)
{
    QList<double> qmlEditPoint;
    for(int i = 0 ; i<midpointList.size();i++){
        if(firstMeterX < (midpointList[i][0]+20) && firstMeterX > (midpointList[i][0]-20))
            if(firstMeterY < (midpointList[i][1]+20) && firstMeterY > (midpointList[i][1]-20)){
                qmlEditPoint.clear();
                qmlEditPoint.append(midpointList[i][2]);
                qmlEditPoint.append(i);
                emit updateImageMapOnly();
                break;
            }
    }
    return qmlEditPoint;
}

QVector2D Client::getKeyPosePositionFromName(const QString name)
{
    for(auto tempElement : qmlKeyPoseList){

        if(tempElement->getName() == name ){

           return QVector2D(tempElement->getX(), tempElement->getY());
        }
    }

    return QVector2D(0,0);
}

void Client::logIn(QString login, QString password, int port)
{
    login_ = login;
    password_ = password;
    port_ = port;
    httpclient=new jsonrpc::HttpClient(serverAdress.toStdString() + ":" + QString::number(port).toStdString());

    if (localConnection)
        httpclient->SetTimeout(1000);

    client=new StubClient(*httpclient);

    try
    {
        this->sessionId=client->logIn(password.toStdString(),login.toStdString());
        signalLogInAccepted(0);
        httpclient->SetTimeout(5000);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        delete client;
        delete httpclient;

        if(e.GetCode()==-32601)
            signalLogInAccepted(1);
        else {
            if (localConnection)
            {
                localConnection = false;
                serverAdress="http://185.25.149.28";
                logIn(login, password, port);
            }
            else
            {
                localConnection = true;
                signalLogInAccepted(2);
            }
        }
        return;
    }
    emit signalLogInRejected(1);
    return;
}

void Client::logIn()
{
    try
    {
        this->sessionId = client->logIn(password_.toStdString(),login_.toStdString());
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
    }
    return;
}

void Client::logOut()
{
    try
    {
        client->logOut(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
    }

    emit stopReceiver();

    delete httpclient;
    delete client;
    delete streamInitializer;
    delete timerMoveHead;
    delete timerRobotInfo;
    delete timerHeadPosition;
    delete timerBatteryState;
    delete starter;
    delete videoReceiver;

    localConnection = true;
}

void Client::startApp()
{

    videoReceiver = new VideoReceiver("udp://localhost:1234", 640, 480);
    videoReceiver->moveToThread(&videoReceiverThread);
    videoReceiverThread.start();

    starter=new QTimer();
    timerRobotInfo=new QTimer();
    timerMoveHead=new QTimer();
    timerCmdVel = new QTimer();
    timerHeadPosition=new QTimer();
    timerMoveHeadCameraKeyboard=new QTimer();
    timerBatteryState=new QTimer();
    timerLiftState = new QTimer();
    streamInitializer = new StreamInitializer("185.25.149.28", 1235);

    QObject::connect(videoReceiver, SIGNAL(frameReady(QImage)), this, SLOT(updateImage(QImage)), Qt::QueuedConnection);
    QObject::connect(videoReceiver, SIGNAL(receiverException(QString)), this, SLOT(printException(QString)), Qt::QueuedConnection);
    QObject::connect(videoReceiver, SIGNAL(receiverInitialized()), this, SLOT(receiverInitialized()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(startReceiver(int)), videoReceiver, SLOT(startReceiver(int)), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(stopReceiver()), videoReceiver, SLOT(stopReceiver()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(setResolution(int, int)), videoReceiver, SLOT(setResolution(int, int)), Qt::QueuedConnection);
    QObject::connect(starter, SIGNAL(timeout()), videoReceiver, SLOT(init()), Qt::QueuedConnection);
    QObject::connect(timerCmdVel,SIGNAL(timeout()),this,SLOT(goInDirectionWithSpeed()),Qt::QueuedConnection);

    QObject::connect(timerMoveHead, SIGNAL(timeout()), this, SLOT(timeOutMoveHead()), Qt::QueuedConnection);
    QObject::connect(timerRobotInfo, SIGNAL(timeout()), this, SLOT(getExtendedRobotInfo()), Qt::QueuedConnection);
//    QObject::connect(timerRobotInfo, SIGNAL(timeout()), this, SLOT(getStandardRobotInfo()), Qt::QueuedConnection);
    QObject::connect(timerHeadPosition, SIGNAL(timeout()), this, SLOT(refreshHeadPosition()), Qt::QueuedConnection);
    QObject::connect(timerMoveHeadCameraKeyboard, SIGNAL(timeout()), this, SLOT(moveHeadKeyboard()), Qt::QueuedConnection);
    QObject::connect(timerBatteryState, SIGNAL(timeout()), this, SLOT(refreshBatteryState()), Qt::QueuedConnection);
    QObject::connect(timerLiftState, SIGNAL(timeout()), this, SLOT(refreshLiftStatus()), Qt::QueuedConnection);


    starter->start(5000);
    timerBatteryState->start(2000);
    timerLiftState->start(1000);
    getTheNominalBatteryValue();

    refreshBatteryState();

    if (localConnection)
        initLocalStream();
    else {
        streamInitializer->start();
        QObject::connect(streamInitializer, SIGNAL(mappedAddressReceived(QString,int)), this, SLOT(initInternetStream(QString,int)), Qt::QueuedConnection);
    }

    getListOfMaps();
}
void Client::setRideMode(int mode)
{
    if(mode == 2){

        cyclicStateOfKeyPosePath = true;
    }
    else{

        cyclicStateOfKeyPosePath = false;
    }
    try
    {
        client->setRideMode(mode,sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::getRideStatus()
{
    try
    {
        client->getRideStatus(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::getListOfMaps()
{
    try
    {
        Json::Value maps = client->getAvailableMaps(sessionId);
        auto diferent = false;
        if(maps.size() != qmlListOfMaps.size()){
            diferent = true;
        }
        QList<QString> tempQmlListOfMaps;
        if(!diferent){

            for(unsigned i=0; i<maps.size(); i++){
                tempQmlListOfMaps.append(QString(maps[i].asString().c_str()));
                if(tempQmlListOfMaps[i] != qmlListOfMaps[i]){
                    diferent = true;
                }
            }
            if(diferent){
                mutex.lock();
                qmlListOfMaps.clear();
                qmlListOfMaps.append(tempQmlListOfMaps);
                mutex.unlock();
                emit signalUpdateListOfMaps();
            }
        }else{
            mutex.lock();
            qmlListOfMaps.clear();
            for(unsigned i=0; i<maps.size(); i++){
                qmlListOfMaps.append(QString(maps[i].asString().c_str()));
            }
            mutex.unlock();
            emit signalUpdateListOfMaps();
        }
    }
    catch (jsonrpc::JsonRpcException e)
    {
        mutex.unlock();
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::loadMap(int i, bool on)
{
    try
    {
        visualTypeOfMap=on;
        client->changeMap(qmlListOfMaps.at(i).toStdString(),sessionId);
        QTimer::singleShot(200, this, SLOT(showMap()));
        mapIsBuilding=false;
        QObject::disconnect(timerRobotInfo,SIGNAL(timeout()),this,SLOT(getTempMap()));
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }

}

void Client::setRobotPosition(double X, double Y, double fi)
{
    try
    {
        client->setRobotPosition(-fi, sessionId, X*myMapResolution, (myMapHeight-Y)*myMapResolution);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::joystickEnable()
{
    if(enableSteering){
        this->robotLinearSpeed=0;
        this->robotAngularSpeed=0;
        timerCmdVel->start(50);
    }
}

void Client::joystickDisable()
{
    emit sendVelocity(QVariant(QString::number(0)+"%"),QVariant(QString::number(0)+"%"),QVariant(0), QVariant(0));
    try
    {
        client->goInDirectionWithSpeed(0,sessionId,0);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
    timerCmdVel->stop();
}

void Client::joystickValueChanged(double linear, double angular)
{
    this->robotLinearSpeed=maxLinearSpeed*linear;
    this->robotAngularSpeed=maxAngularSpeed*angular;
    emit sendVelocity(QVariant(QString::number(fabs(linear)*100)+"%"),QVariant(QString::number(fabs(angular)*100)+"%"),QVariant(linear),QVariant(angular));
}

void Client::goInDirectionWithSpeed()
{
    try
    {
        client->goInDirectionWithSpeed(robotAngularSpeed,sessionId,robotLinearSpeed);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::changeRobot(QString login, QString password, int index, QString ip, int port)
{
    if (index == -1){

        serverAdress = "http://" + ip;
    }else if(index==1){

        serverAdress="http://192.168.10.10";
    }else if(index==0){

        serverAdress="http://192.168.10.20";
    }else if(index==2){

        serverAdress="http://192.168.10.15";
    }else if(index==3){

        serverAdress="http://192.168.10.16";
    }else if(index==4){

        serverAdress="http://192.168.10.25";
    }else if(index==5){

        serverAdress="http://192.168.10.30";
    }
    else if(index==6){

        serverAdress="http://127.0.0.1";
    }


    Client::logIn(login, password, port);
}

void Client::changeStartingPoint(int which)
{
    try
    {
        client->changeStartPoint(sessionId, which);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::addKeyPose(QString name_, QString description_, double firstMeterX_, double firstMeterY_, double fiRadian_, int typeOfKeyPose_, int typeOfTimeSet_, int time_)
{
    Json::Value position;
    position.clear();
    position.append(firstMeterX_ * myMapResolution);
    position.append((myMapHeight - firstMeterY_) * myMapResolution);
    position.append(-fiRadian_);

    Json::Value ints, floats, strings, boolParams;
    ints.clear();
    floats = Json::arrayValue;
    strings = Json::arrayValue;
    auto tempLength = static_cast<int>(KeyPoseElement::INTSDEADLINESTRUCTURE::NUMBER_OF_ITEMS);
    for(int i = 0; i < tempLength; ++i){

        if(i == static_cast<int>(KeyPoseElement::INTSDEADLINESTRUCTURE::DEADLINE_TIME_SEC)){
            ints.append(time_);
        }
        else{
            ints.append(Json::nullValue);
        }
    }
    tempLength = static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::NUMBER_OF_ITEMS);
    boolParams.resize(tempLength);
    if(typeOfKeyPose_ == static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::BUTTON_WITH_DEADLINE) ||
       typeOfKeyPose_ == static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::SIMPLE)){
        if(KeyPoseElement::keyPoseBoolParamMap.count(typeOfTimeSet_) > 0){
            for(int i = 0; i < tempLength; ++i){
                if(i == KeyPoseElement::keyPoseBoolParamMap.find(typeOfTimeSet_)->second){
                    boolParams[i] = true;
                }
                else{
                    boolParams[i] = false;
                }
            }
        }
    }
    else if(typeOfKeyPose_ == static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::GIVE_WAY)){
        boolParams[static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::SET_DEFAULT_TIME)] = false;
        boolParams[static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::SET_INFINITE_TIME)] = true;
    }
    else{
        for(int i = 0; i < tempLength; ++i){
            boolParams[i] = false;
        }
    }
    try
    {
        client->addKeypose(boolParams, description_.toStdString(),floats, ints, name_.toStdString(), position, sessionId, strings, KeyPoseElement::keyPoseTypeToRosMap.at(typeOfKeyPose_));
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

KeyPoseElement* Client::getKeyPoseToDelete(double firstMeterX, double firstMeterY)
{
    try
    {
        mutex.lock();
        for(int i = 0;i<qmlKeyPoseList.size();i++){
            if(firstMeterX<(qmlKeyPoseList[i]->x+7)&&firstMeterX>(qmlKeyPoseList[i]->x-7))
                if(firstMeterY<(qmlKeyPoseList[i]->y+7)&&firstMeterY>(qmlKeyPoseList[i]->y-7)){
                    client->deleteKeypose(qmlKeyPoseList[i]->name.toStdString(),sessionId);
                    qmlSettingsOfVisibleKeyPoses.removeAt(i);
                    emit updateImageMapOnly();
                    mutex.unlock();
                    return qmlKeyPoseList.at(i);
                }
        }
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        mutex.unlock();
        if(e.GetCode() == -32003){

            logIn();
        }
    }
    mutex.unlock();
    return nullptr;
}

void Client::deleteKeyPose(double firstMeterX, double firstMeterY)
{
    try
    {
        mutex.lock();
        for(int i = 0;i<qmlKeyPoseList.size();i++){
            if(firstMeterX<(qmlKeyPoseList[i]->x+20)&&firstMeterX>(qmlKeyPoseList[i]->x-20))
                if(firstMeterY<(qmlKeyPoseList[i]->y+20)&&firstMeterY>(qmlKeyPoseList[i]->y-20)){
                    client->deleteKeypose(qmlKeyPoseList[i]->name.toStdString(),sessionId);
                    qmlSettingsOfVisibleKeyPoses.removeAt(i);
                    emit updateImageMapOnly();
                    mutex.unlock();
                }
        }
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        mutex.unlock();
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::deleteKeyPoseName(QString name)
{
    try
    {
        client->deleteKeypose(name.toStdString(),sessionId);
        emit updateImageMapOnly();
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::resetDiagnosticsAction(bool on)
{
    try
    {
        client->resetDiagnosticsAction(sessionId);
        std::cout << "test";
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::addKeyPoseAtRobotPosition(QString name_, QString description_, int typeOfKeyPose_, int typeOfTimeSet_, int time_)
{
    if(qmlRobotPosition.size()==3){
        Json::Value position;
        position.clear();
        position.append(qmlRobotPosition[0]*myMapResolution);
        position.append((myMapHeight-qmlRobotPosition[1])*myMapResolution);
        position.append(-qmlRobotPosition[2]);
        Json::Value ints, floats, strings, boolParams;
        ints.clear();
        floats = Json::arrayValue;
        strings = Json::arrayValue;
        auto tempLength = static_cast<int>(KeyPoseElement::INTSDEADLINESTRUCTURE::NUMBER_OF_ITEMS);
        for(int i = 0; i < tempLength; ++i){

            if(i == static_cast<int>(KeyPoseElement::INTSDEADLINESTRUCTURE::DEADLINE_TIME_SEC)){
                ints.append(time_);
            }
            else{
                ints.append(Json::nullValue);
            }
        }
        tempLength = static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::NUMBER_OF_ITEMS);
        boolParams.resize(tempLength);
        if(KeyPoseElement::keyPoseBoolParamMap.count(typeOfTimeSet_) > 0){
            for(int i = 0; i < tempLength; ++i){
                if(i == KeyPoseElement::keyPoseBoolParamMap.find(typeOfTimeSet_)->second){
                    boolParams[i] = true;
                }
                else{
                    boolParams[i] = false;
                }
            }
        }
        else{
            for(int i = 0; i < tempLength; ++i){
                boolParams[i] = false;
            }
        }

        try
        {
            client->addKeypose(boolParams, description_.toStdString(),floats, ints, name_.toStdString(), position, sessionId, strings, KeyPoseElement::keyPoseTypeToRosMap.at(typeOfKeyPose_));
        }
        catch (jsonrpc::JsonRpcException e)
        {
            std::cerr << e.what() << std::endl;
            if(e.GetCode() == -32003){

                logIn();
            }
        }
    }
}

void Client::sendToKeyPose(QString name)
{
    try{
        client->goToKeypose(name.toStdString(),sessionId);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::bulidNewMap()
{
    try{
        client->bulidNewMap(sessionId);
        QObject::connect(timerRobotInfo,SIGNAL(timeout()),this,SLOT(getTempMap()));
        mapIsBuilding=true;
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::saveMap(QString name)
{
    try{
        client->saveMap(name.toStdString(),sessionId);
        QObject::disconnect(timerRobotInfo,SIGNAL(timeout()),this,SLOT(getTempMap()));
        mapIsBuilding=false;
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::lockerHandler(int on)
{
    try{
        client->lockerAction(Locker::qmlToJsonLockerActionMap.at(on), sessionId);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::brakeModeHandler(bool on)
{
    try{
        client->changeBrakeMode(on,sessionId);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::addNoDrivingPoint(double x, double y)
{
    int indexOfNearPoint = getIndexOfNoDrivingPoint(x,y);
    if(indexOfNearPoint != -1){

        qmlNewNoDrivingArea.push_back(qmlNewNoDrivingArea.at(indexOfNearPoint));
    }
    else{

        Point* tempPoint = new Point;
        tempPoint->setX(x);
        tempPoint->setY(y);
        qmlNewNoDrivingArea.push_back(tempPoint);
    }
}

int Client::getIndexOfNoDrivingPoint(double x, double y)
{
    for(auto tempElement : qmlNewNoDrivingArea){

        if(sqrt(pow(tempElement->x()- x,2) + pow(tempElement->y() - y, 2)) < 6){

            return qmlNewNoDrivingArea.indexOf(tempElement);
        }
    }

    return -1; //not found
}

Point* Client::deleteLastNoDrivingPoint()
{
    if(!qmlNewNoDrivingArea.isEmpty()){

        qmlNewNoDrivingArea.removeLast();
        Point* retPoint = new Point;
        retPoint->setX(qmlNewNoDrivingArea.last()->x());
        retPoint->setY(qmlNewNoDrivingArea.last()->y());
        return retPoint;
    }

    return nullptr;

}

void Client::mapWindowVisableChanged(bool visible)
{
    if(visible){
        timerRobotInfo->start(500);
        timerHeadPosition->stop();
    }
    else{
        timerRobotInfo->stop();
        if(cameraWindowVisable)
            timerHeadPosition->start(500);
    }
}

void Client::cameraWindowVisableChanged(bool visible)
{
    cameraWindowVisable=visible;
    if(visible&&!timerRobotInfo->isActive()){
        timerHeadPosition->start(500);
    }
    else{
        timerHeadPosition->stop();
    }
}

void Client::setEnableSteering(bool enable)
{
    enableSteering=enable;
}

void Client::addMidpoint(double firstMeterX, double firstMeterY, double fiRadian, int index)
{

    if(index == -1){
        try
        {
            index = client->getNumberOfMidpoints(sessionId);
        }
        catch (jsonrpc::JsonRpcException e)
        {
            std::cerr << e.what() << std::endl;
            if(e.GetCode() == -32003){

                logIn();
            }
        }
    }

    try
    {
        client->addMidpoint(-fiRadian, index, sessionId, firstMeterX*myMapResolution, (myMapHeight-firstMeterY)*myMapResolution);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::startRobot()
{
    try
    {
        client->startRobot(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }

}

void Client::stopRobot()
{
    try
    {
        startKeyPosePath("", true);
        client->stopRobot(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::removeMidpointIndex(int index)
{
    try
    {
        client->removeMidpoint(index,sessionId);
        emit updateImageMapOnly();

    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::removeMidpoint(double firstMeterX, double firstMeterY)
{
    try
    {
        for(int i = 0 ; i<midpointList.size();i++){
            if(firstMeterX<(midpointList[i][0]+20)&&firstMeterX>(midpointList[i][0]-20))
                if(firstMeterY<(midpointList[i][1]+20)&&firstMeterY>(midpointList[i][1]-20)){
                    client->removeMidpoint(i,sessionId);
                    emit updateImageMapOnly();
                    break;
                }
        }
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::removeAllMidpoint()
{

    try
    {
        client->removeAllMidpoints(sessionId);
        emit updateImageMapOnly();
    }
    catch (jsonrpc::JsonRpcException e)
    {
        mutex.unlock();
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::statusSlot()
{
    try
    {
        client->status(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::showMap(int heightPixels, int widthPixels)
{
    if(!mapIsBuilding)
    {
        try
        {
            mutex.lock();
            Json::Value mapArray;
            mapArray=client->getMapFilePng(heightPixels,sessionId,visualTypeOfMap,widthPixels);
            myMapAdress=myMapAdress.fromStdString(mapArray[0].asString());
            myMapAdress=serverAdress+":2408"+myMapAdress;
            myMapResolution=mapArray[1].asDouble();
            myMapWidth=mapArray[2].asInt();
            myMapHeight=mapArray[3].asInt();
            mutex.unlock();
            emit updateMapNow();
        }
        catch (jsonrpc::JsonRpcException e)
        {
            mutex.unlock();
            std::cerr << e.what() << std::endl;
            if(e.GetCode() == -32003){

                logIn();
            }
        }
    }
}

void Client::showMap()
{
   showMap(1000,1000);
}
void Client::timeOutMoveHead()
{
    try
    {
        client->moveHead(horizontalMoveHead*0.002,verticalMoveHead*0.002,sessionId);
        horizontalMoveHead=0;
        verticalMoveHead=0;
        timerMoveHead->stop();
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::getTempMap()
{
    try{
        myMapAdress="";
        mutex.lock();
        Json::Value mapArray;
        mapArray=client->getTempMapFile(sessionId);
        myMapAdress=myMapAdress.fromStdString(mapArray[0].asString());
        myMapAdress=serverAdress+":2408"+myMapAdress;
        myMapResolution=mapArray[1].asDouble();
        myMapWidth=mapArray[2].asInt();
        myMapHeight=mapArray[3].asInt();
        mutex.unlock();
        emit updateMapNow();
    }
    catch (jsonrpc::JsonRpcException e){

        mutex.unlock();
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::getExtendedRobotInfo()
{
    if(myMapWidth==-1)
        return;
    try
    {
        Json::Value tempArray;
        tempArray = client->getExtendedRobotInfo(sessionId);

        //robot position
        refreshRobotPosition(tempArray[0]);

        //midpoints
        refreshMidpointList(tempArray[1]);

        //keyposes
        refreshKeyPose(tempArray[2]);

        //head position
        refreshHeadPosition(tempArray[3]);

        // path
        refreshPath(tempArray[4]);

        // laser scan
        refreshLaserScan(tempArray[5]);

        //midKeyPoseGraph
        refreshMidKeyPoseGraph(tempArray[6]);

        //warningZone
        refreshWarningZone(tempArray[7]);

        emit updateImageMapOnly();
        emit refreshKeyPoseQmlList();
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003 || e.GetCode() == -32601){

            logIn();
        }
        // mutex.unlock();
    }
}

void Client::getStandardRobotInfo()
{
    if(myMapWidth==-1)
        return;
    try
    {
        Json::Value tempArray;
        tempArray=client->getStandardRobotInfo(sessionId);

        //robot position
        refreshRobotPosition(tempArray[0]);

        //midpoints
        refreshMidpointList(tempArray[1]);

        //keyposes
        refreshKeyPose(tempArray[2]);

        //head position
        refreshHeadPosition(tempArray[3]);

        //midKeyPoseGraph
        refreshMidKeyPoseGraph(tempArray[4]);

        //warningZone
        refreshWarningZone(tempArray[5]);

        emit updateImageMapOnly();
        emit refreshKeyPoseQmlList();
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003 || e.GetCode() == -32601){

            logIn();
        }
    }
}

void Client::changeRobotInfoType(bool on)
{
    if(on){
        QObject::disconnect(timerRobotInfo, SIGNAL(timeout()), this, SLOT(getStandardRobotInfo()));
        QObject::connect(timerRobotInfo, SIGNAL(timeout()), this, SLOT(getExtendedRobotInfo()), Qt::QueuedConnection);
    }
    else{
        QObject::disconnect(timerRobotInfo, SIGNAL(timeout()), this, SLOT(getExtendedRobotInfo()));
        QObject::connect(timerRobotInfo, SIGNAL(timeout()), this, SLOT(getStandardRobotInfo()), Qt::QueuedConnection);
        qmlLaserScan.clear();
        qmlPath.clear();
    }

}

void Client::setHeadCameraSpeedKeyboard(double horizontal, double vertical)
{
    if(horizontal == 0 && vertical ==0 || !enableMoveHeadCameraByKeyboard){
        timerMoveHeadCameraKeyboard->stop();
    }
    else if(timerMoveHeadCameraKeyboard->isActive()==false){
        timerMoveHeadCameraKeyboard->start(20);
        verticalMoveHeadSpeed=vertical;
        horizontalMoveHeadSpeed=horizontal;
    }
    else{
        verticalMoveHeadSpeed=vertical;
        horizontalMoveHeadSpeed=horizontal;
    }
}

void Client::setEnableMoveHeadKeyboard(bool enable)
{
    enableMoveHeadCameraByKeyboard=enable;
}

void Client::changeMidpointsTolerance(double xy, double angle)
{
    try
    {
        client->setMidpointsTolerance(angle,sessionId,xy);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::setVisibleOfKeyPoses(int Id, bool checked)
{
    mutex.lock();
    if(Id == -1){

        for(int i = 0; i < qmlSettingsOfVisibleKeyPoses.size(); ++i){

            qmlSettingsOfVisibleKeyPoses[i] = checked;
        }
    }
    else if(qmlSettingsOfVisibleKeyPoses.size() - 1 >= Id && Id >= 0){

        qmlSettingsOfVisibleKeyPoses[Id] = checked;
    }
    mutex.unlock();
}

void Client::addMidKeyPoseGraph(double firstX, double firstY, double lastX, double lastY)
{
    mutex.lock();
    MidKeyPoseGraphElement * firstPoint = nullptr;
    MidKeyPoseGraphElement* lastPoint = nullptr;
    for(int i = 0; i < tempMidKeyPoseGraphList.size(); i++){

        if(sqrt(pow(tempMidKeyPoseGraphList[i]->getX()- firstX,2) + pow(tempMidKeyPoseGraphList[i]->getY() - firstY, 2)) < 6){

            firstPoint = tempMidKeyPoseGraphList[i];
            continue;
        }
        else if(sqrt(pow(tempMidKeyPoseGraphList[i]->getX()- lastX,2) + pow(tempMidKeyPoseGraphList[i]->getY() - lastY, 2)) < 6){

            lastPoint = tempMidKeyPoseGraphList[i];
            continue;
        }
    }
    if(firstPoint == nullptr && lastPoint == nullptr){

        firstPoint = new MidKeyPoseGraphElement();
        lastPoint = new MidKeyPoseGraphElement();
        firstPoint->setX(firstX);
        firstPoint->setY(firstY);
        firstPoint->setEdges(lastPoint);
        tempMidKeyPoseGraphList.push_back(firstPoint);

        lastPoint->setX(lastX);
        lastPoint->setY(lastY);
        tempMidKeyPoseGraphList.push_back(lastPoint);

    }
    else if(firstPoint == nullptr){

        firstPoint = new MidKeyPoseGraphElement();
        firstPoint->setX(firstX);
        firstPoint->setY(firstY);
        firstPoint->setEdges(lastPoint);
        tempMidKeyPoseGraphList.push_back(firstPoint);
    }
    else if(lastPoint == nullptr){

        lastPoint = new MidKeyPoseGraphElement();
        lastPoint->setX(lastX);
        lastPoint->setY(lastY);
        firstPoint->setEdges(lastPoint);
        tempMidKeyPoseGraphList.push_back(lastPoint);
    }
    else{

        if(firstPoint->getEdges().indexOf(lastPoint) == -1){

            firstPoint->setEdges(lastPoint);
        }
    }
    mutex.unlock();
}

void Client::saveMidKeyPoseGraph(bool on, bool accepted)
{
    try{
        if(!on){
            if(!accepted){
                drawingStateOfMidKeyPoseGraph = false;
                tempMidKeyPoseGraphList.clear();
                return;
            }
            mutex.lock();
            QList<bool> connectedPoint;
            for(unsigned i = 0; i < tempMidKeyPoseGraphList.size(); i++ ){

                connectedPoint.append(false);
            }
            for(auto tempElement : tempMidKeyPoseGraphList){

                if(tempElement->getEdges().size() > 0){

                    connectedPoint[tempMidKeyPoseGraphList.indexOf(tempElement)] = true;
                    for(auto tempEdge : tempElement->getEdges()){

                        connectedPoint[tempMidKeyPoseGraphList.indexOf(tempEdge)] = true;
                    }
                }
            }

            for(unsigned i = connectedPoint.size() ; i > 0 ; i--){

                if(!connectedPoint[i-1]){

                    tempMidKeyPoseGraphList.removeAt(i-1);
                }
            }

            mutex.unlock();
            Json::Value graphElement;
            for(int i = 0 ; i < tempMidKeyPoseGraphList.size(); i++){
                Json::Value tempElement;
                tempElement.append(tempMidKeyPoseGraphList[i]->getX()*myMapResolution);
                tempElement.append((myMapHeight - tempMidKeyPoseGraphList[i]->getY())*myMapResolution);

                for(int j = 0; j < tempMidKeyPoseGraphList[i]->getEdges().size(); j++){

                    tempElement.append(tempMidKeyPoseGraphList.indexOf(tempMidKeyPoseGraphList[i]->getEdges()[j]));
                }
                graphElement.append(tempElement);
            }
            if (graphElement.isNull()){

                graphElement.append(Json::nullValue);
            }
            client->saveMidKeyPoseGraph(graphElement, sessionId);
            drawingStateOfMidKeyPoseGraph = false;
            tempMidKeyPoseGraphList.clear();
        }
        else{

            if(qmlMidKeyPoseGraphList.size()){

                tempMidKeyPoseGraphList.clear();
                tempMidKeyPoseGraphList = qmlMidKeyPoseGraphList;
            }
            drawingStateOfMidKeyPoseGraph = true;
        }
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::deleteMidKeyPoseGraphPoint(double firstX, double firstY, double lastX, double lastY)
{
    mutex.lock();
    MidKeyPoseGraphElement* firstPoint,* lastPoint;
    firstPoint = getMidKeyPose(firstX, firstY);
    lastPoint = getMidKeyPose(lastX, lastY);
    if(firstPoint != nullptr && lastPoint != nullptr){

        firstPoint->deleteEdge(lastPoint);
    }
    mutex.unlock();
}
MidKeyPoseGraphElement* Client::getMidKeyPose(double x, double y)
{
    for(auto tempElement : tempMidKeyPoseGraphList){

        if(sqrt(pow(tempElement->getX()- x,2) + pow(tempElement->getY() - y, 2)) < 6){

            return tempElement;
        }
    }

    return nullptr; //not found
}

int Client::getIndexOfMidKeyPose(double x, double y)
{
    for(auto tempElement : tempMidKeyPoseGraphList){

        if(sqrt(pow(tempElement->getX()- x,2) + pow(tempElement->getY() - y, 2)) < 6){

            return tempMidKeyPoseGraphList.indexOf(tempElement);
        }
    }

    return -1; //not found
}

void Client::editMidKeyPoint(double x, double y, double oldX, double oldY)
{
    mutex.lock();
    MidKeyPoseGraphElement* editingPoint = getMidKeyPose(oldX, oldY);
    MidKeyPoseGraphElement* nearDropPoint = getMidKeyPose(x, y);

    if(editingPoint == nearDropPoint){

        mutex.unlock();
        return;
    }
    else if(nearDropPoint == nullptr){

        editingPoint->setX(x);
        editingPoint->setY(y);
    }
    else{

        for(auto tempEdge : editingPoint->getEdges()){

            if(tempEdge != nearDropPoint){

                nearDropPoint->setEdges(tempEdge);
            }
        }

        for(auto tempElement : tempMidKeyPoseGraphList){

            for(auto tempEdge : tempElement->getEdges()){

                if(tempEdge == editingPoint){

                    tempElement->deleteEdge(editingPoint);
                    tempElement->setEdges(nearDropPoint);
                }
            }
        }

        tempMidKeyPoseGraphList.removeAll(editingPoint);
    }
    mutex.unlock();
}
void Client::dockDeparture(int side)
{
    try{
        client->dockDeparture(sessionId, side);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}

void Client::dockApproach(int side)
{
    try{
        client->dockApproach(sessionId, side);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}

void Client::sideButtonClicked(bool state)
{
    try{
        client->sideButtonClicked(sessionId, state);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}
void Client::stopCharingButtonClicked()
{
    try{
        client->stopChargingButtonClicked(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}

void Client::skipButtonClicked()
{
    try{
        client->skipButtonClicked(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}

void Client::addWarningZone(double x, double y, double range, QString name, int type)
{
    try{
        Json::Value circle;
        circle.append(x * myMapResolution);
        circle.append((myMapHeight - y) * myMapResolution);
        circle.append(range * myMapResolution);
        Json::Value shape;
        shape.append(static_cast<int>(WarningZoneElement::JSONWARNINGZONESHAPETYPE::CIRCLE));
        shape.append(Json::Value());
        shape.append(circle);
        shape.append(Json::Value());
        client->saveWarningZone(name.toStdString(), sessionId, shape, WarningZoneElement::qmlToJsonWarningZoneTypeMap.at(type));
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}

void Client::addWarningZone(QString name, int type)
{
    try{
        Json::Value polygon;
        for (auto element : qmlNewWarningZone.getShape()){

            polygon.append(element->x() * myMapResolution);
            polygon.append((myMapHeight - element->y()) * myMapResolution);
        }
        Json::Value shape;
        shape.append(static_cast<int>(WarningZoneElement::JSONWARNINGZONESHAPETYPE::POLYGON));
        shape.append(Json::Value());
        shape.append(polygon);
        shape.append(Json::Value());
        client->saveWarningZone(name.toStdString(), sessionId, shape, WarningZoneElement::qmlToJsonWarningZoneTypeMap.at(type));
        qmlNewWarningZone.clearShape();
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}

void Client::deleteWarningZone(QString name)
{
    try{

        client->deleteWarningZone(name.toStdString(),sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }}

void Client::liftHandler(bool lift)
{
    try{

        if(lift){

            client->liftAction(static_cast<int16_t>(LIFTING::UP), sessionId);
            lastLiftingAction = static_cast<int16_t>(LIFTING::UP);

        }
        else{

            client->liftAction(static_cast<int16_t>(LIFTING::DOWN), sessionId);
            lastLiftingAction = static_cast<int16_t>(LIFTING::DOWN);
        }

        timerLiftState->setInterval(500);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::keyPoseGridViewChooseHandler(QString name)
{
    for(auto tempElement : keyPoseGridViewList){

        if(qobject_cast<KeyPosePathViewElement *>(tempElement)->name() == name){

            keyPoseListViewList.append(tempElement);
            emit signalRefreshContextProperties(QVariant::fromValue(keyPoseListViewList), "keyPoseListViewModel");
            break;
        }
    }
}

void Client::refreshMenuContent()
{
    refreshKeyPoseGridViewModel();
    refreshKeyPosePathList();
}

void Client::moveItemInListView(int oldIndex, int newIndex)
{
    keyPoseListViewList.move(oldIndex, newIndex);
    emit signalRefreshContextProperties(QVariant::fromValue(keyPoseListViewList), "keyPoseListViewModel");
    emit signalSetCurrentIndexOfListView(QVariant::fromValue(newIndex));
}

void Client::deleteItemFromListView(int index)
{
    keyPoseListViewList.removeAt(index);
    emit signalRefreshContextProperties(QVariant::fromValue(keyPoseListViewList), "keyPoseListViewModel");
    if(keyPoseListViewList.size() > index){

        emit signalSetCurrentIndexOfListView(QVariant::fromValue(index));
    }
    else if (keyPoseListViewList.size() != 0){

        emit signalSetCurrentIndexOfListView(QVariant::fromValue(keyPoseListViewList.size()-1));
    }
}

void Client::saveKeyPosePath(QString name, QString description)
{
    Json::Value keyPosePathValue;
    for(auto tempElement : keyPoseListViewList){

        keyPosePathValue.append(qobject_cast<KeyPosePathViewElement *>(tempElement)->name().toStdString());
    }
    try{
        client->saveKeyPosePath(description.toStdString(), keyPosePathValue, name.toStdString(), sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::loadKeyPosePath(QString name)
{
    try{
        keyPoseListViewList.clear();

        for(auto tempElement : listOfKeyPosePath){

            if(tempElement->name() == name){

                for(auto tempString : tempElement->path()){

                    if(tempString.contains("dock")){

                        keyPoseListViewList.append(new KeyPosePathViewElement(tempString, dockImage));
                    }
                    else{

                        keyPoseListViewList.append(new KeyPosePathViewElement(tempString, keyPoseImage));
                    }
                }
                break;
            }
        }
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
    emit signalRefreshContextProperties(QVariant::fromValue(keyPoseListViewList),"keyPoseListViewModel");

}

void Client::startKeyPosePath(QString name, bool stopRobot)
{
    try{

        Json::Value retValue;

        if(!stopRobot){

            for(auto tempElement : listOfKeyPosePath){

                if(tempElement->name() == name){

                    for(auto tempString : tempElement->path()){

                        retValue.append(tempString.toStdString());
                    }
                    break;
                }
            }
        }

        if(retValue.isNull()){

            retValue.append("");
        }
        client->startKeyPosePath(cyclicStateOfKeyPosePath, retValue, sessionId);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::startKeyPosePath(bool cyclic)
{
    try{
        Json::Value retValue;

        for(auto tempElement : keyPoseListViewList){

            QString temp_string = qobject_cast<KeyPosePathViewElement *>(tempElement)->name();
            retValue.append(temp_string.toStdString());
        }

        if(retValue.isNull()){

            retValue.append("");
        }
        client->startKeyPosePath(cyclic, retValue, sessionId);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::moveHeadKeyboard()
{
    moveHead(horizontalMoveHeadSpeed, verticalMoveHeadSpeed);
}

void Client::initLocalStream()
{
    try{
        foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
            if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost) && address.toString().mid(0, 3) == "192")
                client->startStream(0, address.toString().toStdString(), true, 1234, sessionId);
        }
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::initInternetStream(QString ip, int port)
{
    try{
       client->startStream(0, ip.toStdString(), false, port, sessionId);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::moveHead(double horizontal, double vertical)
{
    if(timerMoveHead->isActive()==false)
    {
        timerMoveHead->start(100);
     }

        horizontalMoveHead+=horizontal;
        verticalMoveHead+=vertical;
}

void Client::baseHead()
{
    try
    {
        client->baseHead(sessionId);
        qmlHeadPosition.clear();
        qmlHeadPosition.append(0);
        qmlHeadPosition.append(0);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::moveAbsHead(double horizontal, double vertical)
{
    try
    {
        client->moveHeadAbs(horizontal,sessionId,vertical);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::updateImage(QImage image)
{
    emit newHeadImageReceived(image);
    emit updateHeadCameraNow();
}

void Client::printException(QString message)
{
    std::cout<<message.toStdString()<<std::endl;
}

void Client::receiverInitialized()
{
    emit startReceiver(30);
    starter->stop();
}

void Client::resizeDone()
{

}

void Client::refreshMidpointList()
{
    try
    {
        Json::Value newMidpointsList;
        newMidpointsList = client->getAllMidpoints(sessionId);
        refreshMidpointList(newMidpointsList);
    }
    catch(jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::refreshMidpointList(Json::Value newData){

    mutex.lock();
    qmlMidpointList.clear();
    midpointList.clear();
    for(unsigned i = 0; i < newData.size() / 3; i++)
    {
        qmlMidpointList.append((newData[3*i].asDouble()) / myMapResolution);
        qmlMidpointList.append(myMapHeight - (newData[3*i+1].asDouble()) / myMapResolution);
        qmlMidpointList.append(-newData[3*i+2].asDouble());
        QList<double> midpoint;
        midpoint.append(qmlMidpointList[3*i]);
        midpoint.append(qmlMidpointList[3*i+1]);
        midpoint.append(qmlMidpointList[3*i+2]);
        midpointList.append(midpoint);
    }

    mutex.unlock();

    if(myNumberOfMidpoints != newData.size() / 3){
        emit midpointsChanged();
        myNumberOfMidpoints = newData.size() / 3;
    }
}
void Client::refreshRobotPosition()
{
    try
    {
        Json::Value tempArray;
        tempArray = client->getRobotPosition(sessionId);
        refreshRobotPosition(tempArray);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}
void Client::refreshRobotPosition(Json::Value newData)
{
    mutex.lock();
    qmlRobotPosition.clear();
    qmlRobotPosition.append((newData[0].asDouble()) / myMapResolution);
    qmlRobotPosition.append(myMapHeight - (newData[1].asDouble())/myMapResolution);
    qmlRobotPosition.append( - newData[2].asDouble());
    mutex.unlock();
}

void Client::refreshKeyPose()
{
    try
    {
        Json::Value tempArray;
        tempArray = client->getKeyposes(sessionId);
        refreshKeyPose(tempArray);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::refreshKeyPose(Json::Value newData)
{
    Json::Value temp;
    mutex.lock();
    qmlKeyPoseList.clear();

    for(unsigned i = 0; i<newData.size(); i++){
        temp = newData[i];
        KeyPoseElement* tempElement = new KeyPoseElement();
        tempElement->name = QString::fromStdString(temp[KeyPoseElement::NAME].asString());
        tempElement->type = temp[KeyPoseElement::KEYPOSESTRUCTURE::TYPE].asInt();
        tempElement->description = QString::fromStdString(temp[KeyPoseElement::DESCRIPTION].asString());
        tempElement->x = temp[KeyPoseElement::POSEX].asDouble() / myMapResolution;
        tempElement->y = myMapHeight - temp[KeyPoseElement::POSEY].asDouble() / myMapResolution;
        tempElement->fi = - temp[KeyPoseElement::POSEFI].asDouble();
        QList<int> intsArray;
        for(auto tempValue : temp[KeyPoseElement::INTS]){
            intsArray.append(tempValue.asInt());
        }
        tempElement->intsArray = intsArray;
        QList<float> floatsArray;
        for(auto tempValue : temp[KeyPoseElement::FLOATS]){
            floatsArray.append(tempValue.asFloat());
        }
        tempElement->floatsArray = floatsArray;
        QList<QString> stringsArray;
        for(auto tempValue : temp[KeyPoseElement::STRINGS]){
            stringsArray.append(QString::fromStdString(tempValue.asString()));
        }
        tempElement->stringsArray = stringsArray;
        QList<bool> boolParamsArray;
        for(auto tempValue : temp[KeyPoseElement::BOOLS]){
            boolParamsArray.append(tempValue.asBool());
        }
        tempElement->boolParamsArray = boolParamsArray;

        qmlKeyPoseList.append(tempElement);
    }
    while(newData.size() < qmlSettingsOfVisibleKeyPoses.size()){
        qmlSettingsOfVisibleKeyPoses.pop_back();
    }
    while(newData.size() > qmlSettingsOfVisibleKeyPoses.size()){
        qmlSettingsOfVisibleKeyPoses.append(true);
    }
    mutex.unlock();
}

void Client::refreshHeadPosition()
{
    try{
        Json::Value tempArray;
        tempArray = client->getHeadPositionRadian(sessionId);
        refreshHeadPosition(tempArray);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() <<std::endl;
    }
}

void Client::refreshHeadPosition(Json::Value newData)
{
    mutex.lock();
    qmlHeadPosition.clear();
    if(newData[0].asDouble() > 2.0){

        qmlHeadPosition.append(2);
    }
    else{
        if(newData[0].asDouble()<-2.0){

            qmlHeadPosition.append(-2);
        }
        else{

            qmlHeadPosition.append((newData[0].asDouble()));
        }
    }
    if(newData[1].asDouble()>1.5){

        qmlHeadPosition.append(1.5);
    }
    else{

        if(newData[1].asDouble()<-1.3){

            qmlHeadPosition.append(-1.3);
        }
        else{

           qmlHeadPosition.append((newData[1].asDouble()));
        }
    }
    mutex.unlock();
    emit headPositionChanged();
}

void Client::refreshPath()
{
    try
    {
        Json::Value newPath;
        newPath = client->getPath(sessionId);
        refreshPath(newPath);
    }
    catch(jsonrpc::JsonRpcException e)
    {
       std::cerr << e.what() <<std::endl;
    }
}

void Client::refreshPath(Json::Value newData)
{
    mutex.lock();

    int sizeOfPath = newData.size()/2;
    qmlPath.clear();
    for(int i = 0; i<sizeOfPath; i++)
    {
        qmlPath.append((newData[2*i].asDouble()) / myMapResolution);
        qmlPath.append(myMapHeight-(newData[2*i+1].asDouble()) / myMapResolution);
    }

    mutex.unlock();
}

void Client::refreshLaserScan()
{
    try{

        Json::Value newLaserScan;
        newLaserScan = client->getLaserScan(sessionId);
        refreshLaserScan(newLaserScan);
    }
    catch(jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::refreshLaserScan(Json::Value newData)
{

    mutex.lock();
    qmlLaserScan.clear();
    qmlAngleOfLaserScan = fabs(newData[0].asDouble())+fabs(newData[1].asDouble());
    for(unsigned i = 2; i < newData.size(); i++){
        qmlLaserScan.append(newData[i].asDouble() / myMapResolution);
    }

    mutex.unlock();
}

void Client::getTheNominalBatteryValue()
{
    try{
        mutex.lock();
        nominalBatteryValue = client->getBatteryState(sessionId);
        int nominalBatteryValueCounter = 12000;
        while(nominalBatteryValue > 1.5*nominalBatteryValueCounter || nominalBatteryValue < 0.5*nominalBatteryValueCounter)
        {
            nominalBatteryValueCounter *= 2;
        }
        nominalBatteryValue = nominalBatteryValueCounter;
    }
    catch(jsonrpc::JsonRpcException e){
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
    mutex.unlock();
}

void Client::suspendTaskQueue()
{
    try
    {
        client->suspendTaskQueue(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){
            logIn();
        }
    }
}

void Client::unsuspendTaskQueue()
{
    try
    {
        client->unsuspendTaskQueue(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){
            logIn();
        }
    }
}

void Client::abortTask()
{
    try
    {
        client->abortTask(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){
            logIn();
        }
    }
}

void Client::clearTaskQueue()
{
    try
    {
        client->clearTaskQueue(sessionId);
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){
            logIn();
        }
    }
}

void Client::refreshBatteryState()
{
    try{
        mutex.lock();
        Json::Value newBatteryState;
        int low = 0.94*nominalBatteryValue, high = 1.12*nominalBatteryValue;
        newBatteryState = client->getBatteryState(sessionId);
        qmlBatteryPower = newBatteryState.asInt() - low;
        if((high - low) != 0) qmlBatteryPower = (100 * qmlBatteryPower / (high - low));

        if(qmlBatteryPower > 100){

            qmlBatteryPower = 100;
        }
        else if(qmlBatteryPower < 0){

            qmlBatteryPower = 0;
        }

        Json::Value newBatteryLow, newBatteryCritical;
        newBatteryLow = client->getBatteryLow(sessionId);
        newBatteryCritical = client->getBatteryCritical(sessionId);
        emit signalRefreshBatteryState(QVariant(newBatteryLow.asBool()), QVariant(newBatteryCritical.asBool()));
    }
    catch(jsonrpc::JsonRpcException e){
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
    mutex.unlock();

}

void Client::refreshWarningZone(Json::Value newData)
{
    mutex.lock();
    qmlWarningZone.clear();
    for(const auto& nextElement : newData){

        WarningZoneElement* tempNewWarningZone = new WarningZoneElement();
        tempNewWarningZone->setName(QString(nextElement[static_cast<int>(WarningZoneElement::JSONWARNINGZONESTRUCTURE::NAME)].asString().c_str()));
        tempNewWarningZone->setType(nextElement[static_cast<int>(WarningZoneElement::JSONWARNINGZONESTRUCTURE::TYPE)].asInt());
        Json::Value shape = nextElement[static_cast<int>(WarningZoneElement::JSONWARNINGZONESTRUCTURE::SHAPE)];
        if(tempNewWarningZone->Type() == static_cast<int>(WarningZoneElement::JSONWARNINGZONESHAPETYPE::CIRCLE)){
            tempNewWarningZone->addToShape(shape[static_cast<int>(WarningZoneElement::JSONWARNINGZONECIRCLESTRUCTURE::X)].asDouble()/ myMapResolution,
                    myMapHeight - shape[static_cast<int>(WarningZoneElement::JSONWARNINGZONECIRCLESTRUCTURE::Y)].asDouble() / myMapResolution);
            tempNewWarningZone->addToShape(shape[static_cast<int>(WarningZoneElement::JSONWARNINGZONECIRCLESTRUCTURE::R)].asDouble()/ myMapResolution, -1);
        }
        else{
            for(auto i = 0U; i < shape.size(); i += 2){

                tempNewWarningZone->addToShape(shape[i].asDouble()/ myMapResolution, myMapHeight - shape[i+1].asDouble() / myMapResolution);
            }
        }
        qmlWarningZone.append(tempNewWarningZone);
    }
    mutex.unlock();
}

void Client::refreshWarningZone()
{
    Json::Value newWarningZoneList;

    try{

        newWarningZoneList = client->getWarningZone(sessionId);
        Client::refreshWarningZone(newWarningZoneList);
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
}

void Client::refreshLiftStatus()
{
    try{
        mutex.lock();
        Json::Value tempStatus;
        tempStatus = client->getLiftStatus(sessionId);
        emit signalLiftAction(QVariant(lastLiftingAction), QVariant(tempStatus[0].asInt()));

        if(tempStatus[0].asInt() == 0){

            timerLiftState->setInterval(250);
        }
        else{

            timerLiftState->setInterval(2000);
        }
    }
    catch (jsonrpc::JsonRpcException e){

        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }
    mutex.unlock();
}

void Client::refreshKeyPoseGridViewModel()
{
    keyPoseGridViewList.clear();
    for(auto tempKeyPose : qmlKeyPoseList){

        if(tempKeyPose->getName().contains("dock")){

            keyPoseGridViewList.append(new KeyPosePathViewElement(tempKeyPose->getName(), dockImage, tempKeyPose->getDescription(),tempKeyPose->getTypeSting(), tempKeyPose->getTimeSting()));
        }
        else{

            keyPoseGridViewList.append(new KeyPosePathViewElement(tempKeyPose->getName(), keyPoseImage, tempKeyPose->getDescription(),tempKeyPose->getTypeSting(), tempKeyPose->getTimeSting()));
        }
    }

    emit signalRefreshContextProperties(QVariant::fromValue(keyPoseGridViewList),"keyPoseGridViewModel");
}

void Client::refreshKeyPosePathList()
{

    mutex.lock();
    keyPosePathGridViewList.clear();
    listOfKeyPosePath.clear();
    try
    {
        Json::Value tempList;

        tempList = client->getListOfKeyPosePath(sessionId);
        for(auto tempElement : tempList){

            keyPosePathGridViewList.append(new KeyPosePathViewElement(QString::fromStdString(tempElement[0].asString()),
                                           keyPosePathImage,QString::fromStdString(tempElement[1].asString())));

            QList<QString> tempPath;

            for(unsigned i = 2; i < tempElement.size(); i++){

                tempPath.append(QString(tempElement[i].asString().c_str()));
            }

            listOfKeyPosePath.append(new KeyPosePath(QString(tempElement[0].asString().c_str()),tempPath,QString(tempElement[2].asString().c_str())));
        }
    }
    catch (jsonrpc::JsonRpcException e)
    {
        std::cerr << e.what() << std::endl;
        if(e.GetCode() == -32003){

            logIn();
        }
    }

    mutex.unlock();
    emit signalRefreshContextProperties(QVariant::fromValue(keyPosePathGridViewList),"keyPosePathGridViewModel");
    emit signalKeyPosePathList();
}

void Client::refreshMidKeyPoseGraph(Json::Value newData)
{
    mutex.lock();
    qmlMidKeyPoseGraphList.clear();
    for(unsigned i = 0; i < newData.size(); i++){

        qmlMidKeyPoseGraphList.push_back(new MidKeyPoseGraphElement());
    }
    int i = 0;
    for(auto tempElement : newData){
        qmlMidKeyPoseGraphList.at(i)->setX(tempElement[0].asDouble()/ myMapResolution);
        qmlMidKeyPoseGraphList.at(i)->setY(myMapHeight - (tempElement[1].asDouble())/ myMapResolution);
        for(unsigned j = 2; j < tempElement.size(); j++){

            qmlMidKeyPoseGraphList.at(i)->setEdges(qmlMidKeyPoseGraphList.at(tempElement[j].asInt()));
        }
        i++;
    }
    mutex.unlock();
}
