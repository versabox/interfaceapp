#include "include/interface/videoreceiver.h"

#include <QDateTime>
#include <iostream>

VideoReceiver::VideoReceiver(std::string fileName, int outputWidth, int outputHeight)
    : outputWidth(outputWidth), outputHeight(outputHeight), frameReaderTimer(this), fileName(fileName), uninitialized(true)
{
    QObject::connect(&frameReaderTimer, SIGNAL(timeout()), this, SLOT(receiveFrame()));
}

VideoReceiver::~VideoReceiver()
{
    stopReceiver();
    finish();
}

int VideoReceiver::getVideoWidth()
{
    return outputWidth;
}

int VideoReceiver::getVideoHeight()
{
    return outputHeight;
}

void VideoReceiver::startReceiver(int frequency)
{
    if(uninitialized)
    {
        emit receiverException("Receiver uninitialized. (VideoReceiver::startReceiver)");
        return;
    }

    frameReaderTimer.start(frequency);
}

void VideoReceiver::stopReceiver()
{
    if(uninitialized)
    {
        emit receiverException("Receiver uninitialized. (VideoReceiver::stopReceiver)");
        return;
    }

    frameReaderTimer.stop();
}

void VideoReceiver::setResolution(int width, int height)
{
    if(uninitialized)
        return;

    if (width < 50 || height < 50)
        return;

    outputHeight = height;
    outputWidth = width;

    av_free(sws_ctx);

    sws_ctx = sws_getContext (
                codecCtx->width,
                codecCtx->height,
                codecCtx->pix_fmt,
                width,
                height,
                PIX_FMT_RGB24,
                SWS_FAST_BILINEAR, //tu zmienic rodzaj skalowania!
                NULL,
                NULL,
                NULL
                );
}

void VideoReceiver::receiveFrame()
{
    if(uninitialized)
    {
        emit receiverException("Receiver uninitialized. (VideoReceiver::receiveFrame)");
        return;
    }

    AVPacket packet;
    int frameFinished = 0;

//    if(currentPlayTime==-1)
//        currentPlayTime = (QDateTime::currentMSecsSinceEpoch() - startTime)/100;
//    else
//        currentPlayTime+=1;
    //do
    {
        // Read frame and Is this a packet from the video stream?
        if (av_read_frame(formatCtx, &packet) < 0 || packet.stream_index!=videoStream)
            return;

        // Decode video frame
        avcodec_decode_video2(codecCtx, decodedFrame, &frameFinished, &packet);;
    }
    //while(packet.pts < currentPlayTime);

    // Did we get a video frame?
    if(frameFinished)
    {
        // Convert the image from its native format to RGBA
        sws_scale(
                    sws_ctx,
                    (uint8_t const * const *)decodedFrame->data,
                    decodedFrame->linesize,
                    0,
                    codecCtx->height,
                    frameRGBA->data,
                    frameRGBA->linesize
                    );

        QByteArray rawImageData = convertToPPMByteArray(frameRGBA, outputWidth, outputHeight);
        QImage image;
        image.loadFromData(rawImageData, "PPM");

        emit frameReady(image);
    }
    // Free the packet that was allocated by av_read_frame
    av_free_packet(&packet);
}

void VideoReceiver::init()
{
    if(!uninitialized)
        return;

    AVCodec         *pCodec = NULL;
    AVDictionary    *optionsDict = NULL;
    AVInputFormat   *fmt = NULL;
    char            *format = "mjpeg";

    // Register all formats and codecs
    av_register_all();
    avcodec_register_all();

    avformat_network_init();
    fmt = av_find_input_format(format);

    // Open video file
    if(avformat_open_input(&formatCtx, fileName.c_str(), fmt, NULL)!=0)
    {
        emit receiverException("Couldn't open stream.\nCheck internet connection.");
        return;
    }

    // Set the start time of streaming a video
    startTime = QDateTime::currentMSecsSinceEpoch();

    // Retrieve stream information
    if(avformat_find_stream_info(formatCtx, NULL)<0)
    {
        printf("couldn't find stream info %s", fileName.c_str());
        emit receiverException("Couldn't find stream information");
        return;
    }

    // Dump information about file onto standard error
    av_dump_format(formatCtx, 0, fileName.c_str(), 0);

    // Find the first video stream
    videoStream=-1;
    for(unsigned i=0; i<formatCtx->nb_streams; i++)
    {
        if(formatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
        {
            videoStream=i;
            break;
        }
    }

    if(videoStream==-1)
    {
        printf("Didn't find a video stream %s", fileName.c_str());
        emit receiverException("Didn't find a video stream");
        return;
    }

    // Get a pointer to the codec context for the video stream
    codecCtx=formatCtx->streams[videoStream]->codec;

    // Find the decoder for the video stream
    pCodec=avcodec_find_decoder(codecCtx->codec_id);
    if(pCodec==NULL)
    {
        fprintf(stderr, "Unsupported codec!\n");
        emit receiverException("Codec not found");
        return;
    }

    // Open codec
    if(avcodec_open2(codecCtx, pCodec, &optionsDict)<0)
    {
        emit receiverException("Could not open codec");
        return;
    }

    // Allocate video frame
    decodedFrame=avcodec_alloc_frame();

    // Allocate an AVFrame structure
    frameRGBA=avcodec_alloc_frame();
    if(frameRGBA==NULL)
    {
        emit receiverException("frameRGBA not allocated");
        return;
    }

    // Determine required buffer size and allocate buffer
    int numBytes=avpicture_get_size(PIX_FMT_RGB24, codecCtx->width, codecCtx->height);
    buffer=(uint8_t *)av_malloc(numBytes*sizeof(uint8_t));

    // Assign appropriate parts of bitmap to image planes in pFrameRGBA
    // Note that pFrameRGBA is an AVFrame, but AVFrame is a superset
    // of AVPicture
    avpicture_fill((AVPicture *)frameRGBA, buffer, PIX_FMT_RGB24,/*AV_PIX_FMT_RGBA,*/
                   codecCtx->width, codecCtx->height);

    //get the scaling context
    sws_ctx = sws_getContext (
                codecCtx->width,
                codecCtx->height,
                codecCtx->pix_fmt,
                codecCtx->width,
                codecCtx->height,
                PIX_FMT_RGB24,
                SWS_FAST_BILINEAR, //tu zmienic rodzaj skalowania!
                NULL,
                NULL,
                NULL
                );

    uninitialized = false;
    setResolution(outputWidth, outputHeight);
    emit receiverInitialized();
}

void VideoReceiver::finish()
{
    if(uninitialized)
    {
        emit receiverException("Receiver uninitialized. (VideoReceiver::finish)");
        return;
    }

    // Free the RGB image
    av_free(buffer);
    av_free(frameRGBA);
    // Free the YUV frame
    av_free(decodedFrame);
    // Close the codec
    avcodec_close(codecCtx);
    // Close the video file
    avformat_close_input(&formatCtx);
}

QByteArray VideoReceiver::convertToPPMByteArray(AVFrame *pFrame, int width, int height)
{
    QByteArray temp;

    QByteArray output("P6\n", 3);

    temp.setNum(width);
    output.append(temp);

    output.append(" ", 1);

    temp.setNum(height);
    output.append(temp);

    output.append("\n", 1);

    temp.setNum(255);
    output.append(temp);

    output.append("\n", 1);

    for(int y=0; y<height; y++)
    {
        char *data = reinterpret_cast<char*>(pFrame->data[0]+y*pFrame->linesize[0]);
        temp.setRawData(data, width*3);
        output.append(temp);
    }

    return output;
}

VideoReceiverException::VideoReceiverException(std::string message)
    : message(message)
{}

const char *VideoReceiverException::what() const noexcept(true)
{
    return message.c_str();
}

