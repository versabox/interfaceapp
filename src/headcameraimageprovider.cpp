#include "include/interface/headcameraimageprovider.h"

HeadCameraImageProvider::HeadCameraImageProvider()
    :QQuickImageProvider(QQuickImageProvider::Image)

{
    image=QImage(1920,768,QImage::Format_RGB888);
}

QImage HeadCameraImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    return image;
}

void HeadCameraImageProvider::newImage(QImage image)
{
    this->image=image;
}

