#include "include/interface/supportstructures.h"
std::map<int,int> KeyPoseElement::keyPoseBoolParamMap{
        {static_cast<int>(KeyPoseElement::QMLBOOLPARAMSTRUCTURE::SET_DEFAULT_TIME),
                static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::SET_DEFAULT_TIME)},
        {static_cast<int>(KeyPoseElement::QMLBOOLPARAMSTRUCTURE::SET_INFINITE_TIME),
                static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::SET_INFINITE_TIME)}
    };
std::map<int,int> KeyPoseElement::keyPoseTypeToRosMap{
        {static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::BUTTON_WITH_DEADLINE),
                static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::BUTTON_WITH_DEADLINE)},
        {static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::SIMPLE),
                static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::SIMPLE)},
        {static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::TROLLEY),
                static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::TROLLEY)},
        {static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::GIVE_WAY),
                static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::GIVE_WAY)},
        {static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::DOOR),
                static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::DOOR)},
        {static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::CHARGING),
                static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::CHARGING)},
        {static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::LIFT),
                static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::LIFT)}
    };

std::map<int,int> KeyPoseElement::keyPoseTypeToQmlMap{
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::BUTTON_WITH_DEADLINE),
                static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::BUTTON_WITH_DEADLINE)},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::SIMPLE),
                static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::SIMPLE)},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::TROLLEY),
                static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::TROLLEY)},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::GIVE_WAY),
                static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::GIVE_WAY)},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::DOOR),
                static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::DOOR)},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::CHARGING),
                static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::CHARGING)},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::LIFT),
                static_cast<int>(KeyPoseElement::QMLKEYPOSETYPE::LIFT)}
    };
std::map<int,QString> KeyPoseElement::keyPoseNameTypeMap{
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::BUTTON_WITH_DEADLINE),
                QString("BUTTON WITH DEADLINE")},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::SIMPLE),
                QString("SIMPLE")},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::TROLLEY),
                QString("TROLLEY")},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::GIVE_WAY),
                QString("GIVE WAY")},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::DOOR),
                QString("DOOR")},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::CHARGING),
                QString("CHARGING")},
        {static_cast<int>(KeyPoseElement::ROSKEYPOSETYPE::LIFT),
                QString("LIFT")}
    };
std::map<int,QString> KeyPoseElement::keyPoseNameBoolParamsMap{
        {static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::SET_DEFAULT_TIME),
                QString("DEFAULT TIME")},
        {static_cast<int>(KeyPoseElement::BOOLPARAMSSTRUCTURE::SET_INFINITE_TIME),
                QString("INFINITE TIME")}
    };
std::map<int,int> WarningZoneElement::qmlToJsonWarningZoneTypeMap{
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::CROSSING),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::CROSSING)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::DOCKING),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::DOCKING)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::FORBIDDEN),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::FORBIDDEN)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::INFO),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::INFO)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::SILENT),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::SILENT)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::DOOR),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::DOOR)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::FOLLOW_PATH),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::FOLLOW_PATH)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::FAST_RIDE_ZONE),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::FAST_RIDE_ZONE)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::SLOW_RIDE_ZONE),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::SLOW_RIDE_ZONE)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::STOP_ZONE),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::STOP_ZONE)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::NO_RECOVERY_BEHAVIOR),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::NO_RECOVERY_BEHAVIOR)},
        {static_cast<int>(WarningZoneElement::QMLWARNINGZONETYPE::FOLLOW_PATH_CONDITIONAL_ZONE),
                static_cast<int>(WarningZoneElement::JSONWARNINGZONETYPE::FOLLOW_PATH_CONDITIONAL_ZONE)},
    };

std::map<int,int> Locker::qmlToJsonLockerActionMap{
        {static_cast<int>(Locker::QMLLOCKERACTION::lock),
                static_cast<int>(Locker::JSONLOCKERACTION::lock)},
        {static_cast<int>(Locker::QMLLOCKERACTION::unlock),
                static_cast<int>(Locker::JSONLOCKERACTION::unlock)},
    };
