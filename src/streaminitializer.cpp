#include "include/interface/streaminitializer.h"

StreamInitializer::StreamInitializer(QString ip, int port) : ip(ip), port(port)
{

}

StreamInitializer::~StreamInitializer()
{

}

void StreamInitializer::start()
{
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(1234);

    QObject::connect(udpSocket, SIGNAL(readyRead()),this, SLOT(readUdpDatagram()));
    QObject::connect(&timerUdpStarter,SIGNAL(timeout()),this,SLOT(writeUdpDatagram()));

    timerUdpStarter.start(1000);
}

void StreamInitializer::readUdpDatagram()
{
    while(udpSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(udpSocket->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;
        udpSocket->readDatagram(datagram.data(), datagram.size(),&sender, &senderPort);
        QString data(datagram.data());

        QRegExp rx("\\d{1,}.\\d{1,}.\\d{1,}.\\d{1,}:\\d{4,}");

        if(rx.exactMatch(data)) {
            QRegExp reg(":");
            QStringList ip = data.split(reg);
            emit mappedAddressReceived(ip.at(0), ip.at(1).toInt());
            udpSocket->close();
            timerUdpStarter.stop();
            break;
        }
    }
}

void StreamInitializer::writeUdpDatagram()
{
    udpSocket->writeDatagram(QByteArray(), QHostAddress(ip), port);
}
